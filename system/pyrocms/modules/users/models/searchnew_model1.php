<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @author 		Phil Sturgeon - PyroCMS Development Team
 * @package 	PyroCMS
 * @subpackage 	Users Module
 * @since		v0.1
 *
 */
class Searchnew_Model1 extends MY_Model {
	
	/**
	 * Get a user profile
	 *
	 * @access public
	 * @param array $params Parameters used to retrieve the profile
	 * @return object
	 */
	//priya 
	public function searchresult()
	{
	  if($_POST['submit'] || $_POST['save'] || $_POST['search'] || $_POST['edit'] || $_POST['mid'] || $_POST['GENDER'])
	   {
	   $_SESSION['gender']='';
	   $_SESSION['minage']='';
	   $_SESSION['maxage']='';
	   $_SESSION['minh']='';
	   $_SESSION['maxh']='';
	   $_SESSION['religion']='';
	   $_SESSION['marital']='';
	   $_SESSION['mothertangue']='';
	   $_SESSION['caste']='';
	   $_SESSION['countryright']='';
	   $_SESSION['educationright']='';
	   $_SESSION['photo']='';
	   $_SESSION['stheight']='';
	   $_SESSION['endheight']='';
	   $_SESSION['nage']='';
	   $_SESSION['mage']='';
	   $_SESSION['subcaste']='';
	   $_SESSION['physicalstatus']='';
	   $_SESSION['occuption']='';
	   $_SESSION['income']='';
	   $_SESSION['star']='';
	   $_SESSION['manglik']='';
	   $_SESSION['eating']='';
	   $_SESSION['drink']='';
	   $_SESSION['smoke']='';
	   $_SESSION['keyword']='';
	   $_SESSION['horoscope']='';
	   $_SESSION['ignore']='';
	   $_SESSION['contacted']='';
	   $_SESSION['viewed']='';
	   $_SESSION['shortlist']='';
	   $_SESSION['mid']='';
	   $_SESSION['employed']='';
	   $_SESSION['state']='';
	   $_SESSION['city']='';
	   
	   
	   
	 if($_POST['mid'])
	 {
	  $_SESSION['mid'] =$_POST['mid'];
	  }
	   
	   if($_POST['GENDER'])
	   {
	   $_SESSION['gender']=$_POST['GENDER'];
	   }
	   if($_POST['STAGE'])
	   {
       $_SESSION['minage']=(date('Y')-$_POST['STAGE'])."-".date('m')."-".date('d');
	   $_SESSION['mage']=$_POST['STAGE'];
	   }
	   if($_POST['ENDAGE'])
	   {
       $_SESSION['maxage']=(date('Y')-($_POST['ENDAGE']+1))."-".date('m')."-".date('d');
	   $_SESSION['nage']=$_POST['ENDAGE'];
	   }
	  
	   
       $marital1=$_POST['MARITAL_STATUS'];
	   if($_POST['community'])
	   {
	   $_SESSION['religion']=$_POST['community'];
	   }
	   for($i=0;$i<count($marital1);$i++)
{
if($i!=0)
{
$_SESSION['marital'].=",'".$marital1[$i]."'";
}
else
{
$_SESSION['marital']="'".$marital1[$i]."'";
}
} 
$mothertangue1=explode(",",$_POST['mothertangue']);

for($i=0;$i<count($mothertangue1)&&$mothertangue1[$i]&&!in_array("Any", $mothertangue1);$i++)
{
if($i!=0)
{
$_SESSION['mothertangue'].=",'".$mothertangue1[$i]."'";
}
else
{
$_SESSION['mothertangue']="'".$mothertangue1[$i]."'";
}
}
$caste1=explode(",",$_POST['caste']);
for($i=0;$i<count($caste1)&& $caste1[$i]&&!in_array("Any", $caste1) ;$i++)
{
if($i!=0)
{
$_SESSION['caste'].=",'".$caste1[$i]."'";
}
else
{
$_SESSION['caste']="'".$caste1[$i]."'";
}
}
$COUNTRYRIGHT1=explode(",",$_POST['COUNTRYRIGHT']);
for($i=0;$i<count($COUNTRYRIGHT1)&& $COUNTRYRIGHT1[$i]&&!in_array("Any", $COUNTRYRIGHT1);$i++)
{
if($i!=0)
{
$_SESSION['countryright'].=",'".$COUNTRYRIGHT1[$i]."'";
}
else
{
$_SESSION['countryright']="'".$COUNTRYRIGHT1[$i]."'";
}
}
if($_POST['RESIDINGSTATERIGHT'])
{
$state1=explode(",",$_POST['RESIDINGSTATERIGHT']);
for($i=0;$i<count($state1)&& $state1[$i]&&!in_array("Any", $state1);$i++)
{
if($i!=0)
{
$_SESSION['state'].=",'".$state1[$i]."'";
}
else
{
$_SESSION['state']="'".$state1[$i]."'";
}
}
}
if($_POST['RESIDINGCITYRIGHT'])
{
$city1=explode(",",$_POST['RESIDINGCITYRIGHT']);
for($i=0;$i<count($city1)&& $city1[$i]&&!in_array("Any", $city1);$i++)
{
if($i!=0)
{
$_SESSION['city'].=",'".$city1[$i]."'";
}
else
{
$_SESSION['city']="'".$city1[$i]."'";
}
}
}

$EDUCATIONRIGHT=explode(",",$_POST['EDUCATIONRIGHT']);
for($i=0;$i<count($EDUCATIONRIGHT)&&$EDUCATIONRIGHT[$i]&&!in_array("Any", $EDUCATIONRIGHT);$i++)
{
if($i!=0)
{
$_SESSION['educationright'].=",'".$EDUCATIONRIGHT[$i]."'";
}
else
{
$_SESSION['educationright']="'".$EDUCATIONRIGHT[$i]."'";
}
}
 $_SESSION['photo']= $_POST['PHOTO_OPT'];
 if($_POST['STHEIGHT'])
 {
 $_SESSION['stheight']=$_POST['STHEIGHT'];
 }
  if($_POST['ENDHEIGHT'])
 {
  $_SESSION['endheight']=$_POST['ENDHEIGHT'];
 }
 if($_POST['subcaste'])
 {
$subcaste= explode(",",$_POST['subcaste']);
 for($i=0;$i<count($subcaste)&&$subcaste[$i]&&!in_array("Any", $subcaste);$i++)
{
if($i!=0)
{
 $_SESSION['subcaste'].=",'".$subcaste[$i]."'";
}
else
{
$_SESSION['subcaste']="'".$subcaste[$i]."'";
}
}
 
 
 }
 if($_POST['specialcases'])
 {
 $_SESSION['physicalstatus']=$_POST['specialcases'];
 }
 if($_POST['OCCUPATIONRIGHT'])
 {
$occupation= explode(",",$_POST['OCCUPATIONRIGHT']);
for($i=0;$i<count($occupation)&&$occupation[$i]&&!in_array("Any", $occupation);$i++)
{
if($i!=0)
{
 $_SESSION['occuption'].=",'".$occupation[$i]."'";
}
else
{
$_SESSION['occuption']="'".$occupation[$i]."'";
}
}
 
 }
 if($_POST['STANNUALINCOME'])
 {
 $_SESSION['income']=$_POST['STANNUALINCOME'];
 }
 
 if($_POST['MANGLIK'])
 {
 $_SESSION['manglik']=$_POST['MANGLIK'];
 }
if($_POST['STARRIGHT'])
 {
$star= explode(",",$_POST['STARRIGHT']);
for($i=0;$i<count($star)&&$star[$i]&&!in_array("Any", $star);$i++)
{
if($i!=0)
{
 $_SESSION['star'].=",'".$star[$i]."'";
}
else
{
$_SESSION['star']="'".$star[$i]."'";
}
}
 
 }
 if($_POST['EATINGHABITS'])
 {
 $_SESSION['eating']=implode(',',$_POST['EATINGHABITS']);
 }
 if($_POST['DRINKING'])
 {
 $_SESSION['drink']=implode(',',$_POST['DRINKING']);
 }
  if($_POST['SMOKING'])
 {
 $_SESSION['smoke']=implode(',',$_POST['SMOKING']);
 }
if($_POST['KEYWORDS'])
{
$_SESSION['keyword']=$_POST['KEYWORDS'];
}
if($_POST['HOROSCOPE_OPT'])
{
$_SESSION['horoscope']=$_POST['HOROSCOPE_OPT'];
}
if($_POST['IGNORE_OPT'])
{
$_SESSION['ignore']=$_POST['IGNORE_OPT'];
}
if($_POST['CONTACT_OPT'])
{
$_SESSION['contacted']=$_POST['CONTACT_OPT'];
}
if($_POST['VIEW_OPT'])
{
$_SESSION['viewed']=$_POST['VIEW_OPT'];
}
if($_POST['SHORTLIST_OPT'])
{
$_SESSION['shortlist']=$_POST['SHORTLIST_OPT'];
}
 }
 $str="select * from users as u, profiles as p where u.active=1 and u.activation_code = '' and u.group_id =2 and  u.id = p.user_id  and p.activestatus=1";
 $str1='';
 if($_SESSION['mid'])
 {
 $str.=" and uid='".$_SESSION['mid']."'";
 }
if($_SESSION['gender'])
{
$str.=" and gender='".$_SESSION['gender']."'";
 if($_SESSION['gender']=='f')
 {
  $str1='Female';
  }
  else
  {
  $str1='Male';
  }

}
if($_SESSION['minage'] && $_SESSION['maxage'])
{
$str.=" and dob<='".$_SESSION['minage']."' and dob>'".$_SESSION['maxage']."'";
$str1.=", ".$_SESSION['mage']." to ".$_SESSION['nage'];
}
else
{
if($_SESSION['minage'])
{
$str.=" and dob<='".$_SESSION['minage']."'";
$str1.=", ".$_SESSION['mage'];

}
if($_SESSION['maxage'])
{
$str.=" and dob>'".$_SESSION['maxage']."'";
$str1.=", ".$_SESSION['nage'];
}

}

if($_SESSION['religion'])
{
$str.=" and religion='".$_SESSION['religion']."'";
$str1.=", ".$_SESSION['religion'];
}
if($_SESSION['marital'])
{
$str.=" and maritalstatus in(".$_SESSION['marital'].")";
$str1.=", ".$_SESSION['marital'];
}
if($_SESSION['mothertangue'])
{
$str.=" and mother_tongue in(".$_SESSION['mothertangue'].")";
$str1.=", ".$_SESSION['mothertangue'];
}	
if($_SESSION['caste'])
{
$str.=" and caste in(".$_SESSION['caste'].")";
$str1.=", ".$_SESSION['caste'];
} 
if($_SESSION['countryright'])
{
$str.=" and country in(".$_SESSION['countryright'].")";
$str1.=", ".$_SESSION['countryright'];
}  
if($_SESSION['educationright'])
{
$str.=" and education in(".$_SESSION['educationright'].")";
$str1.=", ".$_SESSION['educationright'];
} 
if($_SESSION['photo'])
{
$str.=" and p.user_id in(select uid from photo)";
$str1.=", photo = yes";
}
if($_SESSION['stheight'] && $_SESSION['endheight'])
{
$str.=" and height>='".$_SESSION['stheight']."' and height<='".$_SESSION['endheight']."'";
$str1.=", ".$_SESSION['stheight']." to ".$_SESSION['endheight'];
}
else
{
if($_SESSION['stheight'])
{
$str.=" and height>='".$_SESSION['stheight']."'";
$str1.=", ".$_SESSION['stheight'];
}   
 if($_SESSION['endheight'])
{
$str.=" and height<='".$_SESSION['endheight']."'";
$str1.=", ".$_SESSION['endheight'];
} 
}
if($_SESSION['subcaste'])
{
$str.=" and subcaste in(".$_SESSION['subcaste'].")";
$str1.=", ".$_SESSION['subcaste'];
}
if($_SESSION['physicalstatus'])
{
$str.=" and specialclasses ='".$_SESSION['physicalstatus']."'";
$str1.=", Physical status: ".$_SESSION['physicalstatus'];
}
if($_SESSION['occuption'])
{
$str.=" and workingwith in(".$_SESSION['occuption'].")";
$str1.=", ".$_SESSION['occuption'];
}
if($_SESSION['income'])
{
$str.=" and income in(".$_SESSION['income'].")";
$str1.=", ".$_SESSION['income'];
}
if($_SESSION['star'])
{
$str.=" and star in(".$_SESSION['star'].")";
$str1.=", ".$_SESSION['star'];
}
if($_SESSION['manglik'])
{
$str.=" and manglik in(".$_SESSION['manglik'].")";
$str1.=", manglik: ".$_SESSION['manglik'];
}
if($_SESSION['eating'])
{
$str.=" and diet in(".$_SESSION['eating'].")";
$str1.=", diet: ".$_SESSION['eating'];
}
if($_SESSION['drink'])
{
$str.=" and drink in(".$_SESSION['drink'].")";
$str1.=", drink:".$_SESSION['drink'];
}
if($_SESSION['smoke'])
{
$str.=" and smoke in(".$_SESSION['smoke'].")";
$str1.=", smoke: ".$_SESSION['smoke'];
}
if($_SESSION['keyword'])
{
$str.=" and bio in(".$_SESSION['keyword'].")";
$str1.=", ".$_SESSION['keyword'];
}
if($_SESSION['horoscope'])
{
$str.=" and horoscope !=''";
$str1.=", Horoscope: ".$_SESSION['horoscope'];
}
if($_SESSION['ignore'])
{
$str.=" and u.id not in(select ignore_id from user_ignore where ignoredby_id=".$this->session->userdata('id').")";
$str1.=", Ignored Profiles don't show";
}
if($_SESSION['contacted'])
{
$str.=" and u.id not in(select receiver_id from user_mail where sender_id=".$this->session->userdata('id').")";
$str1.=", Profiles already contacted don't show";
}
if($_SESSION['viewed'])
{
$str.=" and u.id not in(select user_viewed_id from profileviewed where user_view_by=".$this->session->userdata('id').")";
$str1.=", Viewed Profiles don't show";
}
if($_SESSION['shortlist'])
{
$str.=" and u.id not in(select receiver_id from shortlist where sender_id=".$this->session->userdata('id').")";
$str1.=", Short-listed Profiles don't show";
}
If($_SESSION['employed'])
{
$str.=" and workingwith in(".$_SESSION['employed'].")";
 $str1.=", Working with: ".$_SESSION['employed'];
}
If($_SESSION['state'])
{
$str.=" and stateresidence in(".$_SESSION['state'].")";
 $str1.=", state: ".$_SESSION['state'];
}
If($_SESSION['city'])
{
$str.=" and cityresidence in(".$_SESSION['city'].")";
 $str1.=", city: ".$_SESSION['city'];
}
$_SESSION['str1']=$str1;
//echo $str;
$query= $this->db->query($str)or die(mysql_error());
return $query->result_array();
	}
}?>