<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Register_step2 extends CI_Controller {

	public function index($id)
	{
	if(!$_SESSION['newuid'])
	{
	redirect('users/register');
	}
	
	$data['id']=$id;
	$_SESSION['newuid']=$id;
	$this->load->view('header');
	 $this->load->view('register_step2',$data);
	 $this->load->view('footer');
	}
	
	
	public function post()
	{
	 
	if(!$_SESSION['newuid'])
	{
	redirect('users/register');
	}

	$this->load->model('register_model', '', TRUE);
	$liked=$this->register_model->save_step2();	

	$this->load->model('profile_model', '', TRUE);
 $data['query']=$this->profile_model->getprofile($_SESSION['newuid']);
	
	 $this->load->view('header');
	$this->load->view('partnerprofile',$data);
	 $this->load->view('footer');
 //redirect('users/photo/index/'.$id, 'refresh');
	
	}
}
?>