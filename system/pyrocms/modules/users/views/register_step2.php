<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />


  <title><?php //echo $title; ?></title>

   <link href="<?php echo css_path('reg-v141.css'); ?>" type="text/css" rel="stylesheet">
   
<script type="text/javascript" src="<?php echo js_path('jquery-1.4.2.js'); ?>"></script>
<script type="text/javascript" src="<?php echo js_path('registration-v9.js'); ?>"></script>


 <script type="text/javascript">
 function validateform1()
 {
 
r=0;

if(document.getElementById('postedby').value=='')
{
document.getElementById('errmsg_postedby').style.display='inline';
document.getElementById('postedby').focus();

r=1;
}
if(document.getElementById('maritalstatus').value=='')
{
document.getElementById('errmsg_maritalstatus').style.display='inline';
document.getElementById('maritalstatus').focus();
r=1;
}
if(document.getElementById('stateofresidence').value=='')
{
document.getElementById('stateofresidence').focus();
document.getElementById('errmsg_stateofresidence').style.display='inline';
r=1;
}
if(document.getElementById('nearest_city').value=='')
{
document.getElementById('errmsg_city').style.display='inline';
document.getElementById('nearest_city').focus();
r=1;
}
if(document.getElementById('height').value=='')
{
document.getElementById('height').focus();
document.getElementById('errmsg_height').style.display='inline';
r=1;
}
if(document.getElementById('weight').value=='')
{
document.getElementById('weight').focus();
document.getElementById('errmsg_weight').style.display='inline';
r=1;
}
if(document.getElementById('educationlevel').value=='')
{
document.getElementById('educationlevel').focus();
document.getElementById('errmsg_educationlevel').style.display='inline';
r=1;
}
if(document.getElementById('educationfield').value=='')
{
document.getElementById('educationfield').focus();
document.getElementById('errmsg_educationfield').style.display='inline';
r=1;
}
if(document.getElementById('working_with').value=='')
{
document.getElementById('working_with').focus();
document.getElementById('errmsg_working_with').style.display='inline';
r=1;
}
if(document.getElementById('occupation').value=='')
{
document.getElementById('occupation').focus();
document.getElementById('errmsg_occupation').style.display='inline';
r=1;
}
if(document.getElementById('aboutyourself').value=='')
{
document.getElementById('aboutyourself').focus();
//document.getElementById('postedby').style.display='inline';
r=1;
}
if(r==1)
{


return false
}

 


 
 }
 </script>

</head>

<body>
<br clear="all">

		<!--<![if gt IE 6]><div class="spacer_5"></div><![endif]>-->
		<!--[if !IE]--><div class="spacer_2"></div><!--[endif]-->

        
<div id="formarea">
<div>
  <div>( * Mandatory Fields)</div>
    <div style="float:right;margin-right:18px;_margin-right:10px;font:normal 14px arial;color:#fff">Page 1 of 2</div>
</div>
<div class="bg"><!--"-->
<form method="post" action="<?php echo base_url(); ?>index.php/users/register_step2/post" name="frm_profile" id="frm_profile" autocomplete="off" style="margin:0px" onsubmit="return validateform1()">

            


            <!-- PROFILE CREATED FOR -->

			
            <div style="padding:5px"></div>
  <span  class="heading">Account Information</span>
            <!-- Marital Status / HEIGHT -->
            <div class="form">
                <div class="wrapper" style="padding:5px 15px 10px 15px" >
				<h6 class="dot" style="height:20px;">More Personal Details</h6>
				<?php
				if(isset($msg))
				{
				?>
				<div>
				<font color="#FF0000">
				<?php echo $msg; ?>
				</font>
				</div>
				<?php }?>




                



 <label class="label-180" for="postedby">Profile Created for <span class="mandatory">*</span></label>

				<div id="postedby_div" class="select_error" style="display:inline">
<select name="postedby" id="postedby" class="select1" style="margin-left:0px" onfocus="disable_creative();toggleHint_new('show', this.name, this, this.offsetWidth)" onchange="profile_for_select_action(this);" onblur="validate_postedby();">
    <option value=""  selected="selected">Select</option>
    <option value="Self" label="Self">Self</option>
    <option value="Son" label="Son">Son</option>
    <option value="Daughter" label="Daughter">Daughter</option>
    <option value="Brother" label="Brother">Brother</option>
    <option value="Sister" label="Sister">Sister</option>
    <option value="Friend" label="Friend">Friend</option>
    <option value="Relative" label="Relative">Relative</option>
</select></div>
				<div id="errmsg_postedby" class="error" style="display:none"><span>Relevant relationship information is mandatory.</span></div>
                
            <!-- HINT STARTS HERE -->
            <span class="hint" id="hint_postedby" style="display:none;">
			
            <div>
                <div class="top_hint"></div><div class="middle_hint">Please select your relationship with the person looking to get married.<br><br></div><div class="bottom_hint"></div>
            </div>
            <div class="arrow_hint"></div>
            </span>

   <div style="padding:5px"></div>

 <br clear="all">
<br clear="all" >
                     <!-- Marital Status -->
                    <label class="label-180" for="maritalstatus">Marital Status <span class="mandatory">*</span></label>
					<div id="maritalstatus_div" class="select_error">
						
<select name="maritalstatus" id="maritalstatus" class="select1" style="margin-left:0px" onchange="show_hidechildren_div(this.value);" onfocus="$(&quot;#errmsg_maritalstatus&quot;).hide();toggleHint_new(&quot;show&quot;, this.name, document.getElementById(&quot;maritalstatus&quot;),document.getElementById(&quot;maritalstatus&quot;).offsetWidth)" onblur="validate_maritalstatus();">
    <option value="" label="Select" selected="selected">Select</option>
    <option value="Never Married" label="Never Married">Never Married</option>
    <option value="Divorced" label="Divorced">Divorced</option>
    <option value="Widow / Widower" label="Widowed">Widow / Widower</option>
    <option value="Separated" label="Separated">Separated</option>
    <option value="Annulled" label="Annulled">Annulled</option>
</select>	
<input type="hidden" name="id" value="<?php if(isset($nid)) { echo $nid; } ?>"/>
				</div>
                    <div id="errmsg_maritalstatus" class="error" style="display:none"> Marital Status is mandatory.</div><br clear="all" style="line-height:0px">
					
            <!-- HINT STARTS HERE -->
            <span class="hint" id="hint_maritalstatus" style="display:none;">
            <div>
                <div class="top_hint"></div><div class="middle_hint">Please select marital status of the person<br> looking to get married.<br><br></div><div class="bottom_hint"></div>
            </div>
            <div class="arrow_hint"></div>
            </span>
            <!-- HINT ENDS HERE -->
        			<br clear="all" style="line-height:0px">	  <label class="label-180" for="caste">Caste<span class="mandatory">*</span> : </label>
<?php 						echo $this->session->userdata('casteselected') ;?>
						
						
						
							<br clear="all" style="line-height:0px">
					<div id="brk_ms" style="display:none"></div>
                    <!-- Marital Status END -->
					<div style="padding:5px"></div>

                    <div id="subcaste_optional" style="display:block">
                    <label class="label-180" for="subcaste">Sub-Caste</label>
                    
<input type="text" name="subcaste" id="subcaste" value="" class="input1 ac_input" style="margin-left:0px;" autocomplete="off"><br>
                    </div>

                    <div style="padding:5px"></div>
					<div id="subcaste_optional" style="display:block">
                    <label class="label-180" for="subcaste">Country living in</label>
                    
<?php echo $_POST['countryofresidence']; ?>
<br>
                    </div>

                    <div style="padding:5px"></div>
					
<label class="label-180" for="stateofresidence">State of Residence <span class="mandatory">*</span></label>
                    <div id="state_div" class="select_error">
         <?php if($_POST['countryofresidence']!='India') { ?>               
		 <input type="text" name="stateofresidence" id="stateofresidence"/>
		 <?php }
		 else
		 {?>
<select name="stateofresidence" id="stateofresidence" class="select1" style="margin-left:0px;" onchange="showUser(this.value)">
    
<option value="">Select state</option>
<?php
$q2=mysql_query('select * from countries1  group by state order by state')or die(mysql_error());
while($r2=mysql_fetch_array($q2))
{
?>
<option><?php echo $r2['state'];?></option>
<?php }?>
</select> 

 <?php }?>                 </div>
<div id="errmsg_stateofresidence" class="error" style="display:none">State of Residence is mandatory.</div>
                     <div style="margin-top:2px; padding-left:50px;display:none" id="loading_city"></div>
                    <div style="padding-left:50px;display:none" id="loading_state"></div>
                    <div id="errmsg_state" class="error" style="display:none"></div><br>
                    
            <!-- HINT STARTS HERE -->
            <span class="hint" id="hint_stateofresidence" style="display:none;">
            <div>
                <div class="top_hint"></div><div class="middle_hint">Please select state of current residence of the person looking to get married.<br><br></div><div class="bottom_hint"></div>
            </div>
            <div class="arrow_hint"></div>
            </span>
  <div style="padding:5px"></div>
<script type="text/javascript">
function showUser(str)
{
if (str=="")
  {
  document.getElementById("city_div").innerHTML="";
  return;
  }
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("city_div").innerHTML=xmlhttp.responseText;
    }
  }

xmlhttp.open("GET","http://www.test.hariomtech.com/swayamvara/system/pyrocms/modules/users/views/getuser.php?q="+str,true);
xmlhttp.send();
}
</script>

                    <label class="label-180" for="nearest_city">City of Residence <span class="mandatory">*</span></label>
                    <div id="city_div" class="select_error">
     <?php if($_POST['countryofresidence']!='India') { ?> 
	 <input type="text" name="nearest_city" id="nearest_city"/>
	 <?php } else
	 {?>                    
	 
<select name="nearest_city" id="nearest_city" class="select1" style="margin-left:0px;">
    
<option value="">Select city</option>
</select> 
<?php }?>                     </div>
                    <div id="errmsg_city" class="error" style="display:none">City of Residence is mandatory.</div><br>
					
            <!-- HINT STARTS HERE -->
            <span class="hint" id="hint_nearest_city" style="display:none;">
            <div>
                <div class="top_hint"></div><div class="middle_hint">Please select city of residence of the person looking to get married.<br><br></div><div class="bottom_hint"></div>
            </div>
            <div class="arrow_hint"></div>
            </span>  <div style="padding:5px"></div>
                    <!-- HAVE CHILDREN -->
                    <div id="children_div" style="display: none; ">
                        <label class="label-180" for="no_of_kids">Have Children <span class="mandatory">*</span></label>
                        <div id="have_child_div" class="select_error">
                           
<select name="children" id="children" class="select1" style="margin-left:0px" onchange="reset_havechildren();validate_children();" onfocus="$(&quot;#errmsg_havechildren&quot;).hide();" onblur="validate_children();">
    <option value="" label="Select" selected="selected">Select</option>
    <option value="Yes. Living together" label="Yes. Living together">Yes. Living together</option>
    <option value="No" label="No">No</option>
    <option value="Yes. Not living together" label="Yes. Not living together">Yes. Not living together</option>
</select>                        </div>
                        <div id="errmsg_havechildren" class="error" style="display:none"></div><br>
                        <span id="no_of_kids_span" style="margin-top: 10px; display: none; ">
                            <div id="no_of_kids_div" class="select_error" style="width:100px;float:left;margin:0px 0px 0px 190px;_margin:0px 0px 0px 95px">
                                
<select name="no_of_kids" id="no_of_kids" class="country-code" style="margin-left:0px">
    <option value="0" label="No.of Kids">No.of Kids</option>
    <option value="1" label="1">1</option>
    <option value="2" label="2">2</option>
    <option value="3" label="3">3</option>
    <option value="More than 3" label="More than 3">More than 3</option>
</select>                            </div><b style="display:block;float:left;font:normal 12px arial;color:#FE6C00;margin-left:25px;padding-top:5px">(Optional)</b><br clear="all">
                            <div id="errmsg_no_of_kids" class="error" style="display:none;margin-top:5px"></div>
                        </span><div style="padding:5px"></div>
                    </div>
                    <!-- HAVE CHILDREN END -->



                    <!-- HEIGHT -->
					

					<h6 class="dot">Physical Attributes</h6>
                    <label class="label-180" for="height">Height <span class="mandatory">*</span></label>
                    <div id="height_div" class="select_error">
                    
<select name="height" id="height" class="select1" style="margin-left:0px;" onfocus="$(&quot;#errmsg_height&quot;).hide();toggleHint_new(&quot;show&quot;, this.name, document.getElementById(&quot;height&quot;), document.getElementById(&quot;height&quot;).offsetWidth)" onblur="validate_height();">
    <option value="" label="Select" selected="selected">Select</option>
   <option value="4-06" label="4ft 6in - 137cm">4ft 6in - 137cm</option>
    <option value="4-07" label="4ft 7in - 139cm">4ft 7in - 139cm</option>
    <option value="4-08" label="4ft 8in - 142cm">4ft 8in - 142cm</option>
    <option value="4-09" label="4ft 9in - 144cm">4ft 9in - 144cm</option>
    <option value="4-10" label="4ft 10in - 147cm">4ft 10in - 147cm</option>
    <option value="4-11" label="4ft 11in - 149cm">4ft 11in - 149cm</option>
    <option value="5-00" label="5ft - 152cm">5ft - 152cm</option>
    <option value="5-01" label="5ft 1in - 154cm">5ft 1in - 154cm</option>
    <option value="5-02" label="5ft 2in - 157cm">5ft 2in - 157cm</option>
    <option value="5-03" label="5ft 3in - 160cm">5ft 3in - 160cm</option>
    <option value="5-04" label="5ft 4in - 162cm">5ft 4in - 162cm</option>
    <option value="5-05" label="5ft 5in - 165cm">5ft 5in - 165cm</option>
    <option value="5-06" label="5ft 6in - 167cm">5ft 6in - 167cm</option>
    <option value="5-07" label="5ft 7in - 170cm">5ft 7in - 170cm</option>
    <option value="5-08" label="5ft 8in - 172cm">5ft 8in - 172cm</option>
    <option value="5-09" label="5ft 9in - 175cm">5ft 9in - 175cm</option>
    <option value="5-10" label="5ft 10in - 177cm">5ft 10in - 177cm</option>
    <option value="5-11" label="5ft 11in - 180cm">5ft 11in - 180cm</option>
    <option value="6-00" label="6ft - 182cm">6ft - 182cm</option>
    <option value="6-01" label="6ft 1in - 185cm">6ft 1in - 185cm</option>
    <option value="6-02" label="6ft 2in - 187cm">6ft 2in - 187cm</option>
    <option value="6-03" label="6ft 3in - 190cm">6ft 3in - 190cm</option>
    <option value="6-04" label="6ft 4in - 193cm">6ft 4in - 193cm</option>
    <option value="6-05" label="6ft 5in - 195cm">6ft 5in - 195cm</option>
    <option value="6-06" label="6ft 6in - 198cm">6ft 6in - 198cm</option>
    <option value="6-07" label="6ft 7in - 200cm">6ft 7in - 200cm</option>
    <option value="6-08" label="6ft 8in - 203cm">6ft 8in - 203cm</option>
    <option value="6-09" label="6ft 9in - 205cm">6ft 9in - 205cm</option>
    <option value="6-10" label="6ft 10in - 208cm">6ft 10in - 208cm</option>
    <option value="6-11" label="6ft 11in - 210cm">6ft 11in - 210cm</option>
    <option value="7-00" label="7ft - 213cm">7ft - 213cm</option>			
</select>                    </div>
                    <div id="errmsg_height" class="error" style="display:none;">Height is mandatory.</div><br>
                    
            <!-- HINT STARTS HERE -->
            <span class="hint" id="hint_height" style="display:none;">
            <div>
                <div class="top_hint"></div><div class="middle_hint">Please select height of the person looking<br> to get married.<br><br></div><div class="bottom_hint"></div>
            </div>
            <div class="arrow_hint"></div>
            </span>
            <!-- HINT ENDS HERE -->
                            <!-- HEIGHT END -->

					 <div style="padding:5px"></div>
 <label class="label-180" for="height">Weight <span class="mandatory">*</span></label>
                    <div id="weight_div" class="select_error">
                    
<select name="weight" id="weight" class="select1" style="margin-left:0px;" onfocus="$(&quot;#errmsg_weight&quot;).hide();toggleHint_new(&quot;show&quot;, this.name, document.getElementById(&quot;weight&quot;), document.getElementById(&quot;weight&quot;).offsetWidth)" onblur="validate_weight();">
    <option value="" label="Select" selected="selected">Select</option>
	<?php
	for($i=35;$i<=140;$i++)
	{
	?>
	 <option value="<?php echo $i; ?>"><?php echo $i; ?> Kg</option>
	<?php }?>
   
</select>                    </div>
                    <div id="errmsg_weight" class="error" style="display:none;">Weight is mandatory.</div><br>
                    
            <!-- HINT STARTS HERE -->
            <span class="hint" id="hint_weight" style="display:none;">
            <div>
                <div class="top_hint"></div><div class="middle_hint">Please select Weight of the person looking<br> to get married.<br><br></div><div class="bottom_hint"></div>
            </div>
            <div class="arrow_hint"></div>
            </span>
            <!-- HINT ENDS HERE -->
                            <!-- HEIGHT END --><div style="padding:5px"></div>
							 <label class="label-180" for="bodytype">Body Type</label>
                    <span class="fl">
<label for="bodytype-Slim"><input type="radio" name="bodytype" id="bodytype-Slim" value="Slim" class="radio" onclick="$(&quot;#errmsg_bodytype&quot;).html(&quot;&quot;).hide();">Slim</label> <label for="bodytype-Athletic"><input type="radio" name="bodytype" id="bodytype-Athletic" value="Athletic" class="radio" onclick="$(&quot;#errmsg_bodytype&quot;).html(&quot;&quot;).hide();">Athletic</label> <label for="bodytype-Average"><input type="radio" name="bodytype" id="bodytype-Average" value="Average" class="radio" onclick="$(&quot;#errmsg_bodytype&quot;).html(&quot;&quot;).hide();">Average</label> <label for="bodytype-Heavy"><input type="radio" name="bodytype" id="bodytype-Heavy" value="Heavy" class="radio" onclick="$(&quot;#errmsg_bodytype&quot;).html(&quot;&quot;).hide();">Heavy</label></span>
                    <div id="errmsg_bodytype" class="error" style="display:none;width:170px;"></div><br>

                    

					 <div style="padding:5px"></div>
					  <label class="label-180" for="complexion">Complexion</label>
                    <span class="fl">
<label for="complexion-VeryFair"><input type="radio" name="complexion" id="complexion-VeryFair" value="Very Fair" class="radio" onclick="$(&quot;#errmsg_complexion&quot;).html(&quot;&quot;).hide();">Very Fair</label> <label for="complexion-Fair"><input type="radio" name="complexion" id="complexion-Fair" value="Fair" class="radio" onclick="$(&quot;#errmsg_complexion&quot;).html(&quot;&quot;).hide();">Fair</label> <label for="complexion-Wheatish"><input type="radio" name="complexion" id="complexion-Wheatish" value="Wheatish" class="radio" onclick="$(&quot;#errmsg_complexion&quot;).html(&quot;&quot;).hide();">Wheatish</label> <label for="complexion-Dark"><input type="radio" name="complexion" id="complexion-Dark" value="Dark" class="radio" onclick="$(&quot;#errmsg_complexion&quot;).html(&quot;&quot;).hide();">Dark</label></span>
                    <div id="errmsg_complexion" class="error" style="display:none;width:170px;"></div><br>

                    <div style="padding:5px"></div>

                    <label class="label-180" for="specialcases"><span style="float:left">Special Cases <span class="mandatory"></span></span></label>
                    <div id="specialcases_div" class="select_error">
                        
<select name="specialcases" id="specialcases" class="select1" style="margin-left:0px;width:286px" onfocus="$(&quot;#errmsg_specialcases&quot;).hide();toggleHint_new(&quot;show&quot;, this.name, document.getElementById(&quot;specialcases&quot;),document.getElementById(&quot;specialcases&quot;).offsetWidth)" onblur="validate_specialcases();">
    <option value="None" label="None">None</option>
    <option value="Physically challenged from birth" label="Physically challenged from birth">Physically challenged from birth</option>
    <option value="Physically challenged due to accident" label="Physically challenged due to accident">Physically challenged due to accident</option>
    <option value="Mentally challenged from birth" label="Mentally challenged from birth">Mentally challenged from birth</option>
    <option value="Mentally challenged due to accident" label="Mentally challenged due to accident">Mentally challenged due to accident</option>
    <option value="Physical abnormality affecting only looks" label="Physical abnormality affecting only looks">Physical abnormality affecting only looks</option>
    <option value="Physical abnormality affecting bodily functions" label="Physical abnormality affecting bodily functions">Physical abnormality affecting bodily functions</option>
    <option value="Physically and mentally challenged" label="Physically and mentally challenged">Physically and mentally challenged</option>
    <option value="HIV positive" label="HIV positive">HIV positive</option>
</select>                    </div>
                    <div id="errmsg_specialcases" class="error" style="display:none"></div><br>
					
            <!-- HINT STARTS HERE -->
            <span class="hint" id="hint_specialcases" style="display:none;">
            <div>
                <div class="top_hint"></div><div class="middle_hint">'Special cases' applies to members who are<br> physically or mentally challenged.<br><br></div><div class="bottom_hint"></div>
            </div>
            <div class="arrow_hint"></div>
            </span>
                    <!-- CASTE -->
                   <!-- <div id="show_caste">
                        <label class="label-180" for="caste">Caste / Sect <span class="mandatory">*</span></label>
                        <div id="caste_div">
                            
<select name="caste" id="caste" class="select1" style="margin-left:0px;" onfocus="toggleHint_new(&quot;show&quot;, this.name, document.getElementById(&quot;caste&quot;),document.getElementById(&quot;caste&quot;).offsetWidth)" onchange="show_hide_subcaste(this.value);" onblur="toggleHint(&#39;hide&#39;, &#39;caste&#39;);">
    <option value="" label="Don&#39;t Know" selected="selected">Don't Know</option>
    <option value="6000 Niyogi" label="6000 Niyogi">6000 Niyogi</option>
    
    <option value="Other" label="Other">Other</option>
</select>                        </div>
                    </div>-->
                    
            <!-- HINT STARTS HERE -->
            <!--<span class="hint" id="hint_caste" style="display:none;">
            <div>
                <div class="top_hint"></div><div class="middle_hint">Please select caste of the person looking to get married.<br><br></div><div class="bottom_hint"></div>
            </div>
            <div class="arrow_hint"></div>
            </span>-->
            <!-- HINT ENDS HERE -->
                            <!-- CASTE END -->

					

                    

                </div>
           
            <script language="JavaScript">
            <!--
                var marital_status_obj = document.forms["frm_profile"].maritalstatus;
                var marital_status_chosen = "";
                for(var j=0; j < marital_status_obj.length; j++)
                {
                    if(marital_status_obj[j].checked == true)
                    {
                        marital_status_chosen = marital_status_obj[j].value;
                    }
                }
                show_hidechildren_div(marital_status_chosen);
                reset_havechildren();
            //-->
            </script>
            <!-- Marital Status / HEIGHT END -->

            <div style="padding:1px"></div>

            <!-- Lifestyle -->
            <div class="form">
                <div class="wrapper">
<h6 class="dot">Education & Occupation</h6>
 <label class="label-180" for="educationlevel">Education <span class="mandatory">*</span></label>
                    <div id="educationlevel_div" class="select_error">
                        
<select name="educationlevel" id="educationlevel" class="select1" style="margin-left:0px;" onchange="validate_educationlevel(this.name);" onfocus="$(&quot;#errmsg_educationlevel&quot;).hide();toggleHint_new(&quot;show&quot;, this.name, document.getElementById(&quot;educationlevel&quot;),document.getElementById(&quot;educationlevel&quot;).offsetWidth)" onblur="validate_educationlevel();hide_annual_income();">
    <option value="" label="Select" selected="selected">Select</option>
    <optgroup class="a" label="&nbsp;&nbsp;&nbsp;&nbsp;-- Bachelors - Engineering / Computers --">
<option >BE / B Tech</option>
<option >BCA</option>
<option >Aeronautical Engineering</option>
<option >B Arch</option>
<option >B Plan</option>
</optgroup>
<optgroup class="a" label="&nbsp;&nbsp;&nbsp;&nbsp;-- Masters - Engineering / Computers --">
<option >MCA / PGDCA</option>
<option >ME / M Tech</option>
<option >MS (Engg.)</option>
<option >M Arch</option>
</optgroup>
<optgroup class="a" label="&nbsp;&nbsp;&nbsp;&nbsp;-- Bachelors - Arts / Science / Commerce / B Phil /... --">
<option >B Phil</option>
<option >BCom</option>
<option >BSc</option>
<option >BA</option>
<option >BHM</option>
<option >B Ed</option>
<option >Aviation Degree</option>
</optgroup>
<optgroup class="a" label="&nbsp;&nbsp;&nbsp;&nbsp;-- Masters - Arts / Science / Commerce / M Phil /... --">
<option >M Phil</option>
<option >M Com</option>
<option >M Sc</option>
<option >MA</option>
<option >MHM</option>
<option >M Ed</option>
</optgroup>
<optgroup class="a" label="&nbsp;&nbsp;&nbsp;&nbsp;-- Management - BBA / MBA /... --">
<option >MBA / PGDM</option>
<option >BBA</option>
</optgroup>
<optgroup class="a" label="&nbsp;&nbsp;&nbsp;&nbsp;-- Medicine - General / Dental / Surgeon /... --">
<option >MD / MS (Medical)</option>
<option >MBBS</option>
<option >MDS</option>
<option >MVSc </option>
<option >MPT</option>
<option >BDS</option>
<option>BVSc</option>
<option >BPT</option>
<option >BHMS</option>
<option >BAMS</option>
<option >M Pharm</option>
<option >B Pharm</option>
</optgroup>
<optgroup class="a" label="&nbsp;&nbsp;&nbsp;&nbsp;-- Legal - BL / ML / LLB / LLM /... --">
<option >ML / LLM</option>
<option >BGL / BL / LLB</option>
</optgroup>
<optgroup class="a" label="&nbsp;&nbsp;&nbsp;&nbsp;-- Finance - ICWAI / CA / CS /... --">
<option >CA</option>
<option >ICWA</option>
<option >CS</option>
</optgroup>
<optgroup class="a" label="&nbsp;&nbsp;&nbsp;&nbsp;-- Service - IAS / IPS / IRS / IES / IFS /... --">
<option >IAS / IPS/ IRS / IES / IFS</option>
</optgroup>
<optgroup class="a" label="&nbsp;&nbsp;&nbsp;&nbsp;-- PhD --">
<option >Ph D</option>
</optgroup>
<optgroup class="a" label="&nbsp;&nbsp;&nbsp;&nbsp;-- Diploma --">
<option >Trade School</option>
<option >Diploma</option>
</optgroup>
<optgroup class="a" label="&nbsp;&nbsp;&nbsp;&nbsp;-- Higher Secondary / Secondary --">
<option >Higher Secondary School / High School</option>
</optgroup>
</select>                    </div>
                    <div id="errmsg_educationlevel" class="error" style="display:none">Education Level is mandatory.</div><br>
                    
            <!-- HINT STARTS HERE -->
            <span class="hint" id="hint_educationlevel" style="display:none;">
            <div>
                <div class="top_hint"></div><div class="middle_hint">Please select educational level of the person looking to get married.<br><br></div><div class="bottom_hint"></div>
            </div>
            <div class="arrow_hint"></div>
            </span>
            
            <div id="errmsg_educationfield" class="error" style="display:none">Education field is mandatory.</div>
					
            <!-- HINT STARTS HERE -->
            <span class="hint" id="hint_educationfield" style="display:none;">
            <div>
                <div class="top_hint"></div><div class="middle_hint">Please select educational field of the person looking to get married.<br><br></div><div class="bottom_hint"></div>
            </div>
            <div class="arrow_hint"></div>
            </span>
            <!-- HINT ENDS HERE -->
                            <!-- Education Area END -->

                    <div style="padding:5px"></div>
 <label class="label-180" for="occupation">Working With <span class="mandatory">*</span></label>
                    <div id="working_with_div" class="select_error">
                        
<select name="working_with" id="working_with" class="select1" onfocus="$(&quot;#errmsg_working_with&quot;).hide();toggleHint_new(&quot;show&quot;, this.name, document.getElementById(&quot;working_with&quot;),document.getElementById(&quot;working_with&quot;).offsetWidth)" onchange="validate_working_with();" onblur="validate_working_with();" style="margin-left:0px;">
    <option value="" label="Select" selected="selected">Select</option>
    <option value="Private Company" label="Private Company">Private Company</option>
    <option value="Government / Public Sector" label="Government / Public Sector">Government / Public Sector</option>
    <option value="Defense / Civil Services" label="Defense / Civil Services">Defense / Civil Services</option>
    <option value="Business / Self Employed" label="Business / Self Employed">Business / Self Employed</option>
    <option value="Non Working" label="Non Working">Non Working</option>
</select>                    </div>
                    <div id="errmsg_working_with" class="error" style="display:none">Working with is mandatory.</div><br>
                    
            <!-- HINT STARTS HERE -->
            <span class="hint" id="hint_working_with" style="display:none;">
            <div>
                <div class="top_hint"></div><div class="middle_hint">Please specify type of organization the person looking to get married is working with.<br><br></div><div class="bottom_hint"></div>
            </div>
            <div class="arrow_hint"></div>
            </span>
            <!-- HINT ENDS HERE -->
                            <!-- Working with END -->

                    <div style="padding:5px"></div>
					 <label class="label-180" for="occupation">Occupation <span class="mandatory">*</span></label>
                    <div id="occupation_div" class="select_error">
                        
<select name="occupation" id="occupation" class="select1" onfocus="$(&quot;#errmsg_occupation&quot;).hide();toggleHint_new(&quot;show&quot;, this.name, document.getElementById(&quot;occupation&quot;),document.getElementById(&quot;occupation&quot;).offsetWidth)" onchange="validate_occupation();" onblur="validate_occupation();" style="margin-left:0px;width:286px">
    <option value="" label="Select" selected="selected">Select</option>
    <optgroup label="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-- ADMIN --" class="a"><option value="Manager">&nbsp; Manager</option><option value="Supervisor">&nbsp; Supervisor</option><option value="Officer">&nbsp; Officer</option><option value="Administrative Professional">&nbsp; Administrative Professional</option><option value="Executive">&nbsp; Executive</option><option value="Clerk">&nbsp; Clerk</option></optgroup><optgroup label="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-- AGRICULTURE --" class="a"><option value="Agriculture Farming Professional">&nbsp; Agriculture &amp; Farming Professional</option></optgroup><optgroup label="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-- AIRLINE --" class="a"><option value="Pilot">&nbsp; Pilot</option><option value="Air Hostess">&nbsp; Air Hostess</option><option value="Airline Professional">&nbsp; Airline Professional</option></optgroup><optgroup label="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-- ARCHITECT &amp; DESIGN --" class="a"><option value="Architect">&nbsp; Architect</option><option value="Interior Designer">&nbsp; Interior Designer</option></optgroup><optgroup label="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-- BANKING &amp; FINANCE --" class="a"><option value="Chartered Accountant">&nbsp; Chartered Accountant</option><option value="Company Secretary">&nbsp; Company Secretary</option><option value="Accounts/Finance Professional">&nbsp; Accounts/Finance Professional</option><option value="Banking Service Professional">&nbsp; Banking Service Professional</option><option value="Auditor">&nbsp; Auditor</option></optgroup><optgroup label="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-- BEAUTY &amp; FASHION --" class="a"><option value="Fashion Designer">&nbsp; Fashion Designer</option><option value="Beautician">&nbsp; Beautician</option></optgroup><optgroup label="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-- CIVIL SERVICES --" class="a"><option value="Civil Services (IAS/IPS/IRS/IES/IFS)">&nbsp; Civil Services (IAS/IPS/IRS/IES/IFS)</option></optgroup><optgroup label="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-- DEFENCE --" class="a"><option value="Army">&nbsp; Army</option><option value="Navy">&nbsp; Navy</option><option value="Airforce">&nbsp; Airforce</option></optgroup><optgroup label="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-- EDUCATION --" class="a"><option value="Professor / Lecturer">&nbsp; Professor / Lecturer</option><option value="Teaching / Academician">&nbsp; Teaching / Academician</option><option value="Education Professional">&nbsp; Education Professional</option></optgroup><optgroup label="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-- HOSPITALITY --" class="a"><option value="Hotel / Hospitality Professional">&nbsp; Hotel / Hospitality Professional</option></optgroup><optgroup label="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-- IT &amp; ENGINEERING --" class="a"><option value="Software Professional">&nbsp; Software Professional</option><option value="Hardware Professional">&nbsp; Hardware Professional</option><option value="Engineer - Non IT">&nbsp; Engineer - Non IT</option></optgroup><optgroup label="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-- LEGAL --" class="a"><option value="Lawyer Legal Professional">&nbsp; Lawyer &amp; Legal Professional</option></optgroup><optgroup label="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-- LAW ENFORCEMENT --" class="a"><option value="Law Enforcement Officer">&nbsp; Law Enforcement Officer</option></optgroup><optgroup label="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-- MEDICAL --" class="a"><option value="Doctor">&nbsp; Doctor</option><option value="Health Care Professional">&nbsp; Health Care Professional</option><option value="Paramedical Professional">&nbsp; Paramedical Professional</option><option value="Nurse">&nbsp; Nurse</option></optgroup><optgroup label="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-- MARKETING &amp; SALES --" class="a"><option value="Marketing Professional">&nbsp; Marketing Professional</option><option value="Sales Professional">&nbsp; Sales Professional</option></optgroup><optgroup label="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-- MEDIA &amp; ENTERTAINMENT --" class="a"><option value="Journalist">&nbsp; Journalist</option><option value="Media Professional">&nbsp; Media Professional</option><option value=" Entertainment Professional">&nbsp; Entertainment Professional</option><option value="Event Management Professional">&nbsp; Event Management Professional</option><option value="Advertising / PR Professional">&nbsp; Advertising / PR Professional</option></optgroup><optgroup label="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-- MERCHANT NAVY --" class="a"><option value="Mariner / Merchant Navy">&nbsp; Mariner / Merchant Navy</option></optgroup><optgroup label="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-- SCIENTIST --" class="a"><option value="Scientist / Researcher">&nbsp; Scientist / Researcher</option></optgroup><optgroup label="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-- TOP MANAGEMENT --" class="a"><option value="CXO / President, Director, Chairman">&nbsp; CXO / President, Director, Chairman</option></optgroup><optgroup label="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-- OTHERS --" class="a"><option value="Consultant">&nbsp; Consultant</option><option value="Customer Care Professional">&nbsp; Customer Care Professional</option><option value="Social Worker">&nbsp; Social Worker</option><option value="Sportsman">&nbsp; Sportsman</option><option value="Technician">&nbsp; Technician</option><option value="Arts Craftsman">&nbsp; Arts &amp; Craftsman</option><option value="Not Working">&nbsp; Not Working</option></optgroup>	
</select>                    </div>
                    <div id="errmsg_occupation" class="error" style="display:none;width:200px">
Occupation is mandatory.</div><br>
                    
            <!-- HINT STARTS HERE -->
            <span class="hint" id="hint_occupation" style="display:none;">
            <div>
                <div class="top_hint"></div><div class="middle_hint">Please specify profession area of the person looking to get married.<br><br></div><div class="bottom_hint"></div>
            </div>
            <div class="arrow_hint"></div>
            </span>
            <!-- HINT ENDS HERE -->
                            <!-- Profession / Industry END -->

                    <div style="padding:5px"></div>
					 <label class="label-180" for="annualincome">Annual Income <span class="mandatory"></span></label>
                    <div id="ai_div" class="select_error">
                        
<select name="annualincome" id="annualincome" class="select1" onblur="validate_annualincome();" onfocus="$(&quot;#errmsg_annualincome&quot;).hide();toggleHint_new(&quot;show&quot;, this.name, document.getElementById(&quot;annualincome&quot;),document.getElementById(&quot;annualincome&quot;).offsetWidth)" style="margin-left:0px;">
    <option value="" label="Select" selected="selected">Select</option>
    <option value="Not applicable" label="Not applicable">Not applicable</option>
    <option value="Less than Rs 50 thousand" label="Less than Rs 50 thousand">Less than Rs 50 thousand</option>
    <option value="Rs 50 thousand to 1 lakh" label="Rs 50 thousand to 1 lakh">Rs 50 thousand to 1 lakh</option>
    <option value="Rs 1 lakh to 2 lakhs" label="Rs 1 lakh to 2 lakhs">Rs 1 lakh to 2 lakhs</option>
    <option value="Rs 2 lakhs to 3 lakhs" label="Rs 2 lakhs to 3 lakhs">Rs 2 lakhs to 3 lakhs</option>
    <option value="Rs 3 lakhs to 4 lakhs" label="Rs 3 lakhs to 4 lakhs">Rs 3 lakhs to 4 lakhs</option>
    <option value="Rs 4 lakhs to 5 lakhs" label="Rs 4 lakhs to 5 lakhs">Rs 4 lakhs to 5 lakhs</option>
    <option value="Rs 5 lakhs to 7 lakhs" label="Rs 5 lakhs to 7 lakhs">Rs 5 lakhs to 7 lakhs</option>
    <option value="Rs 7 lakhs to 10 lakhs" label="Rs 7 lakhs to 10 lakhs">Rs 7 lakhs to 10 lakhs</option>
    <option value="Rs 10 lakhs to 15 lakhs" label="Rs 10 lakhs to 15 lakhs">Rs 10 lakhs to 15 lakhs</option>
    <option value="Rs 15 lakhs to 20 lakhs" label="Rs 15 lakhs to 20 lakhs">Rs 15 lakhs to 20 lakhs</option>
    <option value="Rs 20 lakhs to 30 lakhs" label="Rs 20 lakhs to 30 lakhs">Rs 20 lakhs to 30 lakhs</option>
    <option value="Rs 30 lakhs and above" label="Rs 30 lakhs and above">Rs 30 lakhs and above</option>
    <option value="Dont want to specify" label="Dont want to specify">Dont want to specify</option>
</select>                    </div>
                    <div id="errmsg_annualincome" class="error" style="display:none"></div><br>
					
            <!-- HINT STARTS HERE -->
			
            <span class="hint" id="hint_annualincome" style="display:none;">
            <div>
                <div class="top_hint"></div><div class="middle_hint">Please select annual income of the person looking to get married.<br><br></div><div class="bottom_hint"></div>
            </div>
            <div class="arrow_hint"></div>
            </span>
			<div style="padding:5px"></div>
			<h6 class="dot">Habits</h6>
			<label class="label-180" for="diet">Diet</label>
                  <span class="fl">
<label for="diet-Veg"><input type="radio" name="diet" id="diet-Veg" value="Veg" class="radio" onclick="$(&quot;#errmsg_diet&quot;).html(&quot;&quot;).hide();">Veg</label> <label for="diet-NonVeg"><input type="radio" name="diet" id="diet-NonVeg" value="Non-Veg" class="radio" onclick="$(&quot;#errmsg_diet&quot;).html(&quot;&quot;).hide();">Non-Veg</label> <label for="diet-OccasionallyNonVeg"><input type="radio" name="diet" id="diet-OccasionallyNonVeg" value="Occasionally Non-Veg" class="radio" onclick="$(&quot;#errmsg_diet&quot;).html(&quot;&quot;).hide();">Occasionally Non-Veg</label> <label for="diet-Eggetarian"><input type="radio" name="diet" id="diet-Eggetarian" value="Eggetarian" class="radio" onclick="$(&quot;#errmsg_diet&quot;).html(&quot;&quot;).hide();">Eggetarian</label> <label for="diet-Jain"><input type="radio" name="diet" id="diet-Jain" value="Jain" class="radio" onclick="$(&quot;#errmsg_diet&quot;).html(&quot;&quot;).hide();">Jain</label> <label for="diet-Vegan"><input type="radio" name="diet" id="diet-Vegan" value="Vegan" class="radio" onclick="$(&quot;#errmsg_diet&quot;).html(&quot;&quot;).hide();">Vegan</label></span>
                    
                    <div id="errmsg_diet" class="error" style="display:none"></div><br>

                    <div style="padding:5px"></div>
					<div style="padding:5px"></div>
                    

                    <label class="label-180" for="smoke">Smoke <span class="mandatory">*</span></label>
                        <span class="fl">
<label for="smoke-No"><input type="radio" name="smoke" id="smoke-No" value="No" class="radio" onclick="$(&quot;#errmsg_smoke&quot;).html(&quot;&quot;).hide();">No</label> <label for="smoke-Occasionally"><input type="radio" name="smoke" id="smoke-Occasionally" value="Occasionally" class="radio" onclick="$(&quot;#errmsg_smoke&quot;).html(&quot;&quot;).hide();">Occasionally</label> <label for="smoke-Yes"><input type="radio" name="smoke" id="smoke-Yes" value="Yes" class="radio" onclick="$(&quot;#errmsg_smoke&quot;).html(&quot;&quot;).hide();">Yes</label></span>
                    <div id="errmsg_smoke" class="error" style="display:none"></div><br>

                    <div style="padding:5px"></div>
 <label class="label-180" for="drink">Drink <span class="mandatory">*</span></label>
                        <span class="fl">
<label for="drink-No"><input type="radio" name="drink" id="drink-No" value="No" class="radio" onclick="$(&quot;#errmsg_drink&quot;).html(&quot;&quot;).hide();">No</label> <label for="drink-Occasionally"><input type="radio" name="drink" id="drink-Occasionally" value="Occasionally" class="radio" onclick="$(&quot;#errmsg_drink&quot;).html(&quot;&quot;).hide();">Occasionally</label> <label for="drink-Yes"><input type="radio" name="drink" id="drink-Yes" value="Yes" class="radio" onclick="$(&quot;#errmsg_drink&quot;).html(&quot;&quot;).hide();">Yes</label></span>
                    <div id="errmsg_drink" class="error" style="display:none"></div><br>

                    <div style="padding:5px"></div>

			<h6 class="dot">Astrological Info</h6>
			 <label class="label-180" for="diet">Manglik</label>
			<span class="fl">
			<input type="radio" name="manglik" value="Yes"/> Yes &nbsp;&nbsp;
			<input type="radio" name="manglik" value="No"/> No &nbsp;&nbsp;
			<input type="radio" name="manglik" value="Don't know"/> Don't know  
			</span>
<br/>
  <div style="padding:5px"></div>
<label class="label-180" >
Star
</label>
			<span class="fl">
<select class="selectfield" id="STAR" name="STAR" style="width: 250px;">
									<option selected="selected" value="0">- optional -</option>
									<option  style="">Anuradha / Anusham / Anizham</option><option  style="">Ardra / Thiruvathira</option><option  style="">Ashlesha / Ayilyam</option><option  style="">Ashwini / Ashwathi</option><option  style="">Bharani</option><option  style="">Chitra / Chitha</option><option  style="">Dhanista / Avittam</option><option  style="">Hastha / Atham</option><option  style="">Jyesta / Kettai / Thrikketa</option><option  style="">Krithika / Karthika</option><option  style="">Makha / Magam</option><option  style="">Moolam / Moola</option><option  style="">Mrigasira / Makayiram</option><option  style="">Poorvabadrapada / Puratathi</option><option  style="">Poorvapalguni / Puram / Pubbhe</option><option  style="">Poorvashada / Pooradam</option><option  style="">Punarvasu / Punarpusam</option><option  style="">Pushya / Poosam / Pooyam</option><option  style="">Revathi</option><option  style="">Rohini</option><option  style="">Shatataraka / Sadayam / Satabishek</option><option  style="">Shravan / Thiruvonam</option><option  style="">Swati / Chothi</option><option  style="">Uttarabadrapada / Uthratadhi</option><option  style="">Uttarapalguni / Uthram</option><option  style="">Uttarashada / Uthradam</option><option  style="">Vishaka / Vishakam</option>								</select>
									</span>
									<br/>
			<div style="padding:5px"></div>
			<label class="label-180" >

Raasi/moon sign

</label>
			<span class="fl">
			<select class="selectfield" id="RAASI" name="RAASI" style="width: 250px;">
									<option selected="selected" value="0">- optional -</option>
									<option  style="">Dhanu (Sagittarius)</option><option  style="">Kanya (Virgo)</option><option  style="">Kark (Cancer)</option><option  style="">Kumbh (Aquarius)</option><option  style="">Makar (Capricorn)</option><option  style="">Meen (Pisces)</option><option  style="">Mesh (Aries)</option><option  style="">Mithun (Gemini)</option><option  style="">Simha (Leo)</option><option style="">Tula (Libra)</option><option  style="">Vrishabh (Taurus)</option><option  style="">Vrishchik (Scorpio)</option>	
								</select>
								</span>
								<br/>
								<div style="padding:5px"></div>
								<label class="label-180" >

Dosham

</label>
			<span class="fl">
			<input type="text" name="dosham" id="dosham"/>
								</span>
			
                   <h6 class="dot">Family Profile</h6>
				   
                    <label class="label-180" for="personal_values">Family status </label>
                    <span class="fl">
<label for="personal_values-Traditional"><input type="radio" name="personal_values" id="personal_values-Traditional" value="Middle class" class="radio" onclick="$(&quot;#errmsg_personal_values&quot;).html(&quot;&quot;).hide();">Middle class</label> <label for="personal_values-Moderate"><input type="radio" name="personal_values" id="personal_values-Moderate" value="Upper middle class" class="radio" onclick="$(&quot;#errmsg_personal_values&quot;).html(&quot;&quot;).hide();" checked="checked">Upper middle class</label> <label for="personal_values-Liberal"><input type="radio" name="personal_values" id="personal_values-Liberal" value="Rich" class="radio" onclick="$(&quot;#errmsg_personal_values&quot;).html(&quot;&quot;).hide();">Rich</label><label for="personal_values-Liberal"><input type="radio" name="personal_values" id="personal_values-Liberal" value="Affluent" class="radio" onclick="$(&quot;#errmsg_personal_values&quot;).html(&quot;&quot;).hide();">Affluent</label></span>
                    <div id="errmsg_personal_values" class="error" style="display:none;width:170px;"></div> <br>

               
            <!-- Lifestyle END -->

            <div style="padding:5px"></div>

 <label class="label-180" >Family type  </label>
                    <span class="fl">
<label for="personal_values-Traditional"><input type="radio" name="family_type" id="family_type" value="Joint" class="radio" >Joint</label> <label for="personal_values-Moderate"><input type="radio" name="family_type" id="family_type" value="Nuclear" class="radio" checked="checked">    Nuclear

</label> </span>
                    <div id="errmsg_personal_values" class="error" style="display:none;width:170px;"></div> <br>

               
            <!-- Lifestyle END -->

            <div style="padding:5px"></div>
			
 <label class="label-180" >Family values  </label>
                    <span class="fl">
<label for="personal_values-Traditional"><input type="radio" name="family_value" id="family_value" value="Orthodox" class="radio" >Orthodox</label> <label for="personal_values-Moderate"><input type="radio" name="family_value" id="family_value" value="Traditional" class="radio" >Traditional

</label> <label for="personal_values-Moderate"><input type="radio" name="family_value" id="family_value" value="Moderate" class="radio" checked="checked">Moderate

</label><label for="personal_values-Moderate"><input type="radio" name="family_value" id="family_value" value="Liberal" class="radio" >    Liberal



</label> </span>
                    <div id="errmsg_personal_values" class="error" style="display:none;width:170px;"></div> <br>

               
            <!-- Lifestyle END -->

            <div style="padding:5px"></div>
            <!-- Complexion -->
           
            <!-- Complexion END -->
						

			<!-- RESIDENCE -->

             

                    
            <!-- HINT ENDS HERE -->
        
                    
            <!-- HINT ENDS HERE -->
        
                   
               
<input type="hidden" name="residencystatus" value="-" id="residencystatus">                </div>
            </div>
           
                            <!-- Education Area END -->

            

                    
					
         
                   
                  
                    

                    <label class="label-180" for="aboutyourself"><h6 class="dot">Description  <span class="mandatory">*</span></h6></label>
     <span style="color:#FE6C00;float:left;font:normal 12px arial;margin-top:2px;*margin-top:0px">Describe yourself, likes &amp; dislikes, kind of person you are looking for, etc.</span><br clear="all">
                 
<textarea name="aboutyourself" id="aboutyourself" class="" wrap="virtual" onkeyup="calcCharLen(&quot;frm_profile&quot;, &quot;aboutyourself&quot;, &quot;counter1&quot;, 4000);" onblur="calcCharLen(&quot;frm_profile&quot;, &quot;aboutyourself&quot;, &quot;counter1&quot;, 4000);validate_aboutyourself();" onfocus="$(&quot;errmsg_aboutyourself&quot;).hide();" maxlength="4000" style="width:479px;height:75px;font:normal 13px arial;color:#434343;border:1px solid #BDD6A8;y-overflow:scroll;x-overflow:none;background:#F7F7F7 " rows="24" cols="80"></textarea>                    <br>

                    <div style="padding:2px"></div>

                    <div style="margin-left:0px;_margin-left:193px;width:467px;*width:469px;font:normal 13px arial;color:#434343;padding:0px 6px;border:solid 1px #D4D4D4">
                        <div style="float:left;padding-top:4px">(min. 50 characters; max. 4000)</div>
                        <span style="display:block;text-align:right"><input type="text" name="counter1" id="counter1" value="0" readonly="" style="width:45px;font:normal 18px arial;color:#C0C0C0;text-align:right;background:none;border:none"></span>
                        <span style="line-height:2px"><br clear="all"></span>
                    </div>

                    <div id="errmsg_aboutyourself" class="error" style="display:none;width:450px"></div><br/>
					<br/>
               
            			
						<input type="image" src="<?php echo base_url(); ?>system/pyrocms/themes/default/img/crea-profile.gif" onclick="javascript:validate_error(&#39;SH91017969&#39;); return false;"/>
					<!--	<img src="<?php echo base_url(); ?>system/pyrocms/themes/default/img/crea-profile.gif" style="cursor:pointer;border:none;margin:15px 0px 0px 207px" width="234" border="0" height="43" alt="Create My Profile" title="Create My Profile" onclick="javascript:validate_error(&#39;SH91017969&#39;); return false;"></a>
			 Describe yourself END -->
            
		
<input type="hidden" name="btn" value="2" id="btn">		
<input type="hidden" name="profession_grp_flag" value="2" id="profession_grp_flag">      </div>
           </form>  
		    </div> </div>

 
			 
            

</body>

</html>

