<?php ob_start();
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Phoneview extends CI_Controller 
{//**********************************
//   member phone number view by me
//***********************************
function member_phonenumber_viewedbyme()
{ 
$this->load->model('message_model');
$data['query']=$this->message_model->memberphonenumberviewedbyme();	
$data['heading']= "Members phone number viewed by me";
$this->load->view('header');
$this->load->view('phoneview',$data);
$this->load->view('footer');  
} 
//**********************************
//   member whoe view my phone number  
//***********************************
function member_viewed_my_phonenumber()
{ 	
$this->load->model('message_model');
$data['query']=$this->message_model->memberviewedmyphonenumber();	
$data['heading']= "Members who viewed  my phone number";
$this->load->view('header');
$this->load->view('phoneview',$data);
$this->load->view('footer');   
} 
}?>