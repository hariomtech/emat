<?php 
ob_start();
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Message extends CI_Controller 
{

public function index()
{ 	
   $this->load->helper('users/user');	
 $this->load->model('message_model','',TRUE);
		$this->load->view('header');   
	$data['query']= $this->message_model->search();
	if(isset($_POST['decline']) or isset($_POST['decline2']))
	{
		$this->message_model->mail_decline();
	}	

 $this->load->view('message',$data);
 $this->load->view('footer');
}



public function sorttype()
{
  $this->load->view('header');
    $this->load->model('search_model','',TRUE);
	$data1['query']= $this->search_model->sorttype();
   $this->load->view('search',$data1);
   $this->load->view('footer');

}

 public function searchbyprofile()
{
 	$this->load->view('header');
    $this->load->model('search_model','',TRUE);
	$data['query']= $this->search_model->searchbyid();
    $this->load->view('search',$data);
    $this->load->view('footer');
} 

 public function profile($getID)
   { $this->load->helper('users/user');
   $this->load->model('profile_model','',TRUE);
 
	$data['query']= $this->profile_model->getprofile($getID);
	
	$this->load->view('header');
	$this->load->view('profileview',$data);
	$this->load->view('footer');
   }
   
     public function regular()
   {   
     $this->load->model('register_model','',TRUE);
    $data['query']= $this->register_model->getcountry();
	$this->load->view('header');
	$this->load->view('regular',$data);
	$this->load->view('footer');
   } 
   
    public function search1()
   {   
   $this->load->model('searchnew_model','',TRUE);
    $data['query']= $this->searchnew_model->searchresult();
  
	$this->load->view('header');
	$this->load->view('search',$data);
	$this->load->view('footer');
   } 
   
   
//********************     
//********************
// PERSONALISED MESSAGE

function newmsgreceive()
{
$this->load->model('message_model');
$data['query']=$this->message_model->totalmsgreceive();	
$data['query1']=$this->message_model->allmsgreceive();	

$data['heading']= "New message received";
$this->load->view('header');
if(isset($_POST['decline']) or isset($_POST['decline2']))
	{
		$this->message_model->mail_decline();
	}	

if($_POST['replymail'])
	{
		$this->message_model->mail_insert();
		$this->message_model->personalmail();
	}
	if($_POST['status']=="read")
	{
		$this->message_model->mail_read();
	}		
	if($_POST['delyes'])
	{
	$this->message_model->mail_del();
	}
$data['query']=$this->message_model->totalmsgreceive();		

$this->load->view('message',$data);
$this->load->view('footer');
}

function awaitingmyreply()
{
$this->load->model('message_model');

if(isset($_POST['decline']) or isset($_POST['decline2']))
	{
		$this->message_model->mail_decline();
	}	

$data['query']=$this->message_model->totalreplyleft();	
$data['heading']= "Awaiting my reply";
$this->load->view('header');
$this->load->view('message',$data);
$this->load->view('footer');
}
 
function messagereplied()
{
$this->load->model('message_model');
$data['query']=$this->message_model->totalreplied();	
$data['heading']= "Replied messages";
$this->load->view('header');
$this->load->view('message',$data);
$this->load->view('footer');
}
 function declinemessage()
{
$this->load->model('message_model');
$data['query']=$this->message_model->totaldecline();	
$data['heading']= "Messages declined";
$this->load->view('header');
$this->load->view('message',$data);
$this->load->view('footer');
}
function creplyreceived()
{
$this->load->model('message_model');
$data['query']=$this->message_model->replyreceived();	
$data['heading']= "Replies Received";
$this->load->view('header');
$this->load->view('message',$data);
$this->load->view('footer');
}


function readbymembers()
{
$this->load->model('message_model');
$data['query']=$this->message_model->msgreaded();	
$data['heading']= "Read by members";
$this->load->view('header');
$this->load->view('message',$data);
$this->load->view('footer');
}

function unreadbymembers()
{
$this->load->model('message_model');
$data['query']=$this->message_model->msgunreaded();	
$data['heading']= "Read by members";
$this->load->view('header');
$this->load->view('message',$data);
$this->load->view('footer');
}


function declinebymember()
{
$this->load->model('message_model');
$data['query']=$this->message_model->msgdecline();	
$data['heading']= "Declined by members";
$this->load->view('header');
$this->load->view('message',$data);
$this->load->view('footer');
}

 
 
 
 
  
// EXPRESS INTEREST 
function expressinterest()
{
$this->load->model('message_model');
$data['query']=$this->message_model->eetotalnewmsg();	
$data['heading']= "New interests from members";
$this->load->view('header');
$this->load->view('message',$data);
$this->load->view('footer');
}

function interstacceptbyme()
{
$this->load->model('message_model');
$data['query']=$this->message_model->eetotalaccept();	
$data['heading']= "Interests accepted by me";
$this->load->view('header');
$this->load->view('message',$data);
$this->load->view('footer');
}

function interestdeclinebyme()
{
$this->load->model('message_model');
$data['query']=$this->message_model->eetotaldecline();	
$data['heading']= "Interests declined by me";
$this->load->view('header');
$this->load->view('message',$data);
$this->load->view('footer');
}

function eiaccept()
{
$this->load->model('message_model');
$data['query']=$this->message_model->eesentinterestaccept();	
$data['heading']= "Express Interest accept by members";
$this->load->view('header');
$this->load->view('message',$data);
$this->load->view('footer');
}

function eipendinginterest()
{
$this->load->model('message_model');
$data['query']=$this->message_model->eependinginterst();	
$data['heading']= "Reply pending from members";
$this->load->view('header');
$this->load->view('message',$data);
$this->load->view('footer');
}


function declinedbymember()
{
$this->load->model('message_model');
$data['query']=$this->message_model->eeinterestdeclined();	
$data['heading']= "Declined by members";
$this->load->view('header');
$this->load->view('message',$data);
$this->load->view('footer');
}





//********************
//********************
// REQUEST

function phonerequestreceive()
{
$this->load->model('message_model');

$data['heading']= "Phone request received";

if($_POST['delphoneyes'] || $_POST['delallphotoyes'])
{	
$this->message_model->phonerequest_del();
}	
$data['query']=$this->message_model->rphonerequest();	
$this->load->view('header');
$this->load->view('message',$data);
$this->load->view('footer');
}

function photorequestreceive()
{
$this->load->model('message_model');
if($_POST['delphotoyes'] || $_POST['delallphotoyes'])
{	
$this->message_model->photorequest_del();
}	
$data['query1']=$this->message_model->rphotorequest();	
$data['query']=$this->message_model->rphotorequest();	
$data['heading']= "Photo request received";
$this->load->view('header');
$this->load->view('message',$data);
$this->load->view('footer');
}

function hororequestreceive()
{
$this->load->model('message_model');
$data['query']=$this->message_model->rhoroscoperequest();	
$data['heading']= "Horoscope request received";
$this->load->view('header');
$this->load->view('message',$data);
$this->load->view('footer');
}

function refrequestreceive()
{
$this->load->model('message_model');

if($_POST['delreferenceyes'] || $_POST['delallphotoyes'])
{	
$this->message_model->referencerequest_del();
}

$data['query']=$this->message_model->rreferencerequest();	
$data['heading']= "Reference request received";
$this->load->view('header');
$this->load->view('message',$data);
$this->load->view('footer');
}

function phonerequestsent()
{
$this->load->model('message_model');

if($_POST['delphoneyes'] || $_POST['delallphotoyes'])
{	
$this->message_model->phonerequest_del();
}	

$data['query']=$this->message_model->sphonerequest();	
$data['heading']= "Phone request sent";
$this->load->view('header');
$this->load->view('message',$data);
$this->load->view('footer');
}

function photorequestsent()
{
$this->load->model('message_model');

if($_POST['delphotoyes'] || $_POST['delallphotoyes'])
{	
$this->message_model->photorequest_del();
}	

$data['query']=$this->message_model->sphotorequest();	
$data['heading']= "Photo request sent";
$this->load->view('header');
$this->load->view('message',$data);
$this->load->view('footer');
}

function hororequestsent()
{
$this->load->model('message_model');
$data['query']=$this->message_model->shoroscoperequest();	
$data['heading']= "Horoscope request received";
$this->load->view('header');
$this->load->view('message',$data);
$this->load->view('footer');
}

function refrequestsent()
{
$this->load->model('message_model');

if($_POST['delreferenceyes'] || $_POST['delallphotoyes'])
{	
$this->message_model->referencerequest_del();
}


$data['query']=$this->message_model->sreferencerequest();	
$data['heading']= "Reference request received";
$this->load->view('header');
$this->load->view('message',$data);
$this->load->view('footer');
}

function shorlistmembers()
{
$this->load->model('message_model');
$data['query']=$this->message_model->shortlist();	
$data['heading']= "Shortlisted members by me";
$this->load->view('header');
$this->load->view('message',$data);
$this->load->view('footer');
}

//********************
//LISTS AND VIEWS
//********************
//   members i ignored
//********************
function members_ignored_byme()
{ 		
$this->load->model('message_model');
$data['query']=$this->message_model->membersignoredbyme();	
$data['heading']= "Members ignored by me";
$this->load->view('header');
$this->load->view('message',$data);
$this->load->view('footer');
} 
//********************
//   member i blocked
//********************
function members_blocked_byme()
{ 		
$this->load->model('message_model');

if($_POST['unblockyes'] || $_POST['unblockallyes'])
{	
$this->message_model->unblockyes();
}

$data['query1']=$this->message_model->memberblockedbyme();

$data['query']=$this->message_model->memberblockedbyme();	
$data['heading']= "Members blocked by me";
$this->load->view('header');
$this->load->view('message',$data);
$this->load->view('footer'); 
} 
//**********************************
//   member phone number view by me
//***********************************
function member_phonenumber_viewedbyme()
{ 	
$this->load->model('message_model');
$data['query']=$this->message_model->memberphonenumberviewedbyme();	
$data['heading']= "Members phone number viewed by me";
$this->load->view('header');
$this->load->view('message',$data);
$this->load->view('footer');  
} 
//**********************************
//   member whoe view my phone number  
//***********************************
function member_viewed_my_phonenumber()
{ 	
$this->load->model('message_model');
$data['query']=$this->message_model->memberviewedmyphonenumber();	
$data['heading']= "Members who viewed  my phone number";
$this->load->view('header');
$this->load->view('message',$data);
$this->load->view('footer');   
} 
//**********************************
//   member whoe view my profile
//***********************************
function members_viewed_my_profile()
{ 		
$this->load->model('message_model');
$data['query']=$this->message_model->membersviewedmyprofile();	
$data['heading']= "Members who viewed  my profile";
$this->load->view('header');
$this->load->view('message',$data);
$this->load->view('footer');    
} 

}?>