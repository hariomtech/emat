<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @author 		Phil Sturgeon - PyroCMS Development Team
 * @package 	PyroCMS
 * @subpackage 	Users Module
 * @since		v0.1
 *
 */
class Searchnew_Model extends MY_Model {
	
	/**
	 * Get a user profile
	 *
	 * @access public
	 * @param array $params Parameters used to retrieve the profile
	 * @return object
	 */
	//priya 
	public function searchresultno()
{
$gender=$_POST['GENDER'];
$minage=(date('Y')-$_POST['STAGE'])."-".date('m')."-".date('d');
$maxage=(date('Y')-($_POST['ENDAGE']+1))."-".date('m')."-".date('d');
$minh=$_POST['STHEIGHT'];
$maxh=$_POST['ENDHEIGHT'];
$marital1=$_POST['MARITAL_STATUS'];
for($i=0;$i<count($marital1);$i++)
{
if($i!=0)
{
$marital.=" or ";
$marital2.=",";
}
$marital.="maritalstatus='".$marital1[$i]."'";
$marital2.=$marital1[$i];
}

$religion=$_POST['community'];

$mothertangue1=explode(",",$_POST['mothertangue']);

for($i=0;$i<count($mothertangue1)&&$mothertangue1[$i]&&!in_array("Any", $mothertangue1);$i++)
{
if($i!=0)
{
$mothertangue.=" or ";
$mothertangue2.=",";
}
$mothertangue.="mother_tongue='".$mothertangue1[$i]."'";
$mothertangue2.=$mothertangue1[$i];
}
$caste1=explode(",",$_POST['caste']);
for($i=0;$i<count($caste1)&& $caste1[$i]&&!in_array("Any", $caste1) ;$i++)
{
if($i!=0)
{
$caste.=" or ";
$caste2.=",";
}
$caste.="caste='".$caste1[$i]."'";
$caste2.=$caste1[$i];
}

$COUNTRYRIGHT1=explode(",",$_POST['COUNTRYRIGHT']);
for($i=0;$i<count($COUNTRYRIGHT1)&& $COUNTRYRIGHT1[$i]&&!in_array("Any", $COUNTRYRIGHT1);$i++)
{
if($i!=0)
{
$country.=" or ";
$country2.=",";
}
$country.="country='".$COUNTRYRIGHT1[$i]."'";
$country2.= $COUNTRYRIGHT1[$i];
}
 
$EDUCATIONRIGHT=explode(",",$_POST['EDUCATIONRIGHT']);
for($i=0;$i<count($EDUCATIONRIGHT)&&$EDUCATIONRIGHT[$i]&&!in_array("Any", $EDUCATIONRIGHT);$i++)
{
if($i!=0)
{
$educationfield.=" or ";
$educationfield2.=",";
}
$educationfield.="education='".$EDUCATIONRIGHT[$i]."'";
$educationfield2.=$EDUCATIONRIGHT[$i];
}

$str="select * from users as u, profiles as p where u.active=1 and u.activation_code = '' and u.group_id =2 and  u.id = p.user_id  and p.activestatus=1";
if($gender)
{
$str.=" and gender='".$gender."'";
}
if($minage && $maxage)
{
$str.=" and dob<='".$minage."'";
}
if($maxage)
{
$str.=" and dob>'".$maxage."'";
}
if($marital)
{
$str.=" and (".$marital.")";
}
if($religion)
{
$str.=" and religion='".$religion."'";
}
if($mothertangue)
{
$str.= " and (".$mothertangue.")";
}
if($caste)
{
$str.=" and (".$caste.")";
}
if($country)
{
$str.=" and (".$country.")";
}
if($educationfield)
{
$str.=" and (".$educationfield.")";
}
if($_POST['PHOTO_OPT']=='Y')
{
$withpic='yes';
$str.=" and (photo1!='' or photo2!='' or photo3!='' )";
}

if($_POST['STHEIGHT'])
{
$str.=" and height>='".$_POST['STHEIGHT']."'";
}
if($_POST['ENDHEIGHT'])
{
$str.=" and height<='".$_POST['ENDHEIGHT']."'";
}
$newdata = array(
                   'gender'  => $gender,
                   'age'     => $_POST['STAGE']." - ".$_POST['ENDAGE'],
                   'country' => $country2,
				   'religion' => $religion,
				   'caste' => $caste2,
				   'withpic' =>$withpic
				   
               );
$this->session->set_userdata($newdata); 
//echo $str;
// gender='".$gender."' and dob<='".$minage."' and dob>='".$maxage."' and maritalstatus in(".$marital.") and religion='".$religion."' and mother_tongue='".$mothertangue."' and caste in(".$caste.") and country in(".$COUNTRYRIGHT.") and educationfield in(".$EDUCATIONRIGHT.")";


if(isset($_SESSION['regular']))
{
$str=$_SESSION['regular'];
}

$query= mysql_query($str)or die(mysql_error());
$no=mysql_num_rows($query);
//echo mysql_num_rows($query);
//return $query;
$_SESSION['regular']=$str;
return $no;  	
}

public function searchresult($num, $offset)
{
$gender=$_POST['GENDER'];
$minage=(date('Y')-$_POST['STAGE'])."-".date('m')."-".date('d');
$maxage=(date('Y')-($_POST['ENDAGE']+1))."-".date('m')."-".date('d');
$minh=$_POST['STHEIGHT'];
$maxh=$_POST['ENDHEIGHT'];
$marital1=$_POST['MARITAL_STATUS'];
for($i=0;$i<count($marital1);$i++)
{
if($i!=0)
{
$marital.=" or ";
$marital2.=",";
}
$marital.="maritalstatus='".$marital1[$i]."'";
$marital2.=$marital1[$i];
}

$religion=$_POST['community'];

$mothertangue1=explode(",",$_POST['mothertangue']);

for($i=0;$i<count($mothertangue1)&&$mothertangue1[$i]&&!in_array("Any", $mothertangue1);$i++)
{
if($i!=0)
{
$mothertangue.=" or ";
$mothertangue2.=",";
}
$mothertangue.="mother_tongue='".$mothertangue1[$i]."'";
$mothertangue2.=$mothertangue1[$i];
}
$caste1=explode(",",$_POST['caste']);
for($i=0;$i<count($caste1)&& $caste1[$i]&&!in_array("Any", $caste1) ;$i++)
{
if($i!=0)
{
$caste.=" or ";
$caste2.=",";
}
$caste.="caste='".$caste1[$i]."'";
$caste2.=$caste1[$i];
}

$COUNTRYRIGHT1=explode(",",$_POST['COUNTRYRIGHT']);
for($i=0;$i<count($COUNTRYRIGHT1)&& $COUNTRYRIGHT1[$i]&&!in_array("Any", $COUNTRYRIGHT1);$i++)
{
if($i!=0)
{
$country.=" or ";
$country2.=",";
}
$country.="country='".$COUNTRYRIGHT1[$i]."'";
$country2.= $COUNTRYRIGHT1[$i];
}
 
$EDUCATIONRIGHT=explode(",",$_POST['EDUCATIONRIGHT']);
for($i=0;$i<count($EDUCATIONRIGHT)&&$EDUCATIONRIGHT[$i]&&!in_array("Any", $EDUCATIONRIGHT);$i++)
{
if($i!=0)
{
$educationfield.=" or ";
$educationfield2.=",";
}
$educationfield.="education='".$EDUCATIONRIGHT[$i]."'";
$educationfield2.=$EDUCATIONRIGHT[$i];
}

$str="select * from users as u, profiles as p where u.active=1 and u.activation_code = '' and u.group_id =2 and  u.id = p.user_id  and p.activestatus=1";
if($gender)
{
$str.=" and gender='".$gender."'";
}
if($minage && $maxage)
{
$str.=" and dob<='".$minage."'";
}
if($maxage)
{
$str.=" and dob>'".$maxage."'";
}
if($marital)
{
$str.=" and (".$marital.")";
}
if($religion)
{
$str.=" and religion='".$religion."'";
}
if($mothertangue)
{
$str.= " and (".$mothertangue.")";
}
if($caste)
{
$str.=" and (".$caste.")";
}
if($country)
{
$str.=" and (".$country.")";
}
if($educationfield)
{
$str.=" and (".$educationfield.")";
}
if($_POST['PHOTO_OPT']=='Y')
{
$withpic='yes';
$str.=" and (photo1!='' or photo2!='' or photo3!='' )";
}

if($_POST['STHEIGHT'])
{
$str.=" and height>='".$_POST['STHEIGHT']."'";
}
if($_POST['ENDHEIGHT'])
{
$str.=" and height<='".$_POST['ENDHEIGHT']."'";
}
$newdata = array(
                   'gender'  => $gender,
                   'age'     => $_POST['STAGE']." - ".$_POST['ENDAGE'],
                   'country' => $country2,
				   'religion' => $religion,
				   'caste' => $caste2,
				   'withpic' =>$withpic
				   
               );
$this->session->set_userdata($newdata); 
//echo $str;
// gender='".$gender."' and dob<='".$minage."' and dob>='".$maxage."' and maritalstatus in(".$marital.") and religion='".$religion."' and mother_tongue='".$mothertangue."' and caste in(".$caste.") and country in(".$COUNTRYRIGHT.") and educationfield in(".$EDUCATIONRIGHT.")";

//echo $str;
if(!$_POST['submit'] && !$_POST['save'])
{
//echo $str;
$str=$_SESSION['searchr'];
}
if($offset=='')
  {
  $offset=0;
  //$num=5;
  }
$query= $this->db->query($str.' Limit '.$offset.", ".$num)or die(mysql_error());
//echo mysql_num_rows($query);
//return $query;
$_SESSION['searchr']=$str;
return $query->result_array();  	
}

public function saveresult($id)
{
 $gender=$_POST['GENDER'];
$age=$_POST['STAGE']."-".$_POST['ENDAGE'];

$height=$_POST['STHEIGHT'].":".$_POST['ENDHEIGHT'];

$marital1=$_POST['MARITAL_STATUS'];
for($i=0;$i<count($marital1);$i++)
{
if($i!=0)
{
$marital.=",";
}
$marital.=$marital1[$i];
}

$religion=$_POST['community'];

$mothertangue1=explode(",",$_POST['mothertangue']);

for($i=0;$i<count($mothertangue1)&&$mothertangue1[$i];$i++)
{
if($i!=0)
{
$mothertangue.=",";
}
$mothertangue.=$mothertangue1[$i];
}
$caste1=explode(",",$_POST['caste']);
for($i=0;$i<count($caste1)&& $caste1[$i] ;$i++)
{
if($i!=0)
{
$caste.=",";
}
$caste.=$caste1[$i];
}

$COUNTRYRIGHT1=explode(",",$_POST['COUNTRYRIGHT']);
for($i=0;$i<count($COUNTRYRIGHT1)&& $COUNTRYRIGHT1[$i];$i++)
{
if($i!=0)
{
$country.=",";
}
$country.=$COUNTRYRIGHT1[$i];
}
 
$EDUCATIONRIGHT=explode(",",$_POST['EDUCATIONRIGHT']);
for($i=0;$i<count($EDUCATIONRIGHT)&&$EDUCATIONRIGHT[$i];$i++)
{
if($i!=0)
{
$educationfield.=",";
}
$educationfield.=$EDUCATIONRIGHT[$i];
}

 //echo "insert into search(user_id,name,gender,age,height,maritalstatus,religion,mothertongue,caste,country,education,  	showprofile) value('".$id."','".$_POST['name']."','".$gender."','".$age."','".$height."','".$marital."','".$religion."','".$mothertangue."','".$caste."','".$country."','".$educationfield."')";

 $query=mysql_query('select * from search where user_id='.$id." and name='".$_POST['search_name']."' and gender='".$gender."' and age='".$age."' and height='".$height."' and maritalstatus='".$marital."' and religion='".$religion."' and mothertongue='".$mothertangue."' and caste='".$caste."' and country='".$country."' and education='".$educationfield."' and showprofile='".$_POST['PHOTO_OPT']."' and horoscope='".$_POST['HOROSCOPE_OPT']."'")or die(mysql_error());
if(mysql_num_rows($query)<=0)
{
 mysql_query("insert into search(user_id,name,gender,age,height,maritalstatus,religion,mothertongue,caste,country,education,  	showprofile,horoscope,status) value('".$id."','".$_POST['search_name']."','".$gender."','".$age."','".$height."','".$marital."','".$religion."','".$mothertangue."','".$caste."','".$country."','".$educationfield."','".$_POST['PHOTO_OPT']."','".$_POST['HOROSCOPE_OPT']."','Regular')")or die(mysql_error());
 }
 
}

public function getsavesearch($id)
{
$query= $this->db->query('select * from search where user_id='.$id)or die(mysql_error());

return $query->result_array();  	
}

public function getsaverecord($sid,$id)
{

$query= $this->db->query("select * from search where user_id='".$sid."' and search_id='".$id."'")or die(mysql_error());

return $query->result_array();  	

}

public function editsaveresult($id)
{
  $gender=$_POST['GENDER'];
$age=$_POST['STAGE']."-".$_POST['ENDAGE'];

$height=$_POST['STHEIGHT'].":".$_POST['ENDHEIGHT'];

$marital1=implode(",",$_POST['MARITAL_STATUS']);

$religion=$_POST['community'];

$mothertangue1=$_POST['mothertangue'];


$caste1=$_POST['caste'];


$COUNTRYRIGHT1=$_POST['COUNTRYRIGHT'];

$EDUCATIONRIGHT=$_POST['EDUCATIONRIGHT'];
$photo1=$_POST['PHOTO_OPT'];
$id=$_POST['savedid'];
$newdata = array(
                   'gender'  => $gender,
                   'age'     => $_POST['STAGE']." - ".$_POST['ENDAGE'],
                   'country' => $COUNTRYRIGHT1,
				   'religion' => $religion,
				   'caste' => $caste1,
				   'withpic' =>$photo1
				   
               );
$this->session->set_userdata($newdata); 
//echo "update search set name='".$_POST['search_name']."', gender ='".$gender."', age='".$age."', height ='".$height."', maritalstatus='".$marital1."', religion='".$religion."', mothertongue ='".$mothertangue1."', caste ='".$caste1."', country ='".$COUNTRYRIGHT1."', education ='".$EDUCATIONRIGHT."', showprofile='".$photo1."', horoscope='".$_POST['HOROSCOPE_OPT']."' where search_id='".$id."'";
mysql_query("update search set name='".$_POST['search_name']."', gender ='".$gender."', age='".$age."', height ='".$height."', maritalstatus='".$marital1."', religion='".$religion."', mothertongue ='".$mothertangue1."', caste ='".$caste1."', country ='".$COUNTRYRIGHT1."', education ='".$EDUCATIONRIGHT."', showprofile='".$photo1."', horoscope='".$_POST['HOROSCOPE_OPT']."' where search_id='".$id."'")or die(mysql_error());
 
}
public function deleteresult($sid,$id)
{
  $q1=mysql_query("select * from search where search_id='".$id."' and user_id='".$sid."'")or die(mysql_error());
  if(mysql_num_rows($q1)>0)
  {
  mysql_query("delete from search where search_id='".$id."' and user_id='".$sid."'")or die(mysql_error());
   return ("Record deleted sucessfully !");
  }
  else
  {
  return ("Mismatch id or record has deleted !");
  }
}

public function searchregular($sid,$id)
{
$query=mysql_query("select * from search where search_id='".$id."' and user_id='".$sid."'")or die(mysql_error());
if(mysql_num_rows($query)>0)
{
$row=mysql_fetch_array($query);
$gender=$row['gender'];
$age=explode('-',$row['age']);
$minage=(date('Y')-$age[0])."-".date('m')."-".date('d');
$maxage=(date('Y')-($age[1]+1))."-".date('m')."-".date('d');
$height=explode(':',$row['height']);
$minh=$height[0];
$maxh=$height[1];
$marital1=explode(",",$row['maritalstatus']);
for($i=0;$i<count($marital1);$i++)
{
if($i!=0)
{
$marital.=" or ";
$marital2.=",";
}
$marital.="maritalstatus='".$marital1[$i]."'";
$marital2.=$marital1[$i];
}

$religion=$row['religion'];

$mothertangue1=explode(",",$row['mothertongue']);

for($i=0;$i<count($mothertangue1)&&$mothertangue1[$i]&&!in_array("Any", $mothertangue1);$i++)
{
if($i!=0)
{
$mothertangue.=" or ";
$mothertangue2.=",";
}
$mothertangue.="mother_tongue='".$mothertangue1[$i]."'";
$mothertangue2.=$mothertangue1[$i];
}
$caste1=explode(",",$row['caste']);
for($i=0;$i<count($caste1)&& $caste1[$i]&&!in_array("Any", $caste1) ;$i++)
{
if($i!=0)
{
$caste.=" or ";
$caste2.=",";
}
$caste.="caste='".$caste1[$i]."'";
$caste2.=$caste1[$i];
}

$COUNTRYRIGHT1=explode(",",$row['country']);
for($i=0;$i<count($COUNTRYRIGHT1)&& $COUNTRYRIGHT1[$i]&&!in_array("Any", $COUNTRYRIGHT1);$i++)
{
if($i!=0)
{
$country.=" or ";
$country2.=",";
}
$country.="country='".$COUNTRYRIGHT1[$i]."'";
$country2.= $COUNTRYRIGHT1[$i];
}
 
$EDUCATIONRIGHT=explode(",",$row['education']);
for($i=0;$i<count($EDUCATIONRIGHT)&&$EDUCATIONRIGHT[$i]&&!in_array("Any", $EDUCATIONRIGHT);$i++)
{
if($i!=0)
{
$educationfield.=" or ";
$educationfield2.=",";
}
$educationfield.="education='".$EDUCATIONRIGHT[$i]."'";
$educationfield2.=$EDUCATIONRIGHT[$i];
}

$str="select * from users as u, profiles as p where u.active=1 and u.activation_code = '' and u.group_id =2 and  u.id = p.user_id  and p.activestatus=1";
if($gender)
{
$str.=" and gender='".$gender."'";
}
if($minage && $maxage)
{
$str.=" and dob<='".$minage."'";
}
if($maxage)
{
$str.=" and dob>'".$maxage."'";
}
if($marital)
{
$str.=" and (".$marital.")";
}
if($religion)
{
$str.=" and religion='".$religion."'";
}
if($mothertangue)
{
$str.= " and (".$mothertangue.")";
}
if($caste)
{
$str.=" and (".$caste.")";
}
if($country)
{
$str.=" and (".$country.")";
}
if($educationfield)
{
$str.=" and (".$educationfield.")";
}
if($row['showprofile']=='Y')
{
$withpic='yes';
$str.=" and (photo1!='' or photo2!='' or photo3!='' )";
}

if($minh)
{
$str.=" and height>='".$minh."'";
}
if($maxh)
{
$str.=" and height<='".$maxh."'";
}
$newdata = array(
                   'gender'  => $gender,
                   'age'     => $row['age'],
                   'country' => $country2,
				   'religion' => $religion,
				   'caste' => $caste2,
				   'withpic' =>$withpic
				   
               );
$this->session->set_userdata($newdata); 
//echo $str;


$query= $this->db->query($str)or die(mysql_error());

return $query->result_array();
}

}

public function searchmemberbyid()
{
$str="select * from profiles as p,users as u where uid='".$_POST['mid']."' and p.user_id=u.id and u.active=1 and u.activation_code = '' and u.group_id =2 and p.activestatus=1";
 $query= $this->db->query($str)or die(mysql_error());

return $query->result_array(); 
}

public function searchbykeyword($num, $offset)
{
$search=explode(',',$_POST['search']);
//echo "hello";

for($i=0;$i<count($search);$i++)
 { 
 $regs = array();
   if(ereg('yrs',$search[$i],$regs))
   {
   $age=$search[$i];
   }
   if(ereg('female',strtolower($search[$i]),$regs) || ereg('f',strtolower($search[$i]),$regs))
   {
    $gender='f';
   }
   else if(ereg('male',strtolower($search[$i]),$regs) || ereg('m',strtolower($search[$i]),$regs))
   {
   $gender='m' ;
   }
   if($i==0)
   {
   $str2="'".trim($search[$i])."'";
   }
   else
   {
   $str2.=",'".trim($search[$i])."'";
   }
   
  
 }
if($age)
{
$age11=explode('yrs',strtolower($age));
   if(ereg('-',$age11[0],$regs))
   {
     $age1=explode('-',$age11[0]);
	   
   }
   else
   {
   $age1[0]=$age11[0];
   }
}


if($age1[0] && $age1[1])
{
 $agenew=" and dob<='".(date('Y')-$age1[0])."-".date('m')."-".date('d')."' and dob>'".(date('Y')-($age1[1]+1))."-".date('m')."-".date('d')."'";
}
else if($age1[0])
{
 $agenew=" and dob<='".(date('Y')-$age1[0])."-".date('m')."-".date('d')."' and dob>'".(date('Y')-($age1[0]+1))."-".date('m')."-".date('d')."'";
}
$str="select * from profiles as p,users as u where p.user_id=u.id and u.active=1 and u.activation_code = '' and u.group_id =2 and p.activestatus=1";
if($agenew)
{
$str.=$agenew;
}
if($gender)
{
 $str.=" and gender='".$gender."'";
}
//echo $str;
 $str.=" and (uid in(".$str2.") or first_name in(".$str2.") or last_name in(".$str2.")or createdby in(".$str2.")or bio in(".$str2.")or phone in(".$str2.")or mobile in(".$str2.")or religion in(".$str2.")or mother_tongue in(".$str2.")or country in(".$str2.")or maritalstatus in(".$str2.")or height in(".$str2.")or caste in(".$str2.")or subcaste in(".$str2.")or diet in(".$str2.")or personalvalue in(".$str2.")or complexion in(".$str2.")or bodytype in(".$str2.")or specialclasses in(".$str2.")or stateresidence in(".$str2.")or cityresidence in(".$str2.")or education in(".$str2.")or workingwith in(".$str2.")or workingas in(".$str2.")or income in(".$str2.")or manglik in(".$str2.")or star in(".$str2.")or raasi in(".$str2.")or familystatus in(".$str2.")or familytype in(".$str2.")";

if($age1[0] && $age1[1])
{
$str.=" or (dob<='".(date('Y')-$age1[0])."-".date('m')."-".date('d')."' and dob>'".(date('Y')-($age1[1]+1))."-".date('m')."-".date('d')."')";
}
else if($age1[0])
{
$str.=" or (dob<='".(date('Y')-$age1[0])."-".date('m')."-".date('d')."' and dob>'".(date('Y')-($age1[0]+1))."-".date('m')."-".date('d')."')";
}
if($gender)
{
 $str.=" or gender='".$gender."'";
}
  $str.=")";
  if(!$_POST['search'])
{
$str=$_SESSION['advance'];
}
if($offset=='')
  {
  $offset=0;
  //$num=5;
  }
  if(isset($num)&& $num!=0)
  {
  $ng=' limit '.$offset.",".$num;
  }
  
 $query= $this->db->query($str.$ng)or die(mysql_error());
 $_SESSION['advance']=$str;

return $query->result_array(); 
}

public function byadvancesearch($num, $offset)
{

$gender=$_POST['GENDER'];
$minage=(date('Y')-$_POST['STAGE'])."-".date('m')."-".date('d');
$maxage=(date('Y')-($_POST['ENDAGE']+1))."-".date('m')."-".date('d');
 $minheight=$_POST['STHEIGHT'];
 $maxheight=$_POST['ENDHEIGHT'];

$marital=$_POST['MARITAL_STATUS'];
$religion=$_POST['community'];

$mothertongue=explode(",",$_POST['MOTHERTONGUERIGHT']);
$cast=explode(",",$_POST['CASTERIGHT']);
$subcaste=explode(",",$_POST['subcaste']);
$physicalstatus=$_POST['specialcases'];
$country=explode(",",$_POST['COUNTRYRIGHT']);
$education=explode(",",$_POST['EDUCATIONRIGHT']);
$occuption=explode(",",$_POST['OCCUPATIONRIGHT']);
$income=$_POST['STANNUALINCOME'];
$star=explode(",",$_POST['STARRIGHT']);
$manglik=$_POST['MANGLIK'];
$eating=$_POST['EATINGHABITS'];
$drink=$_POST['DRINKING'];
$smoke=$_POST['SMOKING'];
$photo=$_POST['PHOTO_OPT'];
$keyword=$_POST['KEYWORDS'];
$str="select * from profiles as p,users as u where p.user_id=u.id and u.active=1 and u.activation_code = '' and u.group_id =2 and p.activestatus=1";
if($gender)
{
 $str.=" and gender='".$gender."'";
}
if($minage)
{
 $str.=" and dob<='".$minage."'";
}
if($maxage)
{
 $str.=" and dob>='".$maxage."'";
}

if($minheight)
{
$str.=" and height>='".$minheight."'";
}
if($maxheight)
{
$str.=" and height<='".$maxheight."'";
}
if($marital)
{
for($i=0;$i<count($marital)&&$marital[$i];$i++)
{
if($i!=0)
{
$marital1.=" or ";

}
$marital1.="maritalstatus='".$marital[$i]."'";
}
$str.=" and (".$marital1.")";
}

//echo "hello";

for($i=0;$i<count($mothertongue)&&$mothertongue[$i]&&!in_array("Any", $mothertongue);$i++)
{

if($i!=0)
{
$mothertongue1.=" or ";

}
 $mothertongue1.="mother_tongue='".$mothertongue[$i]."'";
}


for($i=0;$i<count($cast)&&$cast[$i]&&!in_array("Any", $cast);$i++)
{
if($i!=0)
{
$cast1.=" or ";

}
$cast1.="caste='".$cast[$i]."'";
}
for($i=0;$i<count($subcaste)&&$subcaste[$i]&&!in_array("Any", $subcaste);$i++)
{
if($i!=0)
{
$subcaste1.=" or ";

}
$subcaste1.="subcaste='".$subcaste[$i]."'";
}

if($physicalstatus)
{
$physicalstatus1.="specialclasses='".$physicalstatus."'";
}

for($i=0;$i<count($country)&&$country[$i]&&!in_array("Any", $country);$i++)
{
if($i!=0)
{
$country1.=" or ";

}
$country1.="country='".$country[$i]."'";
}

for($i=0;$i<count($education)&&$education[$i]&&!in_array("Any",$education);$i++)
{
if($i!=0)
{
$education1.=" or ";

}
$education1.="education='".$education[$i]."'";
}

for($i=0;$i<count($occuption)&&$occuption[$i]&&!in_array("Any",$occuption);$i++)
{
if($i!=0)
{
$occuption1.=" or ";

}
$occuption1.="workingas='".$education[$i]."'";
}


for($i=0;$i<count($star)&&$star[$i]&&!in_array("Any", $star);$i++)
{
if($i!=0)
{
$star1.=" or ";

}
$star1.="star='".$star[$i]."'";
}
for($i=0;$i<count($eating)&&$eating[$i];$i++)
{
if($i!=0)
{
$eating1.=" or ";

}
$eating1.="diet='".$eating[$i]."'";
}
for($i=0;$i<count($drink)&&$drink[$i];$i++)
{
if($i!=0)
{
$drink1.=" or ";

}
$drink1.="drink='".$drink[$i]."'";
}
for($i=0;$i<count($smoke)&&$smoke[$i];$i++)
{
if($i!=0)
{
$smoke1.=" or ";

}
$smoke1.="smoke='".$smoke[$i]."'";
}
for($i=0;$i<count($keyword)&&$keyword[$i];$i++)
{
if($i!=0)
{
$keyword1.=" or ";

}
$keyword1.="bio='".$keyword[$i]."'";
}

if($mothertongue1)
{
$str.=" and (".$mothertongue1.")";
}
if($cast1)
{
$str.=" and (".$cast1.")";
}
if($subcaste1)
{
$str.=" and (".$subcaste1.")";
}
if($physicalstatus1)
{
$str.=" and (".$physicalstatus1.")";
}
if($country1)
{
$str.=" and (".$country1.")";
}
if($education1)
{
$str.=" and (".$education1.")";
}
if($occuption1)
{
$str.=" and (".$occuption1.")";
}
if($star1)
{
$str.=" and (".$star1.")";
}
if($manglik)
{
$str.=" and manglik='".$manglik."'";
}
if($eating1)
{
$str.=" and (".$eating1.")";
}
if($drink1)
{
$str.=" and (".$drink1.")";
}

if($smoke1)
{
$str.=" and (".$smoke1.")";
}
if($keyword1)
{
$str.=" and (".$keyword1.")";
}
if($income)
{
$str.=" and income='".$income."'";
}

if($photo)
{
//$str.=" and (photo1!='' or photo1='' or photo1='')";
$str.=" and u.id in(select uid from photo)";
}
if($_POST['IGNORE_OPT'])
{
 $str.=" and u.id not in(select ignore_id from user_ignore where ignoredby_id=".$this->session->userdata('id').")";
}
if($_POST['CONTACT_OPT'])
{
 $str.=" and u.id not in(select receiver_id from user_mail where sender_id=".$this->session->userdata('id').")";
}

if($_POST['VIEW_OPT'])
{
 $str.=" and u.id not in(select user_viewed_id from profileviewed where user_view_by=".$this->session->userdata('id').")";
}

if($_POST['SHORTLIST_OPT'])
{
 $str.=" and u.id not in(select receiver_id from shortlist where sender_id=".$this->session->userdata('id').")";
}

//echo $str;
$newdata = array(
                   'gender'  => $gender,
                   'age'     => $_POST['STAGE']."-".$_POST['ENDAGE'],
                   'country' => $_POST['COUNTRYRIGHT'],
				   'religion' => $religion,
				   'caste' => $_POST['CASTERIGHT'],
				   'withpic' =>$photo
				   
               );
$this->session->set_userdata($newdata); 

if(!$_POST['search'] && !$_POST['save'])
{
$str=$_SESSION['str2'];
}
if($offset=='')
  {
  $offset=0;
  //$num=5;
  }
  if(isset($num)&& $num!=0)
  {
  $ng=' limit '.$offset.",".$num;
  }
//echo $str; 
$query= $this->db->query($str.$ng)or die(mysql_error());
$_SESSION['str2']=$str;
return $query->result_array();
}
public function addadvancesearch()
{
$gender=$_POST['GENDER'];
$age=$_POST['STAGE']."-".$_POST['ENDAGE'];
$height=$_POST['STHEIGHT'].":".$_POST['ENDHEIGHT'];
$marital=implode(",",$_POST['MARITAL_STATUS']);
$religion=$_POST['community'];
$mothertongue=$_POST['mothertangue'];
$cast=$_POST['CASTERIGHT'];
$subcaste=$_POST['subcaste'];
$physicalstatus=$_POST['specialcases'];
$country=$_POST['COUNTRYRIGHT'];

$state=$_POST['RESIDINGSTATERIGHT'];
$city=$_POST['RESIDINGCITYRIGHT'];
$education=$_POST['EDUCATIONRIGHT'];
$occuption=$_POST['OCCUPATIONRIGHT'];
$income=$_POST['STANNUALINCOME'];
$star=$_POST['STARRIGHT'];
$manglik=$_POST['MANGLIK'];
$eating=implode(',',$_POST['EATINGHABITS']);
$drink=implode(',',$_POST['DRINKING']);
$smoke=implode(',',$_POST['SMOKING']);
$keyword=$_POST['KEYWORDS']; 
$photo=$_POST['PHOTO_OPT'];   
$horoscope=$_POST['HOROSCOPE_OPT'];
$ignore=$_POST['IGNORE_OPT'];
$contact=$_POST['CONTACT_OPT'];
$view=$_POST['VIEW_OPT'];
$shortlist=$_POST['SHORTLIST_OPT'];
$query =mysql_query("select * from search where user_id=".$this->session->userdata('id')." and name='".$_POST['search_name']."' and gender='".$gender."' and age='".$age."' and height='".$height."' and maritalstatus='".$marital."' and religion='".$religion."' and mothertongue='".$mothertongue."' and caste='".$cast."' and subcaste='".$subcaste."' and physicalstatus='".$physicalstatus."' and country='".$country."' and  state='".$state."' and city='".$city."' and education='".$education."' and occupation='".$occuption."' and showprofile='".$photo."' and status='Advance search' and horoscope='".$horoscope."' and income='".$income."' and star='".$star."' and manglik='".$manglik."' and diet='".$eating."' and drink='".$drink."' and smoke='".$smoke."' and keyword='".$keyword."' and ignoreprofile='".$ignore."' and alreadycontacted='".$contact."' and viewedprofile='".$view."' and shortlistedprofile='".$shortlist."'")or die(mysql_error());
if(mysql_num_rows($query)<=0)
{
mysql_query("insert into search(user_id,name,gender,age,height,maritalstatus,religion,mothertongue,caste,subcaste,physicalstatus,country,state,city,education,occupation,showprofile,status,horoscope,income,star,manglik,diet,drink,smoke,keyword,ignoreprofile,alreadycontacted,viewedprofile,shortlistedprofile) value('".$this->session->userdata('id')."','".$_POST['search_name']."','".$gender."','".$age."','".$height."','".$marital."','".$religion."','".$mothertongue."','".$cast."','".$subcaste."','".$physicalstatus."','".$country."','".$state."','".$city."','".$education."','".$occuption."','".$photo."','Advance search','".$horoscope."','".$income."','".$star."','".$manglik."','".$eating."','".$drink."','".$smoke."','".$keyword."','".$ignore."','".$contact."','".$view."','".$shortlist."')")or die(mysql_error());
}
}

public function getadvancesearch($id,$uid)
{
$str='select * from search where search_id='.$id." and user_id=".$uid;
$query= $this->db->query($str)or die(mysql_error());
return $query->result_array();
}

public function editadvancesearch()
{
$gender=$_POST['GENDER'];
$age=$_POST['STAGE']."-".$_POST['ENDAGE'];
$height=$_POST['STHEIGHT'].":".$_POST['ENDHEIGHT'];
$marital=implode(",",$_POST['MARITAL_STATUS']);
$religion=$_POST['community'];
$mothertongue=$_POST['mothertangue'];
$cast=$_POST['CASTERIGHT'];
$subcaste=$_POST['subcaste'];
$physicalstatus=$_POST['specialcases'];
$country=$_POST['COUNTRYRIGHT'];
$state=$_POST['RESIDINGSTATERIGHT'];
$city=$_POST['RESIDINGCITYRIGHT'];
$education=$_POST['EDUCATIONRIGHT'];
$occuption=$_POST['OCCUPATIONRIGHT'];
$income=$_POST['STANNUALINCOME'];
$star=$_POST['STARRIGHT'];
$manglik=$_POST['MANGLIK'];
$eating=implode(',',$_POST['EATINGHABITS']);
$drink=implode(',',$_POST['DRINKING']);
$smoke=implode(',',$_POST['SMOKING']);
$keyword=$_POST['KEYWORDS']; 
$photo=$_POST['PHOTO_OPT'];   
$horoscope=$_POST['HOROSCOPE_OPT'];
$ignore=$_POST['IGNORE_OPT'];
$contact=$_POST['CONTACT_OPT'];
$view=$_POST['VIEW_OPT'];
$shortlist=$_POST['SHORTLIST_OPT'];

$str="update search set name='".$_POST['search_name']."',gender='".$gender."'";

if($age)
{
$str.=", age='".$age."'";
}
if($height)
{
$str.=", height='".$height."'";
}
if($marital)
{
$str.=", maritalstatus='".$marital."'";
}
if($religion)
{
$str.=", religion='".$religion."'";
}
if($mothertongue)
{
$str.=", mothertongue='".$mothertongue."'";
}
if($state)
{
$str.=", state='".$state."'";
}
if($city)
{
$str.=", city='".$city."'";
}
if($cast)
{
$str.=", caste='".$cast."'";
}
if($subcaste)
{
$str.=", subcaste='".$subcaste."'";
}
if($physicalstatus)
{
$str.=", physicalstatus='".$physicalstatus."'";
}
if($country)
{
$str.=", country='".$country."'";
}
if($education)
{
$str.=", education='".$education."'";
}
if($occuption)
{
$str.=", occupation='".$occuption."'";
}
if($photo)
{
$str.=", showprofile='".$photo."'";
}
if($horoscope)
{
$str.=", horoscope='".$horoscope."'";
}
if($income)
{
$str.=", income='".$income."'";
}
if($star)
{
$str.=", star='".$star."'";
}
if($manglik)
{
$str.=", manglik='".$manglik."'";
}
if($eating)
{
$str.=", diet='".$eating."'";
}
if($drink)
{
$str.=", drink='".$drink."'";
}
if($smoke)
{
$str.=", smoke='".$smoke."'";
}
if($keyword)
{
$str.=", keyword='".$keyword."'";
}
if($ignore)
{
$str.=", ignoreprofile='".$ignore."'";
}
if($ignore)
{
$str.=", ignoreprofile='".$ignore."'";
}
if($contact)
{
$str.=", alreadycontacted='".$contact."'";
}
if($view)
{
$str.=", viewedprofile='".$view."'";
}
if($shortlist)
{
$str.=", shortlistedprofile='".$shortlist."'";
}
if($shortlist)
{
$str.=", shortlistedprofile='".$shortlist."'";
}
echo $str.=" where search_id=".$_POST['sid']." and user_id=".$this->session->userdata('id');


mysql_query($str)or die(mysql_error());

}

public function searchadvance($id)
{
$query=mysql_query('select * from search where search_id='.$id." and user_id=".$this->session->userdata('id'))or die(mysql_error());
if(mysql_num_rows($query)>0)
{
$row=mysql_fetch_array($query);
$gender=$row['GENDER'];
$ag=explode('-',$row['age']);
if($ag[0])
{
$minage=(date('Y')-$ag[0])."-".date('m')."-".date('d');
}
if($ag[1])
{
$maxage=(date('Y')-($ag[1]+1))."-".date('m')."-".date('d');
}
$hg=explode(':',$row['height']);
if($hg[0])
{
 $minheight=$hg[0];
}
if($hg[1])
{
 $maxheight=$hg[1];
}

$marital=explode(',',$row['maritalstatus']);
$religion=$row['religion'];

$mothertongue=explode(",",$row['mothertongue']);
$cast=explode(",",$row['caste']);
$subcaste=explode(",",$row['subcaste']);
$physicalstatus=$row['physicalstatus'];
$country=explode(",",$row['country']);
$education=explode(",",$row['education']);
$occuption=explode(",",$row['occupation']);
$income=$row['income'];
$star=explode(",",$row['star']);
$manglik=$row['manglik'];
$eating=explode(',',$row['diet']);
$drink=explode(',',$row['drink']);
$smoke=explode(',',$row['smoke']);
$photo=$row['showprofile'];
$keyword=explode(',',$row['keyword']);
$str="select * from profiles as p,users as u where p.user_id=u.id and u.active=1 and u.activation_code = '' and u.group_id =2 and p.activestatus=1";
if($gender)
{
 $str.=" and gender='".$gender."'";
}
if($minage)
{
 $str.=" and dob<='".$minage."'";
}
if($maxage)
{
 $str.=" and dob>='".$maxage."'";
}

if($minheight)
{
$str.=" and height>='".$minheight."'";
}
if($maxheight)
{
$str.=" and height<='".$maxheight."'";
}
if($marital)
{
for($i=0;$i<count($marital)&&$marital[$i];$i++)
{
if($i!=0)
{
$marital1.=" or ";

}
$marital1.="maritalstatus='".$marital[$i]."'";
}
$str.=" and (".$marital1.")";
}

//echo "hello";

for($i=0;$i<count($mothertongue)&&$mothertongue[$i]&&!in_array("Any", $mothertongue);$i++)
{

if($i!=0)
{
$mothertongue1.=" or ";

}
 $mothertongue1.="mother_tongue='".$mothertongue[$i]."'";
}


for($i=0;$i<count($cast)&&$cast[$i]&&!in_array("Any", $cast);$i++)
{
if($i!=0)
{
$cast1.=" or ";

}
$cast1.="caste='".$cast[$i]."'";
}
for($i=0;$i<count($subcaste)&&$subcaste[$i]&&!in_array("Any", $subcaste);$i++)
{
if($i!=0)
{
$subcaste1.=" or ";

}
$subcaste1.="subcaste='".$subcaste[$i]."'";
}

if($physicalstatus)
{
$physicalstatus1.="specialclasses='".$physicalstatus."'";
}

for($i=0;$i<count($country)&&$country[$i]&&!in_array("Any", $country);$i++)
{
if($i!=0)
{
$country1.=" or ";

}
$country1.="country='".$country[$i]."'";
}

for($i=0;$i<count($education)&&$education[$i]&&!in_array("Any", $education);$i++)
{
if($i!=0)
{
$education1.=" or ";

}
$education1.="education='".$education[$i]."'";
}

for($i=0;$i<count($occuption)&&$occuption[$i]&&!in_array("Any", $occuption);$i++)
{
if($i!=0)
{
$occuption1.=" or ";

}
$occuption1.="workingas='".$education[$i]."'";
}


for($i=0;$i<count($star)&&$star[$i]&&!in_array("Any", $star);$i++)
{
if($i!=0)
{
$star1.=" or ";

}
$star1.="star='".$star[$i]."'";
}
for($i=0;$i<count($eating)&&$eating[$i];$i++)
{
if($i!=0)
{
$eating1.=" or ";

}
$eating1.="diet='".$eating[$i]."'";
}
for($i=0;$i<count($drink)&&$drink[$i];$i++)
{
if($i!=0)
{
$drink1.=" or ";

}
$drink1.="drink='".$drink[$i]."'";
}
for($i=0;$i<count($smoke)&&$smoke[$i];$i++)
{
if($i!=0)
{
$smoke1.=" or ";

}
$smoke1.="smoke='".$smoke[$i]."'";
}
for($i=0;$i<count($keyword)&&$keyword[$i];$i++)
{
if($i!=0)
{
$keyword1.=" or ";

}
$keyword1.="bio='".$keyword[$i]."'";
}

if($mothertongue1)
{
$str.=" and (".$mothertongue1.")";
}
if($cast1)
{
$str.=" and (".$cast1.")";
}
if($subcaste1)
{
$str.=" and (".$subcaste1.")";
}
if($physicalstatus1)
{
$str.=" and (".$physicalstatus1.")";
}
if($country1)
{
$str.=" and (".$country1.")";
}
if($education1)
{
$str.=" and (".$education1.")";
}
if($occuption1)
{
$str.=" and (".$occuption1.")";
}
if($star1)
{
$str.=" and (".$star1.")";
}
if($manglik)
{
$str.=" and manglik='".$manglik."'";
}
if($eating1)
{
$str.=" and (".$eating1.")";
}
if($drink1)
{
$str.=" and (".$drink1.")";
}

if($smoke1)
{
$str.=" and (".$smoke1.")";
}
if($keyword1)
{
$str.=" and (".$keyword1.")";
}
if($income)
{
$str.=" and income='".$income."'";
}

if($photo)
{
$str.=" and u.id in(select uid from photo)";
}
if($row['ignoreprofile'])
{
 $str.=" and u.id not in(select ignore_id from user_ignore where ignoredby_id=".$this->session->userdata('id').")";
}
if($row['alreadycontacted'])
{
 $str.=" and u.id not in(select receiver_id from user_mail where sender_id=".$this->session->userdata('id').")";
}
if($row['alreadycontacted'])
{
 $str.=" and u.id not in(select sender_id from user_mail where receiver_id=".$this->session->userdata('id').")";
}
if($row['viewedprofile'])
{
 $str.=" and u.id not in(select user_viewed_id from profileviewed where user_view_by=".$this->session->userdata('id').")";
}

if($row['shortlistedprofile'])
{
 $str.=" and u.id not in(select receiver_id from shortlist where sender_id=".$this->session->userdata('id').")";
}

//echo $str;
$newdata = array(
                   'gender'  => $gender,
                   'age'     => $row['age'],
                   'country' => $row['country'],
				   'religion' => $row['religion'],
				   'caste' => $row['caste'],
				   'withpic' =>$row['showprofile']
				   
               );
$this->session->set_userdata($newdata); 
$query= $this->db->query($str)or die(mysql_error());

return $query->result_array();
}
}
}?>