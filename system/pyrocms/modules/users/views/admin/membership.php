
<title>Swayamvaraonline</title>
<script type="text/javascript">
    var APPPATH_URI = "<?php echo APPPATH_URI;?>";
	var SITE_URL = "<?php echo rtrim(site_url(), '/').'/';?>";
    var BASE_URL = "<?php echo BASE_URL;?>";
    var BASE_URI = "<?php echo BASE_URI;?>";
    var DEFAULT_TITLE = "<?php echo $this->settings->site_name; ?>";
	var DIALOG_MESSAGE = "<?php echo lang('dialog.delete_message'); ?>";
</script>

<?php echo $template['metadata']; ?>		
		<div id="page-wrapper">		
		<section id="sidebar" dir="ltr"> 
		<header id="main">
			<div id="logo"></div>
			<h1><?php echo $this->settings->site_name; ?></h1>
		</header>
		<section id="user-links">
			<?php //echo sprintf(lang('cp_logged_in_welcome'), $user->display_name); ?>
			<?php if ($this->settings->enable_profiles) echo ' | '.anchor('edit-profile', lang('cp_edit_profile_label')) ?><br />
			<?php echo anchor('', lang('cp_view_frontend'), 'target="_blank"'); ?> | <?php echo anchor('admin/logout', lang('cp_logout_label')); ?>
		</section>
 <nav id="main-nav">
	<ul>
		<li><?php echo anchor('admin', lang('cp_admin_home_title'), 'class="top-link no-submenu' . (!$this->module > '' ? ' current' : '').'"');?></li>

		<?php
		foreach ($menu_items as $menu_item)
		{
			$count = 0;
			//Let's see how many menu items they have access to
			if ($this->user->group == 'admin')
			{
				$count = count($modules[$menu_item]);
			}
			else
			{
				if (array_key_exists($menu_item, $modules)) 
				{
					foreach ($modules[$menu_item] as $module)
					{
						$count += array_key_exists($module['slug'], $this->permissions) ? 1 : 0;
					}
				}				
			}
			// If we are in the users menu, we have to account for the hacked link below
			if ($menu_item == 'users')
			{
				$count += (array_key_exists('users', $this->permissions) OR $this->user->group == 'admin') ? 1 : 0;
			}
			// If they only have access to one item in this menu, why not just have it as the main link?
			if ($count > 0)
			{
				// They have access to more than one menu item, so create a drop menu
				if ($count > 1 )
				{
					echo '<li>';
					$name = lang('cp_nav_'.$menu_item)!=''&&lang('cp_nav_'.$menu_item)!=NULL ? lang('cp_nav_'.$menu_item) : $menu_item;
					$current = (($this->module_details && $this->module_details['menu'] == $menu_item) or $menu_item == $this->module);
					$class = $current ? "top-link current" : "top-link";
					echo anchor('#', $name, array('class' => $class));

					echo '<ul>';
					
				// User has access to Users module only, no other users item
				} 
				elseif ($count == 1 AND $this->module == 'users' AND $menu_item == 'users') 
				{
					echo '<li>' . anchor('admin/users', lang('cp_manage_users'), array('style' => 'font-weight: bold;', 'class' => $this->module == 'users' ? 'top-link no-submenu  current' : 'top-link no-submenu ')) . '</li>';
				}
				
				// Not a big fan of the following hack, if a module needs two navigation links, we should be able to define that
				if ( (array_key_exists('users', $this->permissions) OR $this->user->group == 'admin') AND $menu_item == 'users' AND $count != 1)
				{
					echo '<li>' . anchor('admin/users', lang('cp_manage_users'), 'class="' . (($this->module == 'users') ? ' current"' : '"')) . '</li>';
				} 

				//Lets get the sub-menu links, or parent link if that is the case
				if (array_key_exists($menu_item, $modules)) 
				{
					if (is_array($modules[$menu_item])) 
					{
						ksort($modules[$menu_item]);
					}
						
					foreach ($modules[$menu_item] as $module)
					{
						if (lang('cp_nav_'.$module['name'])!=''&&lang('cp_nav_'.$module['name'])!=NULL)
						{
							$module['name'] = lang('cp_manage_'.$module['name']);
						}
						$current = $this->module == $module['slug'];
						$class = $current ? "current " : "";
						$class .= $count <= 1 ? "top-link no-submenu " : "";
						
						if (array_key_exists($module['slug'], $this->permissions) OR $this->user->group == 'admin')
						{
							echo '<li>' . anchor('admin/'.$module['slug'], $module['name'], array('class'=>$class)) . '</li>';
						}
					}
				}
			}			
			// They have access to more than one menu item, so close the drop menu
			if ($count > 1)
			{
				echo '</ul>';
				echo '</li>';
			}
		}
		?>

		<?php if (array_key_exists('settings', $this->permissions) OR $this->user->group == 'admin'): ?>
		<li><?php echo anchor('admin/settings', lang('cp_nav_settings'), 'class="top-link no-submenu' . (($this->module == 'settings') ? ' current"' : '"'));?></li>
		<?php endif; ?>

		<?php if (array_key_exists('modules', $this->permissions) OR $this->user->group == 'admin'): ?>
		<li><?php echo anchor('admin/modules', lang('cp_nav_addons'), 'class="last top-link no-submenu' . (($this->module == 'modules') ? ' current"' : '"'));?></li>
		<?php endif;/**/ ?>
	</ul>
</nav>
</section>
<section id="content-wrapper"> 
<header id="page-header">
<h1><a href="<?php echo base_url();?>index.php/admin/membership">Membership</a></h1>
 
 <p id="page-header-help">
 <a class="modal cboxElement" title="Help->Redirects" href="http://localhost/ci/pyrocms/index.php/admin/help/redirects">?</a>
</p>
</header>
<nav id="shortcuts">
<h6>Shortcuts</h6>
<ul>
 <!-- <li><a class="add" href="http://localhost/ci/pyrocms/index.php/admin/redirects/add">Add Redirect</a></li>
  <li><a  href="http://localhost/ci/pyrocms/index.php/admin/redirects">Add Redirect</a></li>-->
  <li><a  href="membership_list">Membership List</a></li>
</ul>
<br class="clear-both">
</nav>

<div id="content">

 <h3>Membership Form</h3>
	<?php /*?><h2 class="page-title" id="page_title"><?php echo lang('user_register_header') ?></h2><?php */?>
<?php
if(isset($msg)){
 echo $msg;}?>
<?php /*?><?php echo form_open('/users/membership/post', array('id'=>'membership')); ?><?php */?>
<?php echo form_open('/admin/users/membership_', array('id'=>'membership')); ?>
<div id="tabs ui-tabs ui-widget ui-widget-content ui-corner-all">
<table>
    <tr>
	    <td><label for="membership_type">Membership Type</label></td>
		<td><select name="membership_type">
		<option>Regular</option>
		<option>Gold</option>
		<option>Silver</option>
		<option>Paid</option>
		<option>UnPaid</option>
		</select></td>
	</tr>
	<tr><td><label for="Price">Price</label></td><td><input type="text" name="price" maxlength="40" value="" /></td></tr>
	<tr><td><label for="description">Description</label></td><td><textarea name="description" rows="3" cols="20"></textarea></td></tr>
	<tr><td><label>Validity</label></td><td><select name="time1">
     	<?php for($i=1;$i<=12;++$i){?><option><?php echo $i;?></option><?php }?>
	</select>
	<select name="time2"><option value="month">Months</option><option value="year">Year</option></select>
	</td></tr>
<tr><td colspan="2">
<div class="buttons float-right padding-top">
<button class="button" type="submit" name="submit" value="<?php echo lang('login_label'); ?>" ><span>Save</span>	</button>

<button class="button" type="button" name="cancel" value="<?php echo lang('login_label'); ?>" onclick="history.go(-1);" ><span>Cancel</span>	</button>

</div>
</td></tr>
</table>	
</div>
<?php echo form_close(); ?>






<?php /*?><?php if(isset($member)){?><?php */?>

<!--<table>
     <tr><td>Membership Type</td><td>Price</td><td>Detail</td><td>Timelimit</td></tr>
	 <?php foreach($member as $members){?>		 
		<tr><td><?php echo $members['membership_type'];?></td>
		<td><?php echo $members['price'];?></td>
		<td><?php echo $members['detail'];?></td>
		<td><?php echo $members['timelimit'];?></td>		
		</tr><?php }  ?>
		                 
							
</table>-->	<?php /*?> <?php }?><?php */?>



</div>
</section></div>


