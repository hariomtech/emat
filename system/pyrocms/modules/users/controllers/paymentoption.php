<?php 
ob_start();
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Paymentoption extends CI_Controller {
 	public function index()
	{
	$this->load->model('payment_model','',TRUE);
 	$data['query']= $this->payment_model->getmembershiptype();
	$this->load->helper('users/user');
	$this->load->view('header');
	$this->load->view('paymentoption',$data);
	$this->load->view('footer');
 
	}
	
	function payment()
	{
	if(!$_SESSION['newuid'])
	{
	 redirect('users/login');  
	}
	
	$this->load->model('payment_model','',TRUE);
 	$this->payment_model->payoption();
	
	if($_POST['payment']=='paypal')
	{
	
     $data['nid']=$_SESSION['newuid'];
   $data['status']='process';
	$this->load->view('paypal',$data);
	}
	else if($_POST['payment']=='creditcard')
	{
	 $data['nid']=$_SESSION['newuid'];
   $data['status']='process';
   
   $this->load->model('profile_model','',TRUE);
 	$data['query']=$this->profile_model->getprofile($_SESSION['newuid']);
	$this->load->view('creditcard',$data);
	}
	else
	{
	/*$this->load->model('payment_model','',TRUE);
 	$data['query']= $this->payment_model->getmembershiptype();
	$this->load->helper('users/user');
	$this->load->view('header');
	$this->load->view('paymentoption',$data);
	$this->load->view('footer');*/
	redirect('users/login');  
	}
	}
	
	public function upgrade()
	{
	if($this->session->userdata('id'))
	{
	$_SESSION['newuid']=$this->session->userdata('id');
	}
	$this->load->model('payment_model','',TRUE);
 	$data['query']= $this->payment_model->getmembershiptype();
	$data['msg']='upgrade';
	$this->load->helper('users/user');
	$this->load->view('header');
	$this->load->view('paymentoption',$data);
	$this->load->view('footer');
 
	} 
	
}?>