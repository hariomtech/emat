<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @author 		Phil Sturgeon - PyroCMS Development Team
 * @package 	PyroCMS
 * @subpackage 	Users Module
 * @since		v0.1
 *
 */
class Users_m extends MY_Model
{

    // Get a user's salt
    function getSalt($email = '')
    {
        if (!empty($email))
        {
            $this->
db->select('salt');
            $query = $this->db->get_where('users', array('email'=>$email));

            if ($query->num_rows() > 0)
            {
                $row = $query->row();
                return $row->salt;
            }
        }

        return FALSE;
    }

	// Get a specified (single) user
    function get($params)
    {
    	if(isset($params['id']))
    	{
    		$this->db->where('users.id', $params['id']);
    	}

    	if(isset($params['email']))
    	{
    		$this->db->where('LOWER(users.email)', strtolower($params['email']));
    	}

    	if(isset($params['role']))
    	{
    		$this->db->where('users.group_id', $params['role']);
    	}

    	$this->db->select('profiles.*, users.*, IF(profiles.last_name = "", profiles.first_name, CONCAT(profiles.first_name, " ", profiles.last_name)) as full_name', FALSE);
    	$this->db->limit(1);
    	$this->db->join('profiles', 'profiles.user_id = users.id', 'left');
    	return $this->db->get('users')->row();
    }

  	public function get_recent($limit = 10)
  	{
    	$this->db->order_by('users.created_on', 'desc');
    	$this->db->limit($limit);
    	return $this->get_all();
  	}

	function get_all()
    {
    	$this->db->select('profiles.*, users.*, g.description as group_name, IF(profiles.last_name = "", profiles.first_name, CONCAT(profiles.first_name, " ", profiles.last_name)) as full_name')
    			 ->join('groups g', 'g.id = users.group_id')
    			 ->join('profiles', 'profiles.user_id = users.id', 'left');
        $this->db->group_by('users.id');
    	return parent::get_all();
    }

	// Create a new user
	function add($input = array())
    {
		$this->load->helper('date');

        return parent::insert(array(
        	'email'				=> $input->email,
        	'password'			=> $input->password,
        	'salt'				=> $input->salt,
        	'first_name' 		=> ucwords(strtolower($input->first_name)),
        	'last_name' 		=> ucwords(strtolower($input->last_name)),
        	'role' 				=> empty($input->role) ? 'user' : $input->role,
        	'is_active' 		=> 0,
        	'lang'				=> $this->config->item('default_language'),
        	'activation_code' 	=> $input->activation_code,
        	'created_on' 		=> now(),
			'last_login'		=> now(),
        	'ip' 				=> $this->input->ip_address()
        ));
	}

	// Update the last login time
	function update_last_login($id) {
		$this->load->helper('date');
		$this->db->update('users', array('last_login' => now()), array('id' => $id));
	}

	// Activate a newly created user
	function activate($id)
	{
		return parent::update($id, array('is_active' => 1, 'activation_code' => ''));
	}

	public function count_by($params = array())
	{
		$this->db->from($this->_table)->join('profiles', 'users.id = profiles.user_id', 'left');

		if(!empty($params['active']))
		{
			$params['active'] = $params['active'] === 2 ? 0 : $params['active'] ;
			$this->db->where('users.active', $params['active']);
		}

		if(!empty($params['group_id']))
		{
			$this->db->where('group_id', $params['group_id']);
		}

		if(!empty($params['name']))
		{
			$this->db->like('users.username', trim($params['name']))
						->or_like('users.email', trim($params['name']))
						->or_like('profiles.first_name', trim($params['name']))
						->or_like('profiles.last_name', trim($params['name']));
		}

		return $this->db->count_all_results();
	}

	public function get_many_by($params = array())
	{
		if(!empty($params['active']))
		{
			$params['active'] = $params['active'] === 2 ? 0 : $params['active'] ;
			$this->db->where('active', $params['active']);
		}

		if(!empty($params['group_id']))
		{
			$this->db->where('group_id', $params['group_id']);
		}

		if(!empty($params['name']))
		{
			$this->db->or_like('users.username', trim($params['name']))
						->or_like('users.email', trim($params['name']))
						->or_like('profiles.first_name', trim($params['name']))
						->or_like('profiles.last_name', trim($params['name']));
		}

		return $this->get_all();
	}
//********************
//**Yet to be viewed
//********************
	public function getYetView()
	{ $params=array('id'=>$this->session->userdata('id'));
	 $query2= $this->get($params);
	 
	 $qyr= mysql_query('SELECT * from profileviewed where user_view_by= '.$this->session->userdata('id'));
	 while($all=mysql_fetch_array($qyr)){
		 $qryID.=$all['user_viewed_id'].",";
}		 
		 $qryID = substr($qryID,0,-1);
		  $addQry= " u.active=1 and u.activation_code = '' and u.group_id =2 and  u.id = p.user_id "; 
		   $addQry.= ' and p.gender != "'.$query2->gender.'"  and u.id NOT IN ("'.$qryID.'") and pp.marital = "'.$query2->maritalstatus.'" and pp.religion="'.$query2->religion.'" and pp.eating = "'.$query2->diet.'" and pp.caste = "'.$query2->caste.'" and pp.manglik = "'.$query2->manglik.'" and (YEAR(CURDATE())-YEAR(p.dob) > pp.agefrom) and (YEAR(CURDATE())-YEAR(p.dob) < pp.ageto) and pp.drinking = "'.$query2->drink.'" and  pp.smoking = "'.$query2->smoke.'"  and pp.mothertongue = "'.$query2->mother_tongue.'" and pp.physicalstatus =  "'.$query2->bodytype.'"  ';

//echo $addQry;
		  
		    $query = $this->db->query('SELECT * from users u, profiles p , partnerpreference pp where  '. $addQry);
			return $query->result_array();  			  
	}

	public function myhome_model()
	{	
	  $params=array('id'=>$this->session->userdata('id'));
	 $query= $this->get($params);
		return $query;
	
	}
//********************
//Viewed & not contacted
//********************
public function viewedModel()
	{ $params=array('id'=>$this->session->userdata('id'));
	 $query2= $this->get($params);
	  $qyr= mysql_query('SELECT * from profileviewed where user_view_by= '.$this->session->userdata('id'));
	 while($all=mysql_fetch_array($qyr)){
		 $qryID.=$all['user_viewed_id'].",";
		 }
		  $qryID = substr($qryID,0,-1);
		 
		  $qyr2= mysql_query('SELECT * from user_mail where sender_id = '.$this->session->userdata('id'));
	 while($all3=mysql_fetch_array($qyr2)){
		 $qryID1.=$all3['sender_id'].",";
		 }
		  $qryID1 = substr($qryID1,0,-1);
	   $addQry= " u.active=1 and u.activation_code = '' and u.group_id =2 and  u.id = p.user_id "; 
		   $addQry.= ' and p.gender != "'.$query2->gender.'" and       u.id  IN ("'.$qryID.'") and p.user_id NOT IN ("'.$qryID1.'") and pp.marital = "'.$query2->maritalstatus.'" and pp.religion="'.$query2->religion.'" and pp.eating = "'.$query2->diet.'" and pp.caste = "'.$query2->caste.'" and pp.manglik = "'.$query2->manglik.'" and (YEAR(CURDATE())-YEAR(p.dob) > pp.agefrom) and (YEAR(CURDATE())-YEAR(p.dob) < pp.ageto) and pp.drinking = "'.$query2->drink.'" and  pp.smoking = "'.$query2->smoke.'"  and pp.mothertongue = "'.$query2->mother_tongue.'" and pp.physicalstatus =  "'.$query2->bodytype.'" ';
		     $query = $this->db->query('SELECT * from users u, profiles p , partnerpreference pp where  '. $addQry);
			return $query->result_array();  			  
	}
//********************
//Members looking for me
//********************
public function memLookforMeModel()
	{ $params=array('id'=>$this->session->userdata('id'));
	 $query2= $this->get($params);
	   $year= date("Y") -  number_format(substr($query2->dob,0,3))  ;
	   $addQry= " u.active=1 and u.activation_code = '' and u.group_id =2 and  u.id = p.user_id "; 
		   $addQry.= ' and p.gender != "'.$query2->gender.'" and (pp.marital = "'.$query2->maritalstatus.'" or pp.religion="'.$query2->religion.'" or pp.eating = "'.$query2->diet.'" or pp.caste = "'.$query2->caste.'" or pp.manglik = "'.$query2->manglik.'" or (YEAR(CURDATE())-YEAR(p.dob) > pp.agefrom) and (YEAR(CURDATE())-YEAR(p.dob) < pp.ageto)   )	group by u.id  ';
		      
		     $query = $this->db->query('SELECT * from users u, profiles p,  partnerpreference pp  where  '. $addQry);
			return $query->result_array();  			  
	}

//********************
//Mutual Matches
//********************
public function mutualModel()
	{ $params=array('id'=>$this->session->userdata('id'));
	 $query2= $this->get($params);
	   $year= date("Y") -  number_format(substr($query2->dob,0,3))  ;
	   $addQry= " u.active=1 and u.activation_code = '' and u.group_id =2 and  u.id = p.user_id "; 
		   $addQry.= ' and p.gender != "'.$query2->gender.'" and pp.marital = "'.$query2->maritalstatus.'" and pp.religion="'.$query2->religion.'" and pp.eating = "'.$query2->diet.'" and pp.caste = "'.$query2->caste.'" and pp.manglik = "'.$query2->manglik.'" and (YEAR(CURDATE())-YEAR(p.dob) > pp.agefrom) and (YEAR(CURDATE())-YEAR(p.dob) < pp.ageto)   	group by u.id  ';
		   
		     $query = $this->db->query('SELECT * from users u, profiles p,  partnerpreference pp  where  '. $addQry);
			return $query->result_array();  			  
	}
	
//********************
//********************
//Personalised Messages	
//********************
//total message received
//********************

function totalmsgreceive()
{
		  $qyr2= $this->db->query('SELECT * from user_mail where receiver_id = '.$this->session->userdata('id'));
	  		return $qyr2->num_rows();
}
//********************
//total message sent
//********************
function totalmsgsent()
{
		  $qyr2= $this->db->query('SELECT * from user_mail where sender_id = '.$this->session->userdata('id'));
	  		return $qyr2->num_rows();
}
//********************
//total new message not read
//********************
function totalnewmsg()
{
		  $qyr2= $this->db->query('SELECT * from user_mail where   status = "sent"  and receiver_id = '.$this->session->userdata('id'));
	  		return $qyr2->num_rows();
}
//********************
//total  message reply left
//********************
function totalreplyleft()
{ 		
$getTo=0;
 
/*$qyr1= mysql_query('SELECT * from user_mail where  status="read"  and  receiver_id = '.$this->session->userdata('id'));
		   while($ro=mysql_fetch_array($qyr1))	{
		 if($ro['id']!="") { 
		  $qyr2= $this->db->query('SELECT * from user_mail where replyof='.$ro['id'].' and sender_id = '.$this->session->userdata('id'));
		if($qyr2->num_rows() < 1) {   $getTo=$getTo + 1; }
		  } else { $getTo=0;} } 
	  		return $getTo;*/
		
		$qyr1= mysql_query('SELECT * from user_mail where  status="read" and  receiver_id = '.$this->session->userdata('id'));
		   $ro=mysql_num_rows($qyr1);
		   return $ro;
			
		 
}


//********************
//total  message replied by both side
//********************
function totalreplied()
{ 		
//$qyr1= $this->db->query('SELECT * from user_mail where  status IN ("replied, reply")  and  receiver_id = '.$this->session->userdata('id'));

$qyr1= $this->db->query("SELECT * from user_mail where status IN ('replied', 'reply')  and  receiver_id = ".$this->session->userdata('id'));


 return $qyr1->num_rows();
}
//********************
//total  message decline
//********************
function totaldecline()
{ 		
$qyr1= $this->db->query('SELECT * from user_mail where  status="decline"  and  receiver_id = '.$this->session->userdata('id'));
 return $qyr1->num_rows();
}

//********************
//total  messages reply received
//********************
function replyreceived()
{ 		
//$qyr1= $this->db->query('SELECT * from user_mail where  status="reply"  and  receiver_id = '.$this->session->userdata('id'));

$qyr1= $this->db->query('SELECT * from user_mail where  (status="reply" or status="replied") and  sender_id = '.$this->session->userdata('id'));
 return $qyr1->num_rows();
}
//********************
//total  messages read by members
//********************
function msgreaded()
{ 		
$qyr1= $this->db->query('SELECT * from user_mail where  status="read"  and  sender_id = '.$this->session->userdata('id'));
 return $qyr1->num_rows();
}
//********************
//total  messages unread by members
//********************
function msgunreaded()
{ 		
$qyr1= $this->db->query('SELECT * from user_mail where  status="sent"  and  sender_id = '.$this->session->userdata('id'));
 return $qyr1->num_rows();
}
//********************
//total  messages decline by members
//********************
function msgdecline()
{ 		
$qyr1= $this->db->query('SELECT * from user_mail where  status="decline"  and  sender_id = '.$this->session->userdata('id'));
 return $qyr1->num_rows();
}
//********************
//********************
//EXPRESS INTEREST
//********************
//total interest  received
//********************

function eetotalmsgreceive()
{
		  $qyr2= $this->db->query('SELECT * from experss_interest where     receiver_id = '.$this->session->userdata('id'));
	  		return $qyr2->num_rows();
}
//********************
//total interest  sent
//********************
function eetotalmsgsent()
{
		  $qyr2= $this->db->query('SELECT * from experss_interest where sender_id = '.$this->session->userdata('id'));
	  		return $qyr2->num_rows();
}
//********************
//total new interest  not read
//********************
function eetotalnewmsg()
{
		  $qyr2= $this->db->query('SELECT * from experss_interest where   status = "sent"  and receiver_id = '.$this->session->userdata('id'));
	  		return $qyr2->num_rows();
}
 
 //********************
//total  interest accept
//********************
function eetotalaccept()
{ 		
$qyr1= $this->db->query('SELECT * from experss_interest where  status="accept"  and  receiver_id = '.$this->session->userdata('id'));
 return $qyr1->num_rows();
}

//********************
//total  interest decline
//********************
function eetotaldecline()
{ 		
$qyr1= $this->db->query('SELECT * from experss_interest where  status="decline"  and  receiver_id = '.$this->session->userdata('id'));
 return $qyr1->num_rows();
}

//********************
//total  interest sent by me and accepted
//********************
function eesentinterestaccept()
{ 		
$qyr1= $this->db->query('SELECT * from experss_interest where  status="accept"  and  sender_id = '.$this->session->userdata('id'));

 return $qyr1->num_rows();
}
//********************
//total  interest not replied
//********************
function eependinginterst()
{ 		
$qyr1= $this->db->query('SELECT * from experss_interest where  (status="read" or status="sent")  and  sender_id = '.$this->session->userdata('id'));
 return $qyr1->num_rows();
}
//********************
//total  interest decline by members
//********************
function eeinterestdeclined()
{ 		
$qyr1= $this->db->query('SELECT * from experss_interest where  status="decline"  and  sender_id = '.$this->session->userdata('id'));
 return $qyr1->num_rows();
}
//********************
//********************
//REQUEST
//********************
//total  request received
//********************
function totalrrequest()
{ 		
$qyr1= $this->db->query('SELECT * from phone_request where  receiver_id = '.$this->session->userdata('id'));
 
$qyr2= $this->db->query('SELECT * from photo_request where  receiver_id = '.$this->session->userdata('id'));

$qyr3= $this->db->query('SELECT * from horoscope_request where  receiver_id = '.$this->session->userdata('id'));
 
$qyr4= $this->db->query('SELECT * from reference_request where  receiver_id = '.$this->session->userdata('id'));
 
 $totalrows = $qyr1->num_rows() + $qyr2->num_rows() + $qyr3->num_rows() + $qyr4->num_rows();
return $totalrows;
}
 //********************
//total  request sent
//********************
function totalsrequest()
{ 		
$qyr1= $this->db->query('SELECT * from phone_request where  sender_id = '.$this->session->userdata('id'));
 
$qyr2= $this->db->query('SELECT * from photo_request where  sender_id = '.$this->session->userdata('id'));

$qyr3= $this->db->query('SELECT * from horoscope_request where  sender_id = '.$this->session->userdata('id'));
 
$qyr4= $this->db->query('SELECT * from reference_request where  sender_id = '.$this->session->userdata('id'));
 
 $totalrows = $qyr1->num_rows() + $qyr2->num_rows() + $qyr3->num_rows() + $qyr4->num_rows();
return $totalrows;
}
 //********************
//total  phone request received
//********************
function rphonerequest()
{ 		
$qyr1= $this->db->query('SELECT * from phone_request where  receiver_id = '.$this->session->userdata('id'));
return $qyr1->num_rows();
}
 //********************
//total  photo request received
//********************
function rphotorequest()
{ 		
$qyr1= $this->db->query('SELECT * from photo_request where  receiver_id = '.$this->session->userdata('id'));
return $qyr1->num_rows();
}
 //********************
//total  horoscope request received
//********************
function rhoroscoperequest()
{ 		
$qyr1= $this->db->query('SELECT * from horoscope_request where  receiver_id = '.$this->session->userdata('id'));
return $qyr1->num_rows();
}
 //********************
//total  reference request received
//********************
function rreferencerequest()
{ 		
$qyr1= $this->db->query('SELECT * from reference_request where  receiver_id = '.$this->session->userdata('id'));
return $qyr1->num_rows();
}
 //********************
//total  phone request sent
//********************
function sphonerequest()
{ 		
$qyr1= $this->db->query('SELECT * from phone_request where  sender_id = '.$this->session->userdata('id'));
return $qyr1->num_rows();
}
 //********************
//total  photo request sent
//********************
function sphotorequest()
{ 		
$qyr1= $this->db->query('SELECT * from photo_request where  sender_id = '.$this->session->userdata('id'));
return $qyr1->num_rows();
}
 //********************
//total  horoscope request sent
//********************
function shoroscoperequest()
{ 		
$qyr1= $this->db->query('SELECT * from horoscope_request where  sender_id = '.$this->session->userdata('id'));
return $qyr1->num_rows();
}
 //********************
//total  reference request sent
//********************
function sreferencerequest()
{ 		
$qyr1= $this->db->query('SELECT * from reference_request where  sender_id = '.$this->session->userdata('id'));
return $qyr1->num_rows();
}
//********************
//SHORLIST
//********************
//total  shortlist
//********************
function shorlist()
{ 		
$qyr1= $this->db->query('SELECT * from shortlist where  sender_id = '.$this->session->userdata('id'));
return $qyr1->num_rows();
}
//********************
//LISTS AND VIEWS
//********************
//   member i ignored
//********************
function memberignored()
{ 		
$qyr1= $this->db->query('SELECT * from user_ignore where  ignoredby_id = '.$this->session->userdata('id'));
return $qyr1->num_rows();
} 
//********************
//   member i blocked
//********************
function memberiblocked()
{ 		
$qyr1= $this->db->query('SELECT * from user_block where  blocker_id = '.$this->session->userdata('id'));
return $qyr1->num_rows();
} 
//**********************************
//   member phone number view by me
//***********************************
function memberphoneviewbyme()
{ 		
$qyr1= $this->db->query('SELECT * from phonenoview where viewedby_id  = '.$this->session->userdata('id'));
return $qyr1->num_rows();
} 
//**********************************
//   member whoe view my phone number  
//***********************************
function memberviewmyphone()
{ 		
$qyr1= $this->db->query('SELECT * from phonenoview where  viewed_id  = '.$this->session->userdata('id'));
return $qyr1->num_rows();
} 
//**********************************
//   member whoe view my profile
//***********************************
function profileviewed()
{ 		
$qyr1= $this->db->query('SELECT * from profileviewed where  user_viewed_id  = '.$this->session->userdata('id'));
return $qyr1->num_rows();
} 

//**********************************
//*********INVITE REFERENCE*********
//**********************************

function invitationpost()
{

$qyr1= $this->db->query('SELECT * from requestrefer where  refereeemail  = "'.$_POST['refereeemailid'].'"');
if($qyr1->num_rows()<1) { 
  $query=$this->db->query("insert into requestrefer (uid,yourname,refereename,relation,refereeemail,message) value('".$this->session->userdata('id')."','".$this->filter($_POST['memberName'])."','".$this->filter($_POST['refereename'])."','".$this->filter($_POST['relationship'])."','".$this->filter($_POST['refereeemailid'])."','".$this->filter($_POST['membercomments'])."') ")or die(mysql_error());
  $did=mysql_insert_id();
$q=mysql_query("select * from email_templates where slug='referencerequest'");
	$r=mysql_fetch_assoc($q);
	$mailbody1=str_replace("[REFERERNAME]",ucwords($this->filter($_POST['refereename'])),$r['body']);
	$link=base_url()."index.php/users/fullprofile/viewprofile/".$this->session->userdata('id');
	$link1=base_url()."index.php/users/reference/mid=".$this->session->userdata('id');
	$mailbody2=str_replace("[MEMBERNAME]",ucfirst($this->filter($_POST['memberName'])),$mailbody1);
	$mailbody3=str_replace("[BODYMESSAGE]",ucfirst($this->filter($_POST['membercomments'])),$mailbody2);
	$mailbody4=str_replace("[link1]",ucfirst($this->filter($link)),$mailbody3);
	$mailbody5=str_replace("[link2]",ucfirst($this->filter($link1)),$mailbody4);
	echo $mailbody4;
	$headers = "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n From: Swayamvara team<info@wayamvara.com>";
$subject=str_replace("[FRIENDNAME]",ucfirst($this->filter($_POST['memberName'])),$r['subject']);
$to=$this->filter($_POST['refereeemailid']);
 mail($to,$subject,$mailbody5,$headers);
return 1;
}  
 else { return 0;}

}

function filter($str)
{
return mysql_real_escape_string($str);
}

//**********************************
//*********ADD REFERENCE*********
//**********************************

function addreference()
{
$qyr1= $this->db->query('SELECT * from referrer where  emailid  = "'.$_POST['refereeemailid'].'"');
if($qyr1->num_rows()<1) { 
  $query=$this->db->query("insert into referrer (  `userid`,`rname`,`rage`,`rgender`,`relation`,`yearsknow`,`emailid`,`phone`,`comments`) value(".$this->session->userdata('id').",'".$this->filter($_POST['refName'])."',".$_POST['refereeage'].",'".$_POST['refereegender']."','".$_POST['relations']."','".$_POST['duration']."','".$_POST['refereeemail']."','".$_POST['refereephone']."','".$_POST['membercomment']."') ")or die(mysql_error());
 return 1;
}  
 else { return 0;}
}

   public function gallery()
   {    
   
/*$query6 = $this->db->query("SELECT * FROM photo where uid=".$this->session->userdata('id')." limit 1") or mysql_error();*/
$query6 = $this->db->query("SELECT * FROM photo where uid=".$this->session->userdata('id')." order by front desc,id") or mysql_error();

	    return $query6->result_array();  	
    }

// END CLASS
}