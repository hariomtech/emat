<link href="<?php echo css_path('reg-v14.css'); ?>" type="text/css" rel="stylesheet">
<script type="text/javascript" src="<?php echo js_path('registration-v11.js'); ?>"></script>
<script type="text/javascript" src="<?php echo js_path('validation.js'); ?>" ></script>
<script type="text/javascript">
 function validateform()
 {


 if(document.getElementById('email').value=="" || document.getElementById('password1').value=="" || document.getElementById('first_name').value=="" || document.getElementById('last_name').value=="" || document.getElementById('postedby').value=="" || document.getElementById('gender-Male').value=="" || document.getElementById('gender-Female').value=="" || document.getElementById('day').value=="" || document.getElementById('month').value=="" || document.getElementById('year').value=="" || document.getElementById('year').value=="" || document.getElementById('community').value=="" || document.getElementById('mother_tongue').value=="" || document.getElementById('countryofresidence').value=="")
 {
 scroll(900,50);
 document.getElementById('error').style.display='inline';
 return false;
 }



 
 }
 </script>
<div id="formarea">
<h2 class="page-title" id="page_title"><?php echo lang('user_register_header') ?></h2>
 
<p><?php echo lang('user_register_reasons') ?></p>

<?php if(!empty($error_string)):?>
<!-- Woops... -->
<div class="error_box">
	<?php echo $error_string;?>
</div>
<?php endif;?>  
  <div class="heading"> <span>Account Information</span>
    <div>( * Mandatory Fields)</div>
    <div style="float:right;margin-right:18px;_margin-right:10px;font:normal 14px arial;color:#fff">Page 1 of 2</div>
  </div>
  <div class="bg">
    <form method="post" action="<?php echo base_url(); ?>index.php/register/post" name="frm_registration" autocomplete="off" style="margin:0px" >
      <div class="form">
        <div class="error_block" id="error_div" style="display:none;"> <span><img src="Life%20Partner%20-%20Marriage%20-%20Matrimonial%20Sites%20-%20Shaadi.com_files/my-shaadi-v2.gif" width="1500" height="600"></span><b style="display:block;float:left;padding-top:12px">Please correct the following errors. Fields with errors are highlighted below:</b><br>
          <br clear="all">
        </div>
        <div style="min-width:395px;float:left">
          <div class="wrapper">
            <div style="display:none" id="error"> <font color="#FF0000"> Please fill Mandatory Fields !! </font> <br/>
            </div>
            <?php
			if(isset($msg))
			{?>
            <div> <font color="#FF0000"> Email id already exist !! </font> <br/>
            </div>
            <?php }
			?>
            <span id="login_information"><b>Login Information</b></span>
            <div style="padding:12px"></div>
            <label class="label" for="email">Email <span class="mandatory">*</span></label>
            <input name="email" id="email" class="input1" onkeydown="this.className='input1'" onfocus="disable_creative();document.getElementById('errmsg_email').style.display='none';toggleHint_new('show', this.name, this, this.offsetWidth);" onblur="validate_email();" type="text" alt="NOBLANK~Email~DM~EMAIL~Email~">
            <span id="msgbox" style="display:none"></span>
            <div id="errmsg_email" class="error" style="display:none"><span>Incorrect email address format. Please type a valid email address. Example raj_23@yahoo.com</span></div>
            <!-- HINT STARTS HERE -->
            <span class="hint" id="hint_email" style="display: none; left: 561px;">
            <div>
              <div class="top_hint"></div>
              <div class="middle_hint">Example raj_doctor@yahoo.com <br>
                Email address will not be shared with anyone. You will receive matches on this email address.</div>
              <div class="bottom_hint"></div>
            </div>
            <div class="arrow_hint"></div>
            </span>
            <!-- HINT ENDS HERE -->
            <br clear="all">
            <div style="padding:5px"></div>
            <label class="label" for="password1">Password <span class="mandatory">*</span></label>
            <input name="password1" id="password1" value="" class="input1" onkeydown="this.className='input1'" onfocus="disable_creative();display_password();toggleHint_new('show', this.name, this, this.offsetWidth);document.getElementById('errmsg_password1').style.display='none'" onblur="validate_password1();" onkeyup="display_password();" type="password" alt="NOBLANK~Password~DM~">
            <div id="errmsg_password1" class="error" style="display:none;  width:310px"><span>Password is mandatory.</span></div>
            <!-- HINT STARTS HERE -->
            <span class="hint" id="hint_password1" style="display:none;">
            <div>
              <div class="top_hint"></div>
              <div class="middle_hint">Password should be here.</div>
              <div class="bottom_hint"></div>
            </div>
            <div class="arrow_hint"></div>
            </span>
            <!-- HINT ENDS HERE -->
            <br clear="all">
            <div id="display_pwd" name="display_pwd" style="margin-left:180px; font:normal 11px arial;display:none">Your Password - <span id="pwd_typed" name="pwd_typed" style="color:#4EA701"></span></div>
            <div style="padding:15px"></div>
            <span id="basic_information"><b>Basic Information</b></span>
            <div style="padding:12px"></div>
            <label class="label" for="postedby">Profile Created for <span class="mandatory">*</span></label>
            <div id="postedby_div" class="select_error" style="display:inline">
              <select name="postedby" id="postedby" class="select1" style="margin-left:0px" onfocus="disable_creative();toggleHint_new('show', this.name, this, this.offsetWidth)" onchange="profile_for_select_action(this);populate_year_based_on_gender();" onblur="validate_postedby();">
                <option value="" label="Select" selected="selected">Select</option>
                <option value="Self" label="Self">Self</option>
                <option value="Son" label="Son">Son</option>
                <option value="Daughter" label="Daughter">Daughter</option>
                <option value="Brother" label="Brother">Brother</option>
                <option value="Sister" label="Sister">Sister</option>
                <option value="Friend" label="Friend">Friend</option>
                <option value="Relative" label="Relative">Relative</option>
              </select>
            </div>
            <div id="errmsg_postedby" class="error" style="display:none"><span>Relevant relationship information is mandatory.</span></div>
            <!-- HINT STARTS HERE -->
            <span class="hint" id="hint_postedby" style="display:none;">
            <div>
              <div class="top_hint"></div>
              <div class="middle_hint">Please select your relationship with the person looking to get married.<br>
                <br>
              </div>
              <div class="bottom_hint"></div>
            </div>
            <div class="arrow_hint"></div>
            </span>
            <!-- HINT ENDS HERE -->
            <br clear="all">
            <div style="padding:5px"></div>
            <label class="label" for="name">Name <span class="mandatory">*</span> </label>
            <input name="first_name" id="first_name" value="First Name" class="input1 area" style="width: 86px;" onfocus="disable_creative(),name_on_focus(this),toggleHint_new('show',  'name', document.getElementById('last_name'), document.getElementById('last_name').offsetWidth);document.getElementById('errmsg_name').style.display='none';document.getElementById('last_name').className = 'input1'" onblur="validate_name();" maxlength="20" onkeypress="return AllowAlphabet(event,this);" type="text" alt="NOBLANK~Name~DM~">
            <input name="last_name" id="last_name" value="Last Name" class="input1 area" style="width: 86px;" onfocus="disable_creative(),name_on_focus(this),toggleHint_new('show', 'name', this, this.offsetWidth);document.getElementById('errmsg_name').style.display='none';document.getElementById('first_name').className = 'input1'" onblur="validate_name();" maxlength="20" onkeypress="return AllowAlphabet(event,this);" type="text" alt="NOBLANK~Last name~DM~">
            <div id="errmsg_name" class="error" style="display:none"><span></span></div>
            <!-- HINT STARTS HERE -->
            <span class="hint" id="hint_name" style="display:none;">
            <div>
              <div class="top_hint"></div>
              <div class="middle_hint">Please specify name of the person looking to <br>
                get married. The name will not be displayed to other members on the site.<br>
              </div>
              <div class="bottom_hint"></div>
            </div>
            <div class="arrow_hint"></div>
            </span>
            <!-- HINT ENDS HERE -->
            <br clear="all">
            <script language="JavaScript">
				<!--
					set_default_text();
				//-->
				</script>
            <div style="padding:5px"></div>
            <label class="label" for="gender-Male">Gender <span class="mandatory">*</span></label>
            <div class="select_error">
              <label for="gender-Male">
              <input name="gender" id="gender-Male" value="Male" onfocus="disable_creative();toggleHint_new('show', this.name, document.getElementById('year'), document.getElementById('year').offsetWidth-79);document.getElementById('errmsg_gender').style.display='none'" onblur="validate_gender()" onclick="populate_year_based_on_gender()" type="radio">
              Male</label>
              <label for="gender-Female">
              <input name="gender" id="gender-Female" value="Female" onfocus="disable_creative();toggleHint_new('show', this.name, document.getElementById('year'), document.getElementById('year').offsetWidth-79);document.getElementById('errmsg_gender').style.display='none'" onblur="validate_gender()" onclick="populate_year_based_on_gender()" type="radio">
              Female</label>
            </div>
            <div id="errmsg_gender" class="error" style="display:none"><span>Gender is mandatory.</span></div>
            <!-- HINT STARTS HERE -->
            <span class="hint" id="hint_gender" style="display:none;">
            <div>
              <div class="top_hint"></div>
              <div class="middle_hint">Please select the gender of the person<br>
                looking to get married.<br>
                <br>
              </div>
              <div class="bottom_hint"></div>
            </div>
            <div class="arrow_hint"></div>
            </span>
            <!-- HINT ENDS HERE -->
            <br clear="all">
            <div style="padding:5px"></div>
            <label class="label" for="day">Date of Birth <span class="mandatory">*</span></label>
            <div id="dateofbirth_div" class="select_error">
              <select name="day" id="day" class="select1" style="width:50px;margin-right:8px;margin-left:0px" onfocus="disable_creative();toggleHint_new('show', 'dateofbirth', document.getElementById('year'), document.getElementById('year').offsetWidth);document.getElementById('errmsg_dateofbirth').style.display='none'" onblur="validate_dateofbirth();">
                <option value="" label="Day" selected="selected">Day</option>
                <?php
	for($i=1;$i<=31;$i++)
	{
	?>
                <option ><?php echo $i; ?></option>
                <?php }?>
              </select>
              <select name="month" id="month" class="select1" style="width:60px;margin:0px 8px 0px 0px" onfocus="disable_creative();toggleHint_new('show', 'dateofbirth', document.getElementById('year'), document.getElementById('year').offsetWidth);document.getElementById('errmsg_dateofbirth').style.display='none'" onblur="validate_dateofbirth();">
                <option value="" label="Month" selected="selected">Month</option>
                <option value="01" label="Jan">Jan</option>
                <option value="02" label="Feb">Feb</option>
                <option value="03" label="Mar">Mar</option>
                <option value="04" label="Apr">Apr</option>
                <option value="05" label="May">May</option>
                <option value="06" label="Jun">Jun</option>
                <option value="07" label="Jul">Jul</option>
                <option value="08" label="Aug">Aug</option>
                <option value="09" label="Sep">Sep</option>
                <option value="10" label="Oct">Oct</option>
                <option value="11" label="Nov">Nov</option>
                <option value="12" label="Dec">Dec</option>
              </select>
              <select name="year" id="year" class="select1" style="width:57px;margin:0px" onfocus="disable_creative();toggleHint_new('show', 'dateofbirth',  this, this.offsetWidth);document.getElementById('errmsg_dateofbirth').style.display='none'" onblur="validate_dateofbirth();">
                <option value="" label="Year" selected="selected">Year</option>
                <?php
   for($i=date(Y)-18;$i>=1942;$i--)
   {
   ?>
                <option><?php echo $i; ?></option>
                <?php }?>
              </select>
            </div>
            <div id="errmsg_dateofbirth" class="error" style="display:none"><span>Day is mandatory.</span></div>
            <!-- HINT STARTS HERE -->
            <span class="hint" id="hint_dateofbirth" style="display:none;">
            <div>
              <div class="top_hint"></div>
              <div class="middle_hint">Please select date of birth of the person looking to get married. (Visible only to you)<br>
                <br>
              </div>
              <div class="bottom_hint"></div>
            </div>
            <div class="arrow_hint"></div>
            </span>
            <!-- HINT ENDS HERE -->
            <br clear="all">
            <div style="padding:5px"></div>
            <label class="label" for="community">Religion <span class="mandatory">*</span></label>
            <div id="community_div" class="select_error">
              <select name="community" id="community" class="select1" style="margin-left:0px" onfocus="disable_creative();toggleHint_new('show', 'religion', this, this.offsetWidth);document.getElementById('errmsg_community').style.display='none'" onblur="validate_community();">
                <option value="" label="Select" selected="selected">Select</option>
                <option value="Hindu" label="Hindu">Hindu</option>
                <option value="Muslim" label="Muslim">Muslim</option>
                <option value="Christian" label="Christian">Christian</option>
                <option value="Sikh" label="Sikh">Sikh</option>
                <option value="Parsi" label="Parsi">Parsi</option>
                <option value="Jain" label="Jain">Jain</option>
                <option value="Buddhist" label="Buddhist">Buddhist</option>
                <option value="Jewish" label="Jewish">Jewish</option>
                <option value="No Religion" label="No Religion">No Religion</option>
                <option value="Spiritual - not religious" label="Spiritual">Spiritual</option>
                <option value="Other" label="Other">Other</option>
              </select>
              <br clear="all">
            </div>
            <div id="errmsg_community" class="error" style="display:none"><span>Religion is mandatory.</span></div>
            <!-- HINT STARTS HERE -->
            <span class="hint" id="hint_religion" style="display:none;">
            <div>
              <div class="top_hint"></div>
              <div class="middle_hint">Please select religion of the person looking to get married.<br>
                <br>
              </div>
              <div class="bottom_hint"></div>
            </div>
            <div class="arrow_hint"></div>
            </span>
            <!-- HINT ENDS HERE -->
            <br clear="all">
            <div style="padding:5px"></div>
            <label class="label" for="mother_tongue">Mother Tongue <span class="mandatory">*</span></label>
            <div id="mother_tongue_div" class="select_error">
              <select name="mother_tongue" id="mother_tongue" class="select1" style="margin-left:0px" onfocus="disable_creative();toggleHint_new('show', this.name, this, this.offsetWidth);document.getElementById('errmsg_mother_tongue').style.display='none'" onblur="validate_mother_tongue();">
                <option value="" label="Select" selected="selected">Select</option>
                <optgroup label="Top Mothertongue"> </optgroup>
                <option value="Hindi" label="Hindi">Hindi</option>
                <option value="Gujarati" label="Gujarati">Gujarati</option>
                <option value="Urdu" label="Urdu">Urdu</option>
                <option value="English" label="English">English</option>
                <option value="Punjabi" label="Punjabi">Punjabi</option>
                <option value="Tamil" label="Tamil">Tamil</option>
                <option value="Marathi" label="Marathi">Marathi</option>
                <option value="Telugu" label="Telugu">Telugu</option>
                <option value="Malayalam" label="Malayalam">Malayalam</option>
                <option value="Bengali" label="Bengali">Bengali</option>
                <option value="Kannada" label="Kannada">Kannada</option>
                <option value="Sindhi" label="Sindhi">Sindhi</option>
                <option value="Konkani" label="Konkani">Konkani</option>
                <option value="Oriya" label="Oriya">Oriya</option>
                <option value="Assamese" label="Assamese">Assamese</option>
                <option value="Marwari" label="Marwari">Marwari</option>
                <optgroup label="All Mothertongue"> </optgroup>
                <option value="Aka" label="Aka">Aka</option>
                <option value="Arabic" label="Arabic">Arabic</option>
                <option value="Arunachali" label="Arunachali">Arunachali</option>
                <option value="Awadhi" label="Awadhi">Awadhi</option>
                <option value="Baluchi" label="Baluchi">Baluchi</option>
                <option value="Bhojpuri" label="Bhojpuri">Bhojpuri</option>
                <option value="Bhutia" label="Bhutia">Bhutia</option>
                <option value="Brahui" label="Brahui">Brahui</option>
                <option value="Brij" label="Brij">Brij</option>
                <option value="Burmese" label="Burmese">Burmese</option>
                <option value="Chattisgarhi" label="Chattisgarhi">Chattisgarhi</option>
                <option value="Chinese" label="Chinese">Chinese</option>
                <option value="Coorgi" label="Coorgi">Coorgi</option>
                <option value="Dogri" label="Dogri">Dogri</option>
                <option value="French" label="French">French</option>
                <option value="Garhwali" label="Garhwali">Garhwali</option>
                <option value="Garo" label="Garo">Garo</option>
                <option value="Haryanavi" label="Haryanavi">Haryanavi</option>
                <option value="Himachali/Pahari" label="Himachali/Pahari">Himachali/Pahari</option>
                <option value="Hindko" label="Hindko">Hindko</option>
                <option value="Kakbarak" label="Kakbarak">Kakbarak</option>
                <option value="Kanauji" label="Kanauji">Kanauji</option>
                <option value="Kashmiri" label="Kashmiri">Kashmiri</option>
                <option value="Khandesi" label="Khandesi">Khandesi</option>
                <option value="Khasi" label="Khasi">Khasi</option>
                <option value="Koshali" label="Koshali">Koshali</option>
                <option value="Kumaoni" label="Kumaoni">Kumaoni</option>
                <option value="Kutchi" label="Kutchi">Kutchi</option>
                <option value="Ladakhi" label="Ladakhi">Ladakhi</option>
                <option value="Lepcha" label="Lepcha">Lepcha</option>
                <option value="Magahi" label="Magahi">Magahi</option>
                <option value="Maithili" label="Maithili">Maithili</option>
                <option value="Malay" label="Malay">Malay</option>
                <option value="Manipuri" label="Manipuri">Manipuri</option>
                <option value="Miji" label="Miji">Miji</option>
                <option value="Mizo" label="Mizo">Mizo</option>
                <option value="Monpa" label="Monpa">Monpa</option>
                <option value="Nepali" label="Nepali">Nepali</option>
                <option value="Pashto" label="Pashto">Pashto</option>
                <option value="Persian" label="Persian">Persian</option>
                <option value="Rajasthani" label="Rajasthani">Rajasthani</option>
                <option value="Russian" label="Russian">Russian</option>
                <option value="Sanskrit" label="Sanskrit">Sanskrit</option>
                <option value="Santhali" label="Santhali">Santhali</option>
                <option value="Seraiki" label="Seraiki">Seraiki</option>
                <option value="Sinhala" label="Sinhala">Sinhala</option>
                <option value="Sourashtra" label="Sourashtra">Sourashtra</option>
                <option value="Spanish" label="Spanish">Spanish</option>
                <option value="Swedish" label="Swedish">Swedish</option>
                <option value="Tagalog" label="Tagalog">Tagalog</option>
                <option value="Tulu" label="Tulu">Tulu</option>
                <option value="Other" label="Other">Other</option>
              </select>
            </div>
            <div id="errmsg_mother_tongue" class="error" style="display:none"><span>Mother tongue is mandatory.</span></div>
            <!-- HINT STARTS HERE -->
            <span class="hint" id="hint_mother_tongue" style="display:none;">
            <div>
              <div class="top_hint"></div>
              <div class="middle_hint">Please select mother tongue of the person looking to get married.<br>
                <br>
              </div>
              <div class="bottom_hint"></div>
            </div>
            <div class="arrow_hint"></div>
            </span>
            <!-- HINT ENDS HERE -->
            <br clear="all">
            <div style="padding:5px"></div>
            <label class="label" for="countryofresidence">Country of Residence <span class="mandatory">*</span></label>
            <div id="countryofresidence_div" class="select_error">
              <select name="countryofresidence" id="countryofresidence" class="select1" style="margin-left:0px" onfocus="disable_creative();toggleHint_new('show', this.name, this, this.offsetWidth);document.getElementById('errmsg_countryofresidence').style.display='none'" onblur="validate_countryofresidence();">
                <option value="" label="Select" selected="selected">Select</option>
                <optgroup label="Top Countries"> </optgroup>
                <option value="India" label="India">India</option>
                <option value="USA" label="USA">USA</option>
                <option value="United Kingdom" label="United Kingdom">United Kingdom</option>
                <option value="United Arab Emirates" label="United Arab Emirates">United Arab Emirates</option>
                <option value="Canada" label="Canada">Canada</option>
                <option value="Australia" label="Australia">Australia</option>
                <option value="Pakistan" label="Pakistan">Pakistan</option>
                <option value="Saudi Arabia" label="Saudi Arabia">Saudi Arabia</option>
                <option value="Kuwait" label="Kuwait">Kuwait</option>
                <option value="South Africa" label="South Africa">South Africa</option>
              </select>
            </div>
            <div id="errmsg_countryofresidence" class="error" style="display:none"><span>Country of residence is mandatory.</span></div>
            <br/>
            <div> <span id="basic_information"><b>Membership</b></span>
              <div> Membership *&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input type="radio" name="membership" value="0" checked="checked" />
                Regular
                &nbsp;&nbsp;
                <input type="radio" name="membership" value="1" />
                Silver
                &nbsp;&nbsp;
                <input type="radio" name="membership" value="2" />
                Gold </div>
            </div>
            <!-- HINT STARTS HERE -->
            <span class="hint" id="hint_countryofresidence" style="display:none;">
            <div>
              <div class="top_hint"></div>
              <div class="middle_hint">Please select country of current residence of the person looking to get married.<br>
                <br>
              </div>
              <div class="bottom_hint"></div>
            </div>
            <div class="arrow_hint"></div>
            </span>
            <!-- HINT ENDS HERE -->
            <br clear="all">
          </div>
        </div>
        <div style="padding: 44px 0px 0px; text-align: center; display: none;" class="creative" id="creative"> <img src="Life%20Partner%20-%20Marriage%20-%20Matrimonial%20Sites%20-%20Shaadi.com_files/free-matches-cr-1.gif" alt="Register Now and start receiving FREE Matches by Email instantly!" title="Register Now and start receiving FREE Matches by Email instantly!" width="287" border="0" height="117"><br>
          <img src="Life%20Partner%20-%20Marriage%20-%20Matrimonial%20Sites%20-%20Shaadi.com_files/free-matches-cr-2.gif" alt="Register Now and start receiving FREE Matches by Email instantly!" title="Register Now and start receiving FREE Matches by Email instantly!" width="287" border="0" height="167"><br>
          <div style="text-align:left;padding-left:55px;font:normal 12px arial;color:#333">
            <div style="font:bold 14px arial;color:#F8991D;margin-bottom: 3px;">Shaadi.com - Trusted since 1996</div>
            <img src="Life%20Partner%20-%20Marriage%20-%20Matrimonial%20Sites%20-%20Shaadi.com_files/bullet-disc.gif" style="margin-right: 5px;" width="8" border="0" height="9">Best Matches<br>
            <img src="Life%20Partner%20-%20Marriage%20-%20Matrimonial%20Sites%20-%20Shaadi.com_files/bullet-disc.gif" style="margin-right: 5px;" width="8" border="0" height="9">Most Responses<br>
            <img src="Life%20Partner%20-%20Marriage%20-%20Matrimonial%20Sites%20-%20Shaadi.com_files/bullet-disc.gif" style="margin-right: 5px;" width="8" border="0" height="9">Fully Secure </div>
        </div>
        <div style="width:531px;margin-left:192px;"> <br style="line-height:0px" clear="all">
          <input name="confirm_policy" id="confirm_policy" value="1" checked="checked" onclick="show_hide_tnc_err();" type="checkbox">
          I agree <a href="#" target="_blank" class="blue">Privacy Policy</a> and <a href="#" target="_blank" class="blue">Terms and Conditions</a>.<span class="mandatory">*</span><br>
          <div class="select_error" style="margin:-1px;border:none;">
            <input name="shaaditimes" id="shaaditimes" value="1" checked="checked" onclick="show_hide_tnc_err();" type="checkbox">
            Send me newsletters and exclusive offers  (twice a month) </div>
          <br>
          <br>
        </div>
      </div>
      <!--[if (IE 6)|(IE 7)]><div style="padding:1px"></div><![endif]-->
      <span>
      <input type="image" src="/swayamvara/uploads/files/register-me.gif" style="border: medium none; margin: 16px 0px 0px 192px; cursor: pointer;" onclick="return validateform(); " width="176" height="43"/>
      </span>
    </form>
  </div>
</div>
