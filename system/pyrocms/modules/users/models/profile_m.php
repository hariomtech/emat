<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @author 		Phil Sturgeon - PyroCMS Development Team
 * @package 	PyroCMS
 * @subpackage 	Users Module
 * @since		v0.1
 *
 */
class Profile_m extends MY_Model {
	
	/**
	 * Get a user profile
	 *
	 * @access public
	 * @param array $params Parameters used to retrieve the profile
	 * @return object
	 */
	 public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->config('users/ion_auth', TRUE);
		$this->load->helper('cookie');
		$this->load->helper('date');
		$this->load->library('session');
		$this->tables  = $this->config->item('tables', 'ion_auth');
		$this->columns = $this->config->item('columns', 'ion_auth');

		$this->identity_column = $this->config->item('identity', 'ion_auth');
	    $this->store_salt      = $this->config->item('store_salt', 'ion_auth');
	    $this->salt_length     = $this->config->item('salt_length', 'ion_auth');
	    $this->meta_join       = $this->config->item('join', 'ion_auth');
	}
	function get_profile($params = array())
	{
		$query = $this->db->get_where('profiles', $params);

		return $query->row();
	}
	
	/**
	 * Update a user's profile
	 *
	 * @access public
	 * @param array $input A mirror of $_POST
	 * @param int $id The ID of the profile to update
	 * @return bool
	 */
	function update_profile($input, $id) {
		
		$this->load->helper('date');
            
		$set = array(
			'gender'		=> 	$input['gender'],
			'bio'			=> 	$input['bio'],

			'phone'			=>	$input['phone'],
			'mobile'		=>	$input['mobile'],
			'address_line1'	=>	$input['address_line1'],
			'address_line2'	=>	$input['address_line2'],
			'address_line3'	=>	$input['address_line3'],
			'postcode'		=>	$input['postcode'],
	 		'website'		=>	$input['website'],
	
			'msn_handle'	=>	$input['msn_handle'],
			'aim_handle'	=>	$input['aim_handle'],
			'yim_handle'	=>	$input['yim_handle'],
			'gtalk_handle'	=>	$input['gtalk_handle'],
			'updated_on'	=>	now()
		);

		if(isset($input['dob_day'])){
			$set['dob'] = mktime(0, 0, 0, $input['dob_month'], $input['dob_day'], $input['dob_year']);
		}

		// Does this user have a profile already?
		if($this->db->get_where('profiles', array('user_id' => $id))->row()):
			$this->db->update('profiles', $set, array('user_id'=>$id));
			
		else:
			$set['user_id'] = $id;
			$this->db->insert('profiles', $set);
		endif;
		
		return TRUE;
	}
	
	public function editprofile($id)
	{
	
	//echo "update profiles set first_name='".$_POST['fname']."',last_name='".$_POST['lname']."',dob='".$_POST['DOBYEAR']."-".$_POST['DOBMONTH']."-".$_POST['day']."',maritalstatus='".$_POST['MARITAL_STATUS']."',height='".$_POST['height']."',weight='".$_POST['KGS']."',bodytype='".$_POST['bodytype']."',complexion='".$_POST['complexion']."',specialclasses='".$_POST['specialcases']."',blood='".$_POST['BLOOD_GROUP']."',mother_tongue='".$_POST['MOTHERTONGUE']."',createdby='".$_POST['REGISTERED_BY']."',religion='".$_POST['RELIGION']."',caste='".$_POST['castearray']."',subcaste='".$_POST['SUBCASTE']."',willingmarry='".$_POST['CASTENOBAR']."',star='".$_POST['STAR']."',raasi='".$_POST['RAASI']."',horoscopematch='".$_POST['HOROMATCH']."',manglik='".$_POST['manglik']."',diet='".$_POST['diet']."',smoke='".$_POST['smoke']."',drink='".$_POST['drink']."',bio='".$_POST['DESCRIPTION']."' where user_id=".$id;
mysql_query("update profiles set first_name='".$_POST['fname']."',last_name='".$_POST['lname']."',dob='".$_POST['DOBYEAR']."-".$_POST['DOBMONTH']."-".$_POST['day']."',maritalstatus='".$_POST['MARITAL_STATUS']."',height='".$_POST['height']."',weight='".$_POST['KGS']."',bodytype='".$_POST['bodytype']."',complexion='".$_POST['complexion']."',specialclasses='".$_POST['specialcases']."',blood='".$_POST['BLOOD_GROUP']."',mother_tongue='".$_POST['MOTHERTONGUE']."',createdby='".$_POST['REGISTERED_BY']."',religion='".$_POST['RELIGION']."',caste='".$_POST['castearray']."',subcaste='".$_POST['SUBCASTE']."',willingmarry='".$_POST['CASTENOBAR']."',star='".$_POST['STAR']."',raasi='".$_POST['RAASI']."',horoscopematch='".addslashes($_POST['HOROMATCH'])."',manglik='".addslashes($_POST['manglik'])."',diet='".$_POST['diet']."',smoke='".$_POST['smoke']."',drink='".$_POST['drink']."',bio='".mysql_escape_string($_POST['DESCRIPTION'])."' where user_id=".$id)or die(mysql_error());
	}
public function editedu($id)
{
mysql_query("update profiles set education='".$_POST['EDUCATION']."',educationdetail='".$_POST['OTHEREDUCATION']."',workingwith='".$_POST['working_with']."',workingas='".$_POST['occupation']."',worddetail='".$_POST['OTHEROCCUPATION']."',income='".$_POST['annualincome']."' where user_id=".$id)or die(mysql_error());
}

public function editfamily($id)
{
//echo "update profiles set personalvalue='".$_POST['family_value']."',familytype='".$_POST['family_type']."',familystatus='".$_POST['personal_values']."',fatherwork='".$_POST['FATHEROCCUPATION']."',motherwork='".$_POST['MOTHEROCCUPATION']."',familyorigin='".$_POST['ANCESTRALORIGIN']."',nofbrother='".$_POST['BROTHERS']."',nofsister='".$_POST['SISTERS']."',bromarry='".$_POST['MARRIEDBROTHERS']."',sismarry='".$_POST['MARRIEDSISTERS']."' where user_id=".$id;

   mysql_query("update profiles set personalvalue='".$_POST['family_value']."',familytype='".$_POST['family_type']."',familystatus='".$_POST['personal_values']."',fatherwork='".$_POST['FATHEROCCUPATION']."',motherwork='".$_POST['MOTHEROCCUPATION']."',familyorigin='".$_POST['ANCESTRALORIGIN']."',nofbrother='".$_POST['BROTHERS']."',nofsister='".$_POST['SISTERS']."',bromarry='".$_POST['MARRIEDBROTHERS']."',sismarry='".$_POST['MARRIEDSISTERS']."',aboutfamily='".$_POST['FAMILYDESCRIPTION']."' where user_id=".$id)or die(mysql_error());
   
   
//echo "update profiles set personalvalue='".$_POST['family_value']."',familytype='".$_POST['family_type']."',familystatus='".$_POST['personal_values']."',fatherwork='".$_POST['FATHEROCCUPATION']."',motherwork='".$_POST['MOTHEROCCUPATION']."',familyorigin='".$_POST['ANCESTRALORIGIN']."',nofbrother='".$_POST['BROTHERS']."',nofsister='".$_POST['SISTERS']."',bromarry='".$_POST['MARRIEDBROTHERS']."',sismarry='".$_POST['MARRIEDSISTERS']."',aboutfamily='".$_POST['FAMILYDESCRIPTION']."' where user_id=".$id;
}

public function showhobby($id)
{
//echo 'select * from hobbies where user_id='.$id;
  $query=$this->db->query('select * from hobbies where user_id='.$id)or die(mysql_error());
   return $query->result_array();
//  return $query;
}

public function edithobby($id)
{
$hobby=implode(',',$_POST['HOBBIES']);
 if($hobby!="" && $_POST['HOBBIESDESC'])
	   {
	    $hobby.=",other:".$_POST['HOBBIESDESC'];
	   }
	  
	   else if($_POST['HOBBIESDESC'])
	   {
	   $hobby="other:".$_POST['HOBBIESDESC'];
	   }
$INTEREST=implode(',',$_POST['INTEREST']);
 if($INTEREST!="" && $_POST['INTERESTDESC'])
	   {
	    $INTEREST.=",other:".$_POST['INTERESTDESC'];
	   }
	   else if($_POST['INTERESTDESC'])
	   {
	   $INTEREST="other:".$_POST['INTERESTDESC'];
	   }
$MUSIC=implode(',',$_POST['MUSIC']);
 if($MUSIC!="" && $_POST['MUSICDESC'])
	   {
	    $MUSIC.=",other:".$_POST['MUSICDESC'];
	   }
	   else  if($_POST['MUSICDESC'])
	   {
	   $MUSIC="other:".$_POST['MUSICDESC'];
	   }
$READ=implode(',',$_POST['READ']);
 if($READ!="" && $_POST['READDESC'])
	   {
	   $READ.=",other:".$_POST['READDESC'];
	   }
	   else if($_POST['READDESC'])
	   {
	   $READ="other:".$_POST['READDESC'];
	   }
$MOVIE=implode(',',$_POST['MOVIE']);
if($MOVIE!="" && $_POST['MOVIEDESC'])
	   {
	   $MOVIE.=",other:".$_POST['MOVIEDESC'];
	   }
	   else if($_POST['MOVIEDESC'])
	   {
	   $MOVIE="other:".$_POST['MOVIEDESC'];
	   }
$FOOD=implode(',',$_POST['FOOD']);
 if($FOOD!="" && $_POST['FOODDESC'])
	   {
	    $FOOD.=",other:".$_POST['FOODDESC'];
	   }
	   else if($_POST['FOODDESC'])
	   {
	   $FOOD='other:'.$_POST['FOODDESC'];
	   }
$DRESS=implode(',',$_POST['DRESS']);
if($DRESS!="" && $_POST['DRESSDESC'])
	   {
	    $DRESS.=",other:".$_POST['DRESSDESC'];
	   }
	   else if($_POST['DRESSESC'])
	   {
	   $DRESS='other:'.$_POST['DRESSESC'];
	   }

$SPOKENLANG=implode(',',$_POST['SPOKENLANG']);
if($SPOKENLANG!="" && $_POST['SPOKENLANGDESC'])
	   {
	    $SPOKENLANG.=",other:".$_POST['SPOKENLANGDESC'];
	   }
	   else if($_POST['SPOKENLANGDESC'])
	   {
	   $SPOKENLANG='other:'.$_POST['SPOKENLANGDESC'];
	   }



$SPORTS=implode(',',$_POST['SPORTS']);
 if($SPORTS!="" && $_POST['SPORTSDESC'])
	   {
	    $SPORTS.=",other:".$_POST['SPORTSDESC'];
	   }
	   else if($_POST['SPORTSDESC'])
	   {
	    $SPORTS='other:'.$_POST['SPORTSDESC'];
	   }

mysql_query('delete from hobbies where user_id='.$id);
mysql_query("insert into hobbies(user_id,hobby,interest,music,read1,movie,sports,food,dress,spoke) value('".$id."','".$hobby."','".$INTEREST."','".$MUSIC."','".$READ."','".$MOVIE."','".$SPORTS."','".$FOOD."','".$DRESS."','".$SPOKENLANG."')")or die(mysql_error());

}

public function showpartner($id)
{
  $query=$this->db->query('select * from partnerpreference where userid='.$id)or die(mysql_error());
   return $query->result_array(); 
}

public function editpartner($id)
{
$j=0;
for($i=1;$i<=5;$i++)
{
if($_POST['MARITAL_STATUS'.$i]!="" && $j!=0)
{
$mstatus.=",".$_POST['MARITAL_STATUS'.$i];
}
else if($_POST['MARITAL_STATUS'.$i]!="" && $j==0)
{
$mstatus=$_POST['MARITAL_STATUS'.$i];
}
}
$j=0;
for($i=1;$i<=3;$i++)
{
if($_POST['MANGLIK'.$i]!="" && $j!=0)
{
$manglik.=",".$_POST['MANGLIK'.$i];
}
else if($_POST['MANGLIK'.$i]!="" && $j==0)
{
$manglik=$_POST['MANGLIK'.$i];
}
}
$j=0;
for($i=1;$i<=6;$i++)
{
if($_POST['EATINGHABITS'.$i]!="" && $j!=0)
{
$eat.=",".$_POST['EATINGHABITS'.$i];
}
else if($_POST['EATINGHABITS'.$i]!="" && $j==0)
{
$eat=$_POST['EATINGHABITS'.$i];
}
}
$j=0;
for($i=1;$i<=3;$i++)
{
if($_POST['DRINKING'.$i]!="" && $j!=0)
{
$drink.=",".$_POST['DRINKING'.$i];
}
else if($_POST['DRINKING'.$i]!="" && $j==0)
{
$drink=$_POST['DRINKING'.$i];
}
}
$j=0;
for($i=1;$i<=4;$i++)
{
if($_POST['SMOKING'.$i]!="" && $j!=0)
{
$smoke.=",".$_POST['SMOKING'.$i];
}
else if($_POST['SMOKING'.$i]!="" && $j==0)
{
$smoke=$_POST['SMOKING'.$i];
}
}

$height=$_POST['FROMFEET'] ." to ".$_POST['TOFEET'];

//echo "insert into partnerpreference(userid,agefrom,ageto,marital,height,physicalstatus,religion,caste,subcaste,manglik,  	star,eating,drinking,smoking,mothertongue,country,citizenship,education,occupation,annualincome,partnerdesc) value('".$id."','".$_POST['STAGE']."','".$_POST['ENAGE']."','".$mstatus."','".$heights."','".$_POST['PHYSICALSTATUS']."','".$_POST['RIGHTRELIGION']."','".$_POST['CASTE']."','".$_POST['subcast']."','".$_POST['subcast']."','".$manglik."','".$star1."','".$eat."','".$drink."','".$smoke."','".$_POST['MOTHERTONGUE']."','".$_POST['COUNTRY']."','".$_POST['EDUCATION']."','".$_POST['OCCUPTION']."','".$_POST['FROMINCOME']."','".$_POST['partnerdesctextarea']."')";
 
mysql_query('delete from partnerpreference where userid='.$id)or die(mysql_error());
mysql_query("insert into partnerpreference(userid,agefrom,ageto,marital,height,physicalstatus,religion,caste,subcaste,manglik,  	star,eating,drinking,smoking,mothertongue,country,education,occupation,annualincome,partnerdesc) value('".$id."','".$_POST['STAGE']."','".$_POST['ENAGE']."','".mysql_real_escape_string($mstatus)."','".mysql_real_escape_string($height)."','".mysql_real_escape_string($_POST['PHYSICALSTATUS'])."','".mysql_real_escape_string($_POST['RIGHTRELIGION'])."','".mysql_real_escape_string($_POST['CASTE'])."','".mysql_real_escape_string($_POST['subcast'])."','".mysql_real_escape_string($manglik)."','".mysql_real_escape_string($star1)."','".mysql_real_escape_string($eat)."','".mysql_real_escape_string($drink)."','".mysql_real_escape_string($smoke)."','".mysql_real_escape_string($_POST['MOTHERTONGUE'])."','".mysql_real_escape_string($_POST['COUNTRY'])."','".mysql_real_escape_string($_POST['EDUCATION'])."','".mysql_real_escape_string($_POST['OCCUPTION'])."','".mysql_real_escape_string($_POST['FROMINCOME'])."','".mysql_real_escape_string($_POST['partnerdesctextarea'])."')")or die(mysql_error());
}
public function hash_password($password, $salt=false)
	{
	    if (empty($password))
	    {
	    	return FALSE;
	    }

	    if ($this->store_salt && $salt)
		{
			return  sha1($password . $salt);
		}
		else
		{
	    	$salt = $this->salt();
	    	return  $salt . substr(sha1($salt . $password), 0, -$this->salt_length);
		}
	}

	/**
	 * This function takes a password and validates it
     * against an entry in the users table.
	 *
	 * @return void
	 * @author Mathew
	 **/
	public function hash_password_db($identity, $password)
	{
	   if (empty($identity) || empty($password))
	   {
		return FALSE;
	   }

	   $query = $this->db->select('password')
	   					 ->select('salt')
						 ->where('id', $identity)
						 
						 ->limit(1)

						 ->get($this->tables['users']);

	$result = $query->row();

		if ($query->num_rows() !== 1)
		{
		    return FALSE;
		}

		if ($this->store_salt)
		{
			return sha1($password . $result->salt);
		}
		else
		{
			$salt = substr($result->password, 0, $this->salt_length);

			return $salt . substr(sha1($salt . $password), 0, -$this->salt_length);
		}
	}



public function editpassword($id)
{
$old= $this->hash_password_db($id, $_POST['cpassword']);
 
//echo 'select * from users where id='.$id." and password='".$old."'";
$query=  mysql_query('select * from users where id='.$id." and password='".$old."'")or die(mysql_error());
 mysql_num_rows($query);
if(mysql_num_rows($query)<=0)
{
return('notexist');
}
else
{
$query=mysql_query('select * from users where id='.$id." and password='".$old."'")or die(mysql_error());
$row=mysql_fetch_array($query);
$new= $this->hash_password($_POST['npassword'], $row['salt']);
mysql_query("update users set password='".$new."' where id=".$id)or die(mysql_error());
return('success');
}
}

public function editemail($id)
{
mysql_query("update users set email='".$_POST['email']."' where id=".$id)or die(mysql_error());
}	

public function editmobile($id)
{
$ph=$_POST['mobile'];
mysql_query("update profiles set mobile='".$ph."' where user_id=".$id)or die(mysql_error());
}	



public function editactivation($id)	
{
mysql_query("update profiles set activestatus='".$_POST['acive']."' where user_id=".$id)or die(mysql_error());
}

public function deleteac($id)
 {
 
 mysql_query("insert into deleteac(uid,reason,comment) value('".$id."','".$_POST['reason']."','".$_POST['comment']."')")or die(mysql_error());
 mysql_query('update users set active=2 where id='.$id)or die(mysql_error());
 }
 
 public function editlocation($id)
 {
  mysql_query("update profiles set country='".$_POST['country']."',stateresidence='".$_POST['RESIDINGSTATE']."',cityresidence='".$_POST['nearest_city']."',citizenship='".$_POST['citizenship']."',residentstatus='".$_POST['RESIDENTSTATUS']."' where user_id=".$id)or die(mysql_error());
 }
 
 public function photoupload($str)
 {
 $pic=explode('.',$str);
 $new=$pic[0]."_thumb.".$pic[1];
 $q1=mysql_query('select * from photo where uid='.$this->session->userdata('id')." and front=1")or die(mysql_error());
 if(mysql_num_rows($q1)<=0)
 {
 $ch=1;
 }
 else
 {
 $ch=0;
 }
 
 mysql_query("insert into photo(uid,photo_url,thumb,front) value('".$this->session->userdata('id')."','".$str."','".$new."','".$ch."')")or die(mysql_error());
 }
 
 public function getphoto()
 {
 //echo "select * from photo where uid='".$this->session->userdata('id')."'";
 $query=$this->db->query("select * from photo where uid='".$this->session->userdata('id')."' order by front desc, id");
  return $query->result_array();
 }
 
 public function deletephoto($id)
 {
 $query=mysql_query("select * from photo where id='".$id."' and uid='".$this->session->userdata('id')."'")or die(mysql_error());
 $row=mysql_fetch_array($query);
 unlink(base_url().$row['photo_url']);
 unlink(base_url().$row['thumb']);
 //echo "delete from photo where id='".$id."' and uid='".$this->session->userdata('id')."'";
 if($row['front']==1)
 {
     $q1=mysql_query("select * from photo where id!='".$id."' and uid='".$this->session->userdata('id')."'")or die(mysql_error());
	 while($r1=mysql_fetch_array($q1))
	 {
	 mysql_query('update photo set front=1 where id='.$r1['id']);
	 break;
	 }
 }
 mysql_query("delete from photo where id='".$id."' and uid='".$this->session->userdata('id')."'")or die(mysql_error());
 }
 
 public function movefirst($id)
 {
 $query=mysql_query("select uid from photo where id=".$id)or die(mysql_error());
 $row=mysql_fetch_array($query);
 
 mysql_query("update photo set front=0 where uid =".$row['uid'])or die(mysql_error());
  mysql_query("update photo set front=1 where id=".$id)or die(mysql_error());
 
 }
 
}?>