<?php 
ob_start();
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Edittag extends CI_Controller {

public function index()
  {
  if(!$this->session->userdata('id'))
	{
	redirect('users/login');
	}
	if($_POST['send'])
	{
	$this->load->model('profile_m','',TRUE);
	$this->profile_m->editemail($this->session->userdata('id'));  
	$data['msg']='Email updated successfully !!';
	}
	if($_POST['sendmobile'])
	{
	$this->load->model('profile_m','',TRUE);
	$this->profile_m->editmobile($this->session->userdata('id'));  
	$data['msg']='Mobile number updated successfully !!';
	}
	
  $this->load->helper('users/user');
	$this->load->view('header');
	 $this->load->model('profile_model','',TRUE);
	$data['query']=$this->profile_model->showprofile($this->session->userdata('id'));
	$this->load->model('profile_m','',TRUE);
	$data['photo']=$this->profile_m->getphoto();
	$this->load->view('edittag',$data);
	$this->load->view('footer');
  }
  
  public function basicinfo()
  {
  if(!$this->session->userdata('id'))
	{
	redirect('users/login');
	}
  $this->load->helper('users/user');
	$this->load->view('header');
	$this->load->model('profile_model','',TRUE);
	$data['query']=$this->profile_model->showprofile($this->session->userdata('id'));
	$this->load->view('basicinfo',$data);
	$this->load->view('footer');
  }
	
  public function editbasicinfo()
  {
 
  if(!$this->session->userdata('id'))
	{
	
	redirect('users/login');
	}
	 
	$this->load->model('profile_m','',TRUE);
	$this->profile_m->editprofile($this->session->userdata('id'));
  $this->load->helper('users/user');
	$this->load->view('header');
	$this->load->model('profile_model','',TRUE);
	$data['query']=$this->profile_model->showprofile($this->session->userdata('id'));
	$data['msg']='Profile updated successfully';
	$this->load->view('basicinfo',$data);
	$this->load->view('footer');
  }	
  
  public function modifyedu()
  {
   if(!$this->session->userdata('id'))
	{
	
	redirect('users/login');
	}
	 $this->load->helper('users/user');
	$this->load->view('header');
	$this->load->model('profile_model','',TRUE);
	$data['query']=$this->profile_model->showprofile($this->session->userdata('id'));
	$this->load->view('modifyedu',$data);
	$this->load->view('footer');
  }
  
  public function editedu()
  {
    if(!$this->session->userdata('id'))
	{
	redirect('users/login');
	}
	 $this->load->helper('users/user');
	$this->load->view('header');
	$this->load->model('profile_m','',TRUE);
	$this->profile_m->editedu($this->session->userdata('id'));
		$this->load->model('profile_model','',TRUE);
	$data['query']=$this->profile_model->showprofile($this->session->userdata('id'));
	$data['msg']='Profile updated successfully !';
	$this->load->view('modifyedu',$data);
	$this->load->view('footer');
  }
  
  public function familydet()
  {
  if(!$this->session->userdata('id'))
	{
	
	redirect('users/login');
	}
	 $this->load->helper('users/user');
	$this->load->view('header');
	
	$this->load->model('profile_model','',TRUE);
	$data['query']=$this->profile_model->showprofile($this->session->userdata('id'));
	$this->load->view('familydet',$data);
	$this->load->view('footer');
  }
  
  public function editfamilydet()
  {
  if(!$this->session->userdata('id'))
	{
	
	redirect('users/login');
	}
	 $this->load->helper('users/user');
	$this->load->view('header');
	$this->load->model('profile_model','',TRUE);
	$this->load->model('profile_m','',TRUE);
	$this->profile_m->editfamily($this->session->userdata('id'));
	$data['msg']='Profile updated successfully !';
	$data['query']=$this->profile_model->showprofile($this->session->userdata('id'));
	
	$this->load->view('familydet',$data);
	$this->load->view('footer');
  }
  
  public function hobbydet()
  {
   if(!$this->session->userdata('id'))
	{
	
	redirect('users/login');
	}
	 $this->load->helper('users/user');
	$this->load->view('header');
	
	$this->load->model('profile_m','',TRUE);
	$data['query']=$this->profile_m->showhobby($this->session->userdata('id'));
	$this->load->view('hobbydet',$data);
	$this->load->view('footer');
  
  }
   public function edithobby()
  {
   if(!$this->session->userdata('id'))
	{
	
	redirect('users/login');
	}
	 $this->load->helper('users/user');
	$this->load->view('header');
	$this->load->model('profile_m','',TRUE);
	$data['query']=$this->profile_m->edithobby($this->session->userdata('id'));
	$this->load->model('profile_m','',TRUE);
	$data['msg']='Profile updated successfully !';
	$data['query']=$this->profile_m->showhobby($this->session->userdata('id'));
	$this->load->view('hobbydet',$data);
	$this->load->view('footer');
  
  }
	
	 public function partnerdet()
  {
   if(!$this->session->userdata('id'))
	{
	
	redirect('users/login');
	}
	 $this->load->helper('users/user');
	$this->load->view('header');
	
	$this->load->model('profile_m','',TRUE);
	
	$data['query']=$this->profile_m->showpartner($this->session->userdata('id'));
	$this->load->view('partnerdet',$data);
	$this->load->view('footer');
  
  }
  
  public function editpartnerdel()
  {
  if(!$this->session->userdata('id'))
	{
	
	redirect('users/login');
	}
	 $this->load->helper('users/user');
	$this->load->view('header');
	
	$this->load->model('profile_m','',TRUE);
	$this->profile_m->editpartner($this->session->userdata('id'));
	$data['query']=$this->profile_m->showpartner($this->session->userdata('id'));
	$data['msg']='Profile updated successfully !';
	$this->load->view('partnerdet',$data);
	$this->load->view('footer');
  }
  
  public function changepassword()
  {
   if(!$this->session->userdata('id'))
	{
	
	redirect('users/login');
	}
	 $this->load->helper('users/user');
	$this->load->view('header');
	$this->load->view('password');
	$this->load->view('footer');
  }
  
  public function editpassword()
  {
  if(!$this->session->userdata('id'))
	{
	
	redirect('users/login');
	}
	 $this->load->helper('users/user');
	$this->load->view('header');
	
	$this->load->model('profile_m','',TRUE);
	$query=$this->profile_m->editpassword($this->session->userdata('id'));
	if($query=='success')
	{
	$data['msg']='Profile updated successfully !';
	}
	else
	{
	$data['msg']='Mismatch current password !';
	}
	$this->load->view('password',$data);
	$this->load->view('footer');
  }
  
  public function active()
  {
  if(!$this->session->userdata('id'))
	{
	
	redirect('users/login');
	}
	 $this->load->helper('users/user');
	$this->load->view('header');
	 $this->load->model('profile_model','',TRUE);
	$data['query']=$this->profile_model->showprofile($this->session->userdata('id'));
	$this->load->view('active',$data);
	$this->load->view('footer');
  }
  
  public function editactivation()
  {
  if(!$this->session->userdata('id'))
	{
	
	redirect('users/login');
	}
	 $this->load->helper('users/user');
	$this->load->view('header');
	$this->load->model('profile_m','',TRUE);
	$this->profile_m->editactivation($this->session->userdata('id'));
	 $this->load->model('profile_model','',TRUE);
	$data['query']=$this->profile_model->showprofile($this->session->userdata('id'));
	$data['msg']='Profile '.$_POST['ativation']." !";
	$this->load->view('active',$data);
	$this->load->view('footer');
  }
  
   public function delete()
  {
  if(!$this->session->userdata('id'))
	{
	
	redirect('users/login');
	}
	 $this->load->helper('users/user');
	$this->load->view('header');
	$this->load->view('delete');
	$this->load->view('footer');
  }
  
  public function deleteid()
  {
     if(!$this->session->userdata('id'))
	{
	redirect('users/login');
	}
	 $this->load->helper('users/user');
	
	$this->load->model('profile_m','',TRUE);
	$this->profile_m->deleteac($this->session->userdata('id'));
	//$this->session->userdata('id')='';
	//$this->session->unset_userdata('id');
	$this->load->view('header');
	$data['msg']='Account deleted successfully !';
	$this->ion_auth->logout();
		$this->session->set_flashdata('success', lang('user_logged_out'));
		redirect('');
	$this->load->view('delete',$data);
	$this->load->view('footer'); 
  }
  
  public function location()
  {
  if(!$this->session->userdata('id'))
	{
	
	redirect('users/login');
	}
	 $this->load->helper('users/user');
	$this->load->view('header');
	$this->load->model('profile_model','',TRUE);
	$data['query']=$this->profile_model->showprofile($this->session->userdata('id'));
	$this->load->view('location',$data);
	$this->load->view('footer');
  }
  public function editlocation()
  {
  if(!$this->session->userdata('id'))
	{
	
	redirect('users/login');
	}
	 $this->load->helper('users/user');
	$this->load->view('header');
	$this->load->model('profile_m','',TRUE);
	$data['query']=$this->profile_m->editlocation($this->session->userdata('id'));
	$this->load->model('profile_model','',TRUE);
	$data['query']=$this->profile_model->showprofile($this->session->userdata('id'));
	$data['msg']='Location updated successfully !';
	$this->load->view('location',$data);
	$this->load->view('footer');
  }
  
  public function uploadphoto()
  {
  if(!$this->session->userdata('id'))
	{
	
	redirect('users/login');
	}
	$this->load->helper('users/user');
	$this->load->view('header');
	$this->load->model('profile_model','',TRUE);
	$data['query']=$this->profile_model->showprofile($this->session->userdata('id'));
	$this->load->model('profile_m','',TRUE);
	$data['photo']=$this->profile_m->getphoto();
	//$date['total']= count($data['photo']);
	$this->load->view('uploadphoto',$data);
	$this->load->view('footer');
  }
  
  public function photoupload()
  {
  if(!$this->session->userdata('id'))
	{
	
	redirect('users/login');
	}
	 $this->load->helper('users/user');
	$this->load->view('header');
	$this->load->model('profile_model','',TRUE);
	$data['query']=$this->profile_model->showprofile($this->session->userdata('id'));
	$this->load->view('uploadphoto',$data);
	$this->load->view('footer');
  }
	
	 public function horoscope()
  {
  if(!$this->session->userdata('id'))
	{
	
	redirect('users/login');
	}
	 $this->load->helper('users/user');
	$this->load->view('header');
	
	$this->load->view('horoscope');
	$this->load->view('footer');
  }
  
  public function horoscopeup()
  {
  if(!$this->session->userdata('id'))
	{
	
	redirect('users/login');
	}
	  $path="files/horoscope/";
	$config['upload_path'] = "files/horoscope/";
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size']    = '5048'; //2 meg


        $this->load->library('upload');
		 
        $i=0;
        foreach($_FILES as $key => $value)
        {
            if( ! empty($key['name']))
            {
                $this->upload->initialize($config);
        
                if ( ! $photoname=$this->upload->do_upload($key))
                {
                    $errors[] = $this->upload->display_errors();
                    
                }    
                else
                {
                   $arr=$this->upload->data();
				   if($i!=0)
				   {
				   $str.=",".$path.$arr['file_name'];
				   }
				   else
				   {
                    $str=$path.$arr['file_name'];
					$i++;
					}

                }
             }
        
        }
	 $this->load->model('photo_model', '', TRUE);
	 $status= $this->photo_model->horoscopesave($str);
	 $this->load->view('header');
	 $data['msg']='Horoscope uploded sucessfully !!';
	  $this->load->helper('users/user');  
		$this->load->view('horoscope',$data);
		 $this->load->view('footer');
  }
}?>