<?php 
ob_start();
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Register extends CI_Controller {

	
	
	public function index()
	{
	if($this->session->userdata('id'))
	{
	redirect('users/login');
	}
	
	$this->load->helper('url');
	$this->load->model('register_model','',TRUE);
    $data['query']= $this->register_model->getcountry();
	$this->load->helper('users/user');

	$this->load->view('header');
	
	$this->load->view('register',$data);
	$this->load->view('footer');
	}
	public function post()
	{
	

	$this->load->model('register_model','',TRUE);
	
	 $status=$this->register_model->checkmail();
	 //$status='no';
	if($status=='exist')
	{
	
	$data['msg']='Email id already exist';
	$this->load->helper('users/user');
	$this->load->view('header');
	$this->load->model('register_model','',TRUE);
    $data['query']= $this->register_model->getcountry();
	$this->load->view('register',$data);
	$this->load->view('footer');

	}
	else
	{

    if($_POST['membership']==0)
	{
	$id=$this->register_model->save();
   $_SESSION['newuid']=$id;
   $data['nid']=$id;
   $this->load->helper('users/user');
	$this->load->view('header');
	 $this->load->view('register_step2',$data);
	 $this->load->view('footer');
	}
	else
	{
	$id=$this->register_model->save();
   $_SESSION['newuid']=$id; 
     $data['nid']=$id;
   $data['status']='process';
	$this->load->view('paypal',$data);

	//redirect('users/register_step2/index/'.$id, 'refresh');
	}

	
	 }

	}
	
	public function verify($uid)
	{
	$this->load->model('register_model', '', TRUE);
	$val= $liked=$this->register_model->checklink($uid);
	if($val=='exist')
	{
	$this->load->model('register_model', '', TRUE);
	$data['id']= $liked=$this->register_model->verify($uid);
	redirect('users/register/verifyview/success', 'refresh');
	//$this->load->view('verify',$data);
	
	}
	else
	{
		redirect('register_step2/verifyview/mismatch', 'refresh');
	}
	}
	
	public function verifyview($msg)
	{
	 if($msg=='success')
	  {
	   $this->load->helper('users/user');
	$this->load->view('header');
	    $data['msg']= "Thank you for verifying your email address";
		$this->load->view('verify',$data);
		$this->load->view('footer');
	  }
	  else
	  {
	   $data['msg']='Mismatch activation link';
	   $this->load->helper('users/user');
	$this->load->view('header');
	   $this->load->view('verify',$data);
	   $this->load->view('footer');
	  }
	}
	
	public function paypal($status)
	{
	
	$data['status']=$status;
	$this->load->view('paypal',$data);
	}
	public function cancel()
	{
	
	$data['status']="Order was canceled";
	$this->load->helper('users/user');
	$this->load->view('header');
	$this->load->view('ordercancel',$data);
	 $this->load->view('footer');

	}
	public function  payment()
	{
	   $_SESSION['newuid']=$id; 
     $data['nid']=$id;
   $data['status']='process';
	$this->load->view('paypal',$data);
	}
}?>