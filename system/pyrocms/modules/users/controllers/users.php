<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User controller for the users module (frontend)
 *
 * @author 		Phil Sturgeon - PyroCMS Dev Team
 * @package 	PyroCMS
 * @subpackage 	Users module
 * @category	Modules
 */
class Users extends Public_Controller {

	/**
	 * Constructor method
	 *
	 * @access public
	 * @return void
	 */
	function __construct()
	{
		// Call the parent's constructor method
		parent::__construct();

		// Load the required classes
		$this->load->model('users_m');
		
		$this->load->helper('user');
		$this->lang->load('user');
		
		$this->load->library('form_validation');
	}

	/**
	 * Let's login, shall we?
	 * @access public
	 * @return void
	 */
	 
	  
	public function login()
	{
		$this->data->redirect_hash = $this->input->post('redirect_hash');

		// Any idea where we are heading after login?
		if ( ! $_POST AND $args = func_get_args())
		{
			$this->data->redirect_hash = md5($url = implode('/', $args));

			if ( ! (($redirect_to = $this->session->userdata('redirect_to')) && is_array($redirect_to)))
			{
				$redirect_to = array();
			}

			$redirect_to[$this->data->redirect_hash] = $url;

			$this->session->set_userdata('redirect_to', $redirect_to);
		}

		// Get the user data
		$user_data = (object) array(
			'email'		=> $this->input->post('email'),
			'password'	=> $this->input->post('password')
		);

		$validation = array(
			array(
				'field' => 'email',
				'label' => lang('user_email_label'),
				'rules' => 'required|trim|callback__check_login'
			),
			array(
				'field' => 'password',
				'label' => lang('user_password_label'),
				'rules' => 'required|min_length[6]|max_length[20]'
			),
		);

		// Set the validation rules
		$this->form_validation->set_rules($validation);

		// If the validation worked, or the user is already logged in
		if ($this->form_validation->run() or $this->ion_auth->logged_in())
		{
			$this->session->set_flashdata('success', lang('user_logged_in'));


			$redirect_to_uri = 'users/myhome'; // '' Home

			if ($redirect_to = $this->session->userdata('redirect_to'))
			{
				if (array_key_exists($this->data->redirect_hash, $redirect_to))
				{
					$redirect_to_uri = $redirect_to[$this->data->redirect_hash];
				}

				// Unset the redirection
				$this->session->unset_userdata('redirect_to');
			}

			// Call post login hook
			$this->hooks->_call_hook('post_user_login');

			redirect($redirect_to_uri);
		}

		// Render the view
		$this->data->user_data =& $user_data;
		$this->template->build('login', $this->data);
	}

	/**
	 * Method to log the user out of the system
	 * @access public
	 * @return void
	 */
	public function logout()
	{
		$this->ion_auth->logout();
		$this->session->set_flashdata('success', lang('user_logged_out'));
		redirect('');
		//redirect('base_url()/index.php/users/login');
	}

	/**
	 * Method to register a new user
	 * @access public
	 * @return void
	 */
	public function register()
	{
		// Validation rules
		$validation = array(
			array(
				'field' => 'first_name',
				'label' => lang('user_first_name'),
				'rules' => 'required'
			),
			array(
				'field' => 'last_name',
				'label' => lang('user_last_name'),
				'rules' => ($this->settings->require_lastname ? 'required' : '')
			),
			array(
				'field' => 'password',
				'label' => lang('user_password'),
				'rules' => 'required|min_length[6]|max_length[20]'
			),
			array(
				'field' => 'confirm_password',
				'label' => lang('user_confirm_password'),
				'rules' => 'required|matches[password]'
			),
			array(
				'field' => 'email',
				'label' => lang('user_email'),
				'rules' => 'required|valid_email|callback__email_check'
			),
			array(
				'field' => 'confirm_email',
				'label' => lang('user_confirm_email'),
				'rules' => 'required|valid_email|matches[email]'
			),
			array(
				'field' => 'username',
				'label' => lang('user_username'),
				'rules' => 'required|alphanumeric|min_length[3]|max_length[20]|callback__username_check'
			),
			array(
				'field' => 'display_name',
				'label' => lang('user_display_name'),
				'rules' => 'required|alphanumeric|min_length[3]|max_length[50]'
			),
		);

		// Set the validation rules
		$this->form_validation->set_rules($validation);

		$email				= $this->input->post('email');
		$password			= $this->input->post('password');
		$username			= $this->input->post('username');
		$user_data_array	= array(
			'first_name'		=> $this->input->post('first_name'),
			'last_name'			=> $this->input->post('last_name'),
			'display_name'		=> $this->input->post('display_name'),
		);

		// Convert the array to an object
		$user_data						= new stdClass();
		$user_data->first_name 			= $user_data_array['first_name'];
		$user_data->last_name			= $user_data_array['last_name'];
		$user_data->display_name		= $user_data_array['display_name'];
		$user_data->username			= $username;
		$user_data->email				= $email;
		$user_data->password 			= $password;
		$user_data->confirm_email 		= $this->input->post('confirm_email');

		if ($this->form_validation->run())
		{
			// Try to create the user
			if ($id = $this->ion_auth->register($username, $password, $email, $user_data_array))
			{
				$this->session->set_flashdata(array('notice' => $this->ion_auth->messages()));
				redirect('users/activate');
			}

			// Can't create the user, show why
			else
			{
				$this->data->error_string = $this->ion_auth->errors();
			}
		}
		else
		{
			// Return the validation error
			$this->data->error_string = $this->form_validation->error_string();
		}

		$this->data->user_data =& $user_data;
		$this->template->title(lang('user_register_title'));
		$this->template->build('register', $this->data);
	}

	/**
	 * Activate a user
	 * @param int $id The ID of the user
	 * @param str $code The activation code
	 * @return void
	 */
	public function activate($id = 0, $code = NULL)
	{
		// Get info from email
		if ($this->input->post('email'))
		{
			$this->data->activate_user = $this->ion_auth->get_user_by_email($this->input->post('email'));
			$id = $this->data->activate_user->id;
		}

		$code = ($this->input->post('activation_code')) ? $this->input->post('activation_code') : $code;

		// If user has supplied both bits of information
		if ($id AND $code)
		{
			// Try to activate this user
			if ($this->ion_auth->activate($id, $code))
			{
				$this->session->set_flashdata('activated_email', $this->ion_auth->messages());

				// Call post activation hook
				$this->hooks->_call_hook('post_user_activation');

				redirect('users/activated');
			}
			else
			{
				$this->data->error_string = $this->ion_auth->errors();
			}
		}

		$this->template->title($this->lang->line('user_activate_account_title'));
		$this->template->set_breadcrumb($this->lang->line('user_activate_label'), 'users/activate');
		$this->template->build('activate', $this->data);
	}

	/**
	 * Activated page
	 * @access public
	 * @return void
	 */
	public function activated()
	{
		//if they are logged in redirect them to the home page
		if ($this->ion_auth->logged_in())
		{
			redirect(base_url());
		}
		$this->data->activated_email = ($email = $this->session->flashdata('activated_email')) ? $email : '';
		$this->template->title($this->lang->line('user_activated_account_title'));
		$this->template->build('activated', $this->data);
	}

	/**
	 * Reset a user's password
	 * @access public
	 * @return void
	 */
	public function reset_pass($code = FALSE)
	{
		//if user is logged in they don't need to be here. and should use profile options
		if ($this->ion_auth->logged_in())
		{
			$this->session->set_flashdata('error', $this->lang->line('user_already_logged_in'));
			redirect('users/profile');
		}

		if ($this->input->post('btnSubmit'))
		{
			$uname = $this->input->post('user_name');
			$email = $this->input->post('email');

			$user_meta = $this->ion_auth->get_user_by_email($email);

			//supplied username match the email also given?  if yes keep going..
			if ($user_meta && $user_meta->username == $uname)
			{
				$new_password = $this->ion_auth->forgotten_password($email);

				if ($new_password)
				{
					//set success message
					$this->data->success_string = lang('forgot_password_successful');
				}
				else
				{
					// Set an error message explaining the reset failed
					$this->data->error_string = $this->ion_auth->errors();
				}
			}
			else
			{
				//wrong username / email combination
				$this->data->error_string = $this->lang->line('user_forgot_incorrect');
			}
		}

		//code is supplied in url so lets try to reset the password
		if ($code)
		{
			//verify reset_code against code stored in db
			$reset = $this->ion_auth->forgotten_password_complete($code);

			//did the password reset?
			if ($reset)
			{
				redirect('users/reset_complete');
			}
			else
			{
				//nope, set error message
				$this->data->error_string = $this->ion_auth->errors();
			}
		}

		$this->template->title($this->lang->line('user_reset_password_title'));
		$this->template->build('reset_pass', $this->data);
	}

	/**
	 * Password reset is finished
	 * @access public
	 * @param string $code Optional parameter the reset_password_code
	 * @return void
	 */
	public function reset_complete()
	{
		//if user is logged in they don't need to be here. and should use profile options
		if ($this->ion_auth->logged_in())
		{
			$this->session->set_flashdata('error', $this->lang->line('user_already_logged_in'));
			redirect('users/profile');
		}

		//set page title
		$this->template->title($this->lang->line('user_password_reset_title'));

		//build and render the output
		$this->template->build('reset_pass_complete', $this->data);
	}

	/**
	 * Callback method used during login
	 * @access public
	 * @param str $email The Email address
	 * @return bool
	 */
	public function _check_login($email)
	{
		$remember = FALSE;
		if ($this->input->post('remember') == 1)
		{
			$remember = TRUE;
		}

		if ($this->ion_auth->login($email, $this->input->post('password'), $remember))
		{
			return TRUE;
		}

		$this->form_validation->set_message('_check_login', $this->ion_auth->errors());
		return FALSE;
	}

	/**
	 * Username check
	 *
	 * @return bool
	 * @author Ben Edmunds
	 */
	public function _username_check($username)
	{
	    if ($this->ion_auth->username_check($username))
	    {
	        $this->form_validation->set_message('_username_check', $this->lang->line('user_error_username'));
	        return FALSE;
	    }
	    else
	    {
	        return TRUE;
	    }
	}
	public function _email_check($email)
	{
		if ($this->ion_auth->email_check($email))
		{
			$this->form_validation->set_message('_email_check', $this->lang->line('user_error_email'));
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}


public function users_list()
	{
	if($this->session->userdata('id')=="")
	{
	redirect("/users/login");
	}
	else{	
	if(isset($_POST['uid']))
	{
	$this->load->model('membership_model','',TRUE);	
	if(isset($_POST['send']))
	{	
	$data['sent']= $this->membership_model->multiplemails();
	}	
	else{
	$this->membership_model->sendmail();		
	$data['member']= $this->membership_model->showusers();	
	$data['f']=$this->membership_model->showdiv();	
	$data['member2']= $this->membership_model->showname();
	$data['member1']= $this->membership_model->showid();	
	}
	$this->load->helper('users/user');
	$this->load->view('header');
	$this->load->view('users_list',$data);
	$this->load->view('footer'); 	
	}	
	else{
	$this->load->model('membership_model','',TRUE);
	$data['member']= $this->membership_model->showusers();	
	$data['f']=$this->membership_model->hidediv();		
	$data['member2']= $this->membership_model->showname();
	$data['member1']= $this->membership_model->showid();	
	$this->load->helper('users/user');
	$this->load->view('header');
	$this->load->view('users_list',$data);
	$this->load->view('footer'); 
	}	
	}
}	

public function myhome()
{

if($this->session->userdata('id')=="")
	{
	redirect("/users/login");
	}
$this->load->model('users_m');
$data['query']=$this->users_m->myhome_model();	
$data['members']=$this->users_m->getYetView();	
$data['totalvnr']=$this->users_m->viewedModel();	

// PERSONALISED MESSAGE
$data['totalrpm']=$this->users_m->totalmsgreceive();
$data['totalspm']=$this->users_m->totalmsgsent();
$data['totalnewmsg']=$this->users_m->totalnewmsg();
$data['totalreplyleft']=$this->users_m->totalreplyleft();
$data['totalreplied']=$this->users_m->totalreplied();
$data['totaldecline']=$this->users_m->totaldecline();
$data['replyreceived']=$this->users_m->replyreceived();
$data['msgreaded']=$this->users_m->msgreaded();
$data['msgunreaded']=$this->users_m->msgunreaded();
$data['msgdecline']=$this->users_m->msgdecline();
// EXPRESS INTEREST
$data['eetotalrpm']=$this->users_m->eetotalmsgreceive();
$data['eetotalspm']=$this->users_m->eetotalmsgsent();
$data['eetotalnewmsg']=$this->users_m->eetotalnewmsg();
$data['eetotalaccept']=$this->users_m->eetotalaccept();
$data['eetotaldecline']=$this->users_m->eetotaldecline();
$data['eesentinterestaccept']=$this->users_m->eesentinterestaccept();
$data['eependinginterst']=$this->users_m->eependinginterst();
$data['eeinterestdeclined']=$this->users_m->eeinterestdeclined();
//REQUEST
$data['totalrrequest']=$this->users_m->totalrrequest();
$data['totalsrequest']=$this->users_m->totalsrequest();
$data['rphonerequest']=$this->users_m->rphonerequest();
$data['rphotorequest']=$this->users_m->rphotorequest();
$data['rhoroscoperequest']=$this->users_m->rhoroscoperequest();
$data['rreferencerequest']=$this->users_m->rreferencerequest();
$data['sphonerequest']=$this->users_m->sphonerequest();
$data['sphotorequest']=$this->users_m->sphotorequest();
$data['shoroscoperequest']=$this->users_m->shoroscoperequest();
$data['sreferencerequest']=$this->users_m->sreferencerequest();  
//SHORTLIST
$data['shorlist']=$this->users_m->shorlist();  
//LISTS AND VIEWS
$data['memberignored']=$this->users_m->memberignored();  
$data['memberiblocked']=$this->users_m->memberiblocked(); 
$data['memberphoneviewbyme']=$this->users_m->memberphoneviewbyme(); 
$data['memberviewmyphone']=$this->users_m->memberviewmyphone(); 
$data['profileviewed']=$this->users_m->profileviewed(); 




$this->load->model('message_model');
$data['query2']=$this->message_model->memberviewedmyphonenumber();	

$data['query3']=$this->message_model->membersviewedmyprofile();	

$data['query4']=$this->message_model->eesentinterestaccept();	

$data['cht']=$this->message_model->eesentinterestaccept1();	

$data['query5']=$this->users_m->getYetView();	

$data['query6']=$this->users_m->gallery();

if($this->session->userdata('id')) {  
$this->load->view('header');
		$this->load->view('users/myhome',$data);
		$this->load->view('footer');
		}
}
//********************
//**Yet to be viewed
//********************
public function yettoview()
{
$this->load->model('users_m');
$data['query']=$this->users_m->getYetView();	
$data['heading']= "Profiles yet to be viewed ";
	$data['label']="Listed here are profiles that exactly match your Partner Preference.";	
	$this->load->view('header');
		$this->load->view('yettoview',$data);
		$this->load->view('footer');
}
//********************
//Viewed & not contacted
//********************
	public function viewed()
{
	$this->load->model('users_m');
	$data['query']=$this->users_m->viewedModel();	
	$data['heading']= "Profiles viewed & not contacted";
	$data['label']="Listed here are profiles that exactly match your Partner Preference.";	
	$this->load->view('header');
		$this->load->view('yettoview',$data);
		$this->load->view('footer');
}
//********************
//Members looking for me
//********************
	public function memberlookingforme()
{
	$this->load->model('users_m');
	$data['query']=$this->users_m->memLookforMeModel();	
	$data['heading']= "Members looking for me";
	$data['label']="Listed here are profiles whose partner preference matches your profile.";	
	$this->load->view('header');
		$this->load->view('yettoview',$data);
		$this->load->view('footer');
}
//********************
//Mutual Matches
//********************
public function mutualmatch()
{
	$this->load->model('users_m');
	$data['query']=$this->users_m->mutualModel();
	$data['heading']= "Mutual Matches";
	$data['label']="Listed here are profiles whose partner preference matches with your profile and your partner preference matches with their profile.";	
	$this->load->view('header');
		$this->load->view('yettoview',$data);
		$this->load->view('footer');
}
//********************
//REFERENCE
//********************
public function reference()
{ if($this->session->userdata('id')=="")
	{
	redirect("/users/login");
	}
	else{
	 	$this->load->view('header');
		$this->load->view('reference',$data);
		$this->load->view('footer');
		}
}
public function requestreference()
{ if($this->session->userdata('id')=="")
	{
	redirect("/users/login");
	}
	else{
$this->load->model('users_m');
$query= $this->users_m->invitationpost();
if($query==1) { 
$data['message']="You have successfully sent an invite to add reference for you. E-mail will be sent to your referee asking him/her to visit our site and add reference for you.";
} else {
$data['message']="You already sent reference request to this .";
}
	 	$this->load->view('header');
		$this->load->view('reference',$data);
		$this->load->view('footer');
		}
}

public function addnewreference()
{
$this->load->model('users_m');
$query= $this->users_m->addreference();
if($query==1) { 
$data['message']="Your referee details have been successfully added. E-mail will be sent to your referee asking him/her to be prepared to receive e-mails or calls from members who want to know more about you.<br/>
If you want to add the contact details of another referee, click the Add More Referees button.";
} else {
$data['message']="You already referred this user.";
}
	 	$this->load->view('header');
		$this->load->view('reference',$data);
		$this->load->view('footer');
}




}