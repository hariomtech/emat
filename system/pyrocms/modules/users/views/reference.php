<br clear="all"><div id="formarea"  >
<?php if($message!="") {?><br/><p style="color:#C8080E; font-size:11px;"> <?php echo $message;  ?></p><?php }?>

<ul class="tabs" style="width:100%;">
    <li><a href="#tab0">Add Reference</a></li>
	  <li><a href="#tab1">Request References</a></li>
    <li><a href="#tab2">View Reference</a></li>
	
	
</ul>
<div class="tab_container" style="width:100%;">
    <div id="tab0" class="tab_content">
    <!-- Add Referee Details - Start -->
	 <div id="refsub_content_2" style="padding: 0px 15px; ">
        <font class="mediumtxt2">  Please provide the names and contact information of the referees.</font><br>
          <div style="padding-bottom:10px;" class="normalrow">
            <div class="smalltxt">
              <form  onsubmit="return validate(this)" style="margin:0px" action="<?php echo base_url();?>index.php/users/addnewreference" method="POST" name="addRef">
              <input type="hidden" value="profile.keralamatrimony.com" name="domain">
              <input type="hidden" value="yes" name="addRefSubmit">
              <div class="fleft mediumtxt2 bold" style="width:270px;"><span class="errortxt" id="refNamespan"></span><br>Name&nbsp;<font class="clr1">*</font>&nbsp;<br><input type="text" tabindex="1"  alt="NOBLANK~Referee Name~DM~" style="width:190px;" class="inputtext" size="32" name="refName"><br><font class="smalltxt1 clr2">(Referee is the person whom you are<br>recommending for members to contact)</font></div><br clear="all">
              <div style="width:270px; float:left;"  ><span class="errortxt" id="refereeagespan"></span><br>Age&nbsp;<font class="clr1">*</font>&nbsp;<br><input type="text" tabindex="2"  alt="NOBLANK~Referee Age~DM~MAXLENGTH~99~Referee Age~DM~" style="width:40px;" class="inputtext" size="5" maxlength="2" name="refereeage"></div>
             <div class="fleft mediumtxt2 bold" style="width:190px; float:left;"><span class="errortxt" id="refereegenderspan"></span><br>Gender&nbsp;<font class="clr1">*</font>&nbsp;<br>
              <input name="refereegender" type="radio" class="frmelements" tabindex="3"    value="M" checked="checked">
              <font class="smalltxt">Male</font>&nbsp;<input type="radio" class="frmelements" tabindex="4"   value="F" name="refereegender"><font class="smalltxt">Female</font></div><br clear="all">
              <div style="width:270px; float:left;" class="fleft mediumtxt2 bold"><span class="errortxt" id="relationsspan"></span><br>Relationship&nbsp;<font class="clr1">*</font>&nbsp;<br><select tabindex="5"   size="1" name="relations" class="inputtext" style="width:190px;">
               <option value="Relative">Relative</option><option value="Friend">Friend</option><option value="Colleague">Colleague</option><option value="Others">Others</option></select></div>
              <div class="fleft mediumtxt2 bold" style="width:190px; float:left;"><span class="errortxt" id="durationspan"></span><br>Years known to me&nbsp;<font class="clr1">*</font>&nbsp;<br><select tabindex="7"   size="1" name="duration" class="inputtext" style="width:190px;"><option value="Less than 1 year">Less than 1 year</option><option value="1 Year">1 Year</option><option value="2 Years">2 Years</option><option value="3 Years">3 Years</option><option value="4 Years">4 Years</option><option value="5 Years">5 Years</option><option value="6 Years">6 Years</option><option value="7 Years">7 Years</option><option value="8 Years">8 Years</option><option value="9 Years">9 Years</option><option value="More than 10 years">More than 10 years</option></select></div><br clear="all">
              <div style="width:270px; float:left;" class="fleft mediumtxt2 bold"><span class="errortxt" id="refereeemailspan"></span><br>E-mail ID&nbsp;<font class="clr1">*</font>&nbsp;<br><input type="text" style="width:190px;" tabindex="9"  alt="NOBLANK~Referee Email~DM~EMAIL~Referee Email~DM~"  class="inputtext" size="32" name="refereeemail"></div>
              <div class="fleft mediumtxt2 bold" style="width:190px; float:left;"><span class="errortxt" id="refereephonespan"></span><br>Phone Number&nbsp;<br><input type="text" style="width:190px;" tabindex="8" class="inputtext" size="32" name="refereephone" alt="PHONE~Phone Number~DM~"></div><br clear="all">
              <div class="mediumtxt2"><span class="errortxt" id="membercommentspan"></span><br><font class="fleft mediumtxt2 bold">Comments</font>&nbsp;<font class="clr1">*</font>&nbsp;<br><textarea tabindex="6" onblur="ChkEmpty(document.addRef.membercomment, 'text','row3','membercommentspan','Enter your comments');" style="width:460px;wordwrap:true" class="inputtext" cols="20" rows="5" name="membercomment"></textarea>&nbsp;<div style="padding-right:15px;" class="fright"> <a class="smalltxt clr1" href="">View Sample Comment</a></div></div><br clear="all">
              <div style="padding-top:10px;padding-right:15px ;" class="fright"><input type="submit" tabindex="10" value="Submit" class="button pntr"></div><br clear="all">
              </form>
            </div>
          </div>
          <div style="height:1px;" class="vdotline1"></div>
          <div class="smalltxt1 clr2">Note: Kindly avoid adding references of parents, own brothers and sisters.</div>
		   

        </div>
		<!-- Add Referee Details - End -->   <br clear="all">
		 </div>
	 
	 <div id="tab1" class="tab_content">
Reference checks are an integral part of matchmaking. Most people would like to do a reference check on a prospective partner before finalizing their marriage. Swayamvara is the only portal that gives you the opportunity to add references to your profile. These references will help prospective life partners know more about you. You can add as many references as you want.

You can use Reference Privacy Settings to control who can see your referee's contact details.

You can add Reference in two ways:

Invite Reference: You can invite your relatives, friends and colleagues to add a reference for you.

Add Reference: Please provide the names and contact information of your referees. 
<br clear="all"><br clear="all">

  <div id="refsub_content_1" style="padding: 0px 15px; display: block;">
        <font class="mediumtxt2">Send a message to a relative, friend or colleague requesting them to add reference for you.  
		
		</font><br>
          <div style="padding-bottom:10px;" class="normalrow">
            <div class="smalltxt">
              <form  action="<?php echo base_url();?>index.php/users/requestreference" style="margin:0px" method="POST" name="inviteRef"  onsubmit="return validate(this)">
              
              <div id="row1" style="width:600px;">
              <div  style="width:270px;float:left;"><span class="errortxt" id="memberNamespan"></span><br>Your Name&nbsp;<font class="clr1">*</font>&nbsp;<br><input type="text" tabindex="1"  alt="NOBLANK~Your name~DM~" style="width:190px;" class="inputtext" size="32" name="memberName"></div> <div style="width:190px;float:left; position:relative;"  ><span class="errortxt" id="refereenamespan"></span><br>Referee's Name&nbsp;<font class="clr1">*</font>&nbsp;<br>
                <input type="text" style="width:190px;" tabindex="2" alt="NOBLANK~Your name~DM~" class="inputtext" size="32" name="refereename" />
                <br>
                <font class="smalltxt1 clr2">(Referee is the person who is going to <br>add a reference for you)</font></div></div><br clear="all">
              <div id="row2">
              <div   style="width:270px;float:left;"><span class="errortxt" id="relationshipspan"></span><br>Relationship with Referee&nbsp;<font class="clr1">*</font>&nbsp;<br><select tabindex="3"     size="1" name="relationship" class="inputtext" style="width:190px;">
              <option value="Relative">Relative</option><option value="Friend">Friend</option><option value="Colleague">Colleague</option><option value="Others">Others</option></select>
              </div>
              <div class="fleft mediumtxt2 bold"><span class="errortxt" id="refereeemailidspan"></span><br>Referee's E-mail ID&nbsp;<font class="clr1">*</font>&nbsp;<br><input type="text" style="width:190px;" tabindex="4"   alt="NOBLANK~Email~DM~EMAIL~Email~DM~" class="inputtext" size="32" name="refereeemailid"></div></div><br clear="all">
              <div id="row3">
              <div class="fleft mediumtxt2 bold" style="width:470px;"><span class="errortxt" id="membercommentsspan"></span><br>Message to the Referee&nbsp;<font class="clr1">*</font>&nbsp;<br><textarea tabindex="5" alt="NOBLANK~Message to the referee~DM~"   class="inputtext" style="width:470px" cols="50" rows="5" name="membercomments">I'm looking for an alliance in Swayamvara. It would be great if you could spare a few minutes and add reference for me. Your reference will help in my partner search.</textarea></div>
              </div><br clear="all">
              <div style="padding-top:10px;padding-right:5px !important;padding-right:5px;" class="fright"><input type="submit" tabindex="10" value="Send Invite" class="button pntr"></div><br clear="all">
              </form>
            </div>
          </div>
          <div style="height:1px;" class="vdotline1"></div>
          <div class="smalltxt1 clr2">Note: Kindly avoid adding references of parents, own brothers and sisters.</div>
     
         </div>

    
  
 
    
    
	</div>
	
	  <div id="tab2" class="tab_content">sdfsdfdf
	  </div>
	</div>
	<br clear="all">
	    </div>