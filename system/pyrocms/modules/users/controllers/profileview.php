<?php 
ob_start();
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profileview extends CI_Controller {
public function index()
{
	if(!$this->session->userdata('id'))
	{
	$this->load->helper('users/user');
	$this->load->view('header');
	$this->load->view('login');
	$this->load->view('footer');	
	}
	else
	{
	$this->session->userdata('id');
	$this->load->model('profile_model','',TRUE);
	$data['snap']= $this->profile_model->getmysnap();	
	$data['interest']= $this->profile_model->getmyinterest();		
	$data['preference']= $this->profile_model->getmypreference();
//	$data['query']= $this->profile_model->getprofile($this->session->userdata('id'));	
	$data['query']= $this->profile_model->getprofile($this->session->userdata('id'));
	$data['member2']= $this->profile_model->showname();
	$data['member1']= $this->profile_model->showid();	
	$this->load->helper('users/user');
	$this->load->view('header');
	$this->load->view('profileview',$data);
	$this->load->view('footer');
	}
	}
	
public function printprofile()
{	
	$this->load->model('profile_model','',TRUE);
	$data['query']= $this->profile_model->getprofile($this->session->userdata('id'));
	$this->load->helper('users/user');
	$this->load->view('printprofile',$data);
}
	
	public function edit()
	{
	
	$this->load->model('profile_model','',TRUE);
	//$data['query']= $this->profile_model->getprofile($this->session->userdata('id'));
	$data['query']= $this->profile_model->getprofile();
	$this->load->helper('users/user');
	$this->load->view('header');
	$this->load->view('editprofile',$data);
	$this->load->view('footer'); 
	}
	
	public function editprofile()
	{echo "Edit Profile";
	if(!$this->session->userdata('id'))
	{
	$this->load->helper('users/user');
	$this->load->view('header');
	$this->load->view('login');
	$this->load->view('footer');
	
	}
	else
	{
	
	$this->load->model('profile_model','',TRUE);
	$this->profile_model->editprofile($this->session->userdata('id'));
	$this->load->helper('users/user');
	$this->load->view('header');
	$data['msg']='Profile successfully updated !!';
	$this->load->view('profileview',$data);
	$this->load->view('footer');
	}
	}
	
	public function deleteprofile()
	{
	if(!$this->session->userdata('id'))
	{
	$this->load->helper('users/user');
	$this->load->view('header');
	$this->load->view('login');
	$this->load->view('footer');
	
	}
	else
	{
	
	$this->load->model('profile_model','',TRUE);
	$this->profile_model->deleteprofile();

	redirect('users/profileview/delete');
	}
	}
	
	public function delete()
	{
	$this->load->view('header');
	$data['msg']='Profile successfully deleted !!';
	$this->load->view('profiledelete',$data);
	$this->load->view('footer');
	}
	
}?>