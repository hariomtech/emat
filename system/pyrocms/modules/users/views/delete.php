

<link href="<?php echo css_path('global-style-wn1.css'); ?>" type="text/css" rel="stylesheet">
<style type="text/css">
.button {
-moz-background-clip:border;
-moz-background-inline-policy:continuous;
-moz-background-origin:padding;
background:#71A55A none repeat scroll 0 0;
border:medium none;
color:#FFFFFF;
font-family:arial,helvetica,sans-serif;
font-size:11px;
font-size-adjust:none;
font-stretch:normal;
font-style:normal;
font-variant:normal;
font-weight:bold;
line-height:normal;
overflow:visible;
padding:1px 10px;
vertical-align:middle;
}
</style>

<div class="fleft" style="padding-left: 20px; height:480px; width:97%" id="formarea">
<form action="<?php echo base_url();?>index.php/users/edittag/deleteid" method="post">
<?php
if($msg)
{
?>
<div>
<font color="#FF0000">
<?php echo $msg; ?>
</font>
</div>
<?php }?>
<div>
We hope you found your life partner and this is the reason you decided to delete your profile. <font class="mediumtext boldtxt">Please note that profiles once deleted cannot be restored.</font></div><br clear="all">
		<span id="delreason" class="errortxt"></span>
		<div class="boldtxt"><label for="reason">Select the reason for deleting your profile</label></div>
		<div id="delprofilereason">
			<select id="reason" name="reason" class="inputtext" onblur="ChkEmpty(REASONCAT, 'select','row1','delreason','Please select the reason for deleting your profile.');" onchange="ChkEmpty(REASONCAT, 'select','row1','delreason','Please select the reason for deleting your profile.');switchotherreason();" tabindex="15">
			<option value="" selected="selected">--Select Reason--</option>

					<option value="Marriage fixed through SwayamvaraMatrimony">Marriage fixed through SwayamvaraMatrimony</option>
					<option value="Marriage fixed through another Matrimony Portal">Marriage fixed through another Matrimony Portal</option>
					<option value="Marriage fixed through other sources">Marriage fixed through other sources</option>
					<option value="Prefer to search later">Prefer to search later</option>
					<option value="Not getting responses as expected">Not getting responses as expected</option>
					<option value="Other reasons">Other reasons</option>

				</select>
		</div>
		<div id="othersitename" style="display: none; padding-top: 10px;">
			<div class="boldtxt"><label for="reason">Please specify the site name</label></div>
			<div><input id="othersite" name="othersite" value="" size="46" class="inputtext" type="text"></div>
		</div>
			<div id="deactive" style="display: none; padding-top: 10px;">
			<div class="boldtxt"><label for="reason">Specify the days/months to deactivate your profile.</label></div>		
			<div class="fleft"></div>
			 <div class="fleft" style="padding-left: 10px;"><span id="delreason1" class="errortxt">Please select the reason for deleting your profile.</span></div><div style="clear: both;"></div>
			 <div class="smalltxt1">Note: After selected days/month your profile gets activated automatically with a intimation mailer.</div>
		</div>

		<div>
			<div id="comments" style="display: block; padding-top: 10px;">
				<span id="delcomments" class="errortxt"></span>
				<div class="boldtxt"><label for="comments1">Add your comments </label>
				</div></div>
				<textarea name="comment" id="comment" cols="90" rows="6"></textarea><br/>
				<input type="submit" name="submit" value="Delete" class="button" onClick="return validate()"/>
				<script type="text/javascript">
				function validate()
				{
				 if(document.getElementById('reason').value=='')
				  {
				  alert('Please select the reason for deleting your profile ! ');
				  return false;
				  }
				 
				}
				</script>
				
				</div>
</form>
</div>

