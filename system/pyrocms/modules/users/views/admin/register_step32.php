<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />


  <title><?php //echo $title; ?></title>

   <link href="<?php echo base_url(); ?>css/reg-v141.css" type="text/css" rel="stylesheet">
   
<script type="text/javascript" src="<?php echo base_url(); ?>scripts/jquery-1.4.2.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>scripts/registration-v9.js"></script>


 <script type="text/javascript">
 function validateform()
 {


 if(document.getElementById('email').value=="" || document.getElementById('password1').value=="" || document.getElementById('first_name').value=="" || document.getElementById('last_name').value=="" || document.getElementById('postedby').value=="" || document.getElementById('gender-Male').value=="" || document.getElementById('gender-Female').value=="" || document.getElementById('day').value=="" || document.getElementById('month').value=="" || document.getElementById('year').value=="" || document.getElementById('year').value=="" || document.getElementById('community').value=="" || document.getElementById('mother_tongue').value=="" || document.getElementById('countryofresidence').value=="")
 {
 scroll(900,50);
 document.getElementById('error').style.display='inline';
 return false;
 }



 
 }
 </script>

</head>

<body>

   <div id="logo_wrap">
            <div class="logo">
                        <b><a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>images/logo1.png" alt="Swayamvaraonline.com" title="Swayamvaraonline.com"></a></b>
                        <span>World's Largest Matrimonial Service</span>
                    </div><div class="help">

			<div class="left"></div>
			<div class="middle">
            <span class="call"><a href="#">Call </a> <a href="#"><img src="<?php echo base_url(); ?>images/blue-arrow.gif" width="9" height="9"></a> <a href="#" style="margin-left: 2px;">Local Nos</a></span><span id="arrow2"><a href="#"><img src="<?php echo base_url(); ?>images/blue-arrow.gif" style="margin-top: 8px;" width="9" height="9"></a></span>
				<br clear="all"><span class="live_help" style="font-size:12px;"><a href="#" >Other 24x7 Help Options</a></span>			
			<br clear="all">
				<span id="livehelp" style="display:none">
				<div style="position:relative;width:180px;z-index:10000"></div>
			  </span>


			</div>
			<div class="right"></div><br clear="all">

			</div>        </div> <br clear="all">

		<!--<![if gt IE 6]><div class="spacer_5"></div><![endif]>-->
		<!--[if !IE]--><div class="spacer_2"></div><!--[endif]-->

        
<div id="formarea">
<div class="heading">
    <span>Account Information</span><div>( * Mandatory Fields)</div>
    <div style="float:right;margin-right:18px;_margin-right:10px;font:normal 14px arial;color:#fff">Page 1 of 2</div>
</div>
<div class="bg">
<form method="post" action="<?php echo base_url(); ?>index.php/register_step2/post" name="frm_profile" id="frm_profile" autocomplete="off" style="margin:0px">

            


            <!-- PROFILE CREATED FOR -->

			
            <div style="padding:5px"></div>

            <!-- Marital Status / HEIGHT -->
            <div class="form">
                <div class="wrapper" style="padding:5px 15px 10px 15px">
				<?php
				if(isset($msg))
				{
				?>
				<div>
				<font color="#FF0000">
				<?php echo $msg; ?>
				</font>
				</div>
				<?php }?>

                    <div style="padding:5px"></div>

                     <!-- Marital Status -->
                    <label class="label-180" for="maritalstatus">Marital Status <span class="mandatory">*</span></label>
					<div id="maritalstatus_div" class="select_error">
						
<select name="maritalstatus" id="maritalstatus" class="select1" style="margin-left:0px" onchange="show_hidechildren_div(this.value);" onfocus="$(&quot;#errmsg_maritalstatus&quot;).hide();toggleHint_new(&quot;show&quot;, this.name, document.getElementById(&quot;maritalstatus&quot;),document.getElementById(&quot;maritalstatus&quot;).offsetWidth)" onblur="validate_maritalstatus();">
    <option value="" label="Select" selected="selected">Select</option>
    <option value="Never Married" label="Never Married">Never Married</option>
    <option value="Divorced" label="Divorced">Divorced</option>
    <option value="Widowed" label="Widowed">Widowed</option>
    <option value="Separated" label="Separated">Separated</option>
    <option value="Annulled" label="Annulled">Annulled</option>
</select>	
<input type="hidden" name="id" value="<?php if(isset($id)) { echo $id; } ?>"/>
				</div>
                    <div id="errmsg_maritalstatus" class="error" style="display:none"></div><br clear="all" style="line-height:0px">
					
            <!-- HINT STARTS HERE -->
            <span class="hint" id="hint_maritalstatus" style="display:none;">
            <div>
                <div class="top_hint"></div><div class="middle_hint">Please select marital status of the person<br> looking to get married.<br><br></div><div class="bottom_hint"></div>
            </div>
            <div class="arrow_hint"></div>
            </span>
            <!-- HINT ENDS HERE -->
        					<br clear="all" style="line-height:0px">
					<div id="brk_ms" style="display:none"></div>
                    <!-- Marital Status END -->

                    <div style="padding:5px"></div>

                    <!-- HAVE CHILDREN -->
                    <div id="children_div" style="display: none; ">
                        <label class="label-180" for="no_of_kids">Have Children <span class="mandatory">*</span></label>
                        <div id="have_child_div" class="select_error">
                           
<select name="children" id="children" class="select1" style="margin-left:0px" onchange="reset_havechildren();validate_children();" onfocus="$(&quot;#errmsg_havechildren&quot;).hide();" onblur="validate_children();">
    <option value="" label="Select" selected="selected">Select</option>
    <option value="Yes. Living together" label="Yes. Living together">Yes. Living together</option>
    <option value="No" label="No">No</option>
    <option value="Yes. Not living together" label="Yes. Not living together">Yes. Not living together</option>
</select>                        </div>
                        <div id="errmsg_havechildren" class="error" style="display:none"></div><br>
                        <span id="no_of_kids_span" style="margin-top: 10px; display: none; ">
                            <div id="no_of_kids_div" class="select_error" style="width:100px;float:left;margin:0px 0px 0px 190px;_margin:0px 0px 0px 95px">
                                
<select name="no_of_kids" id="no_of_kids" class="country-code" style="margin-left:0px">
    <option value="0" label="No.of Kids">No.of Kids</option>
    <option value="1" label="1">1</option>
    <option value="2" label="2">2</option>
    <option value="3" label="3">3</option>
    <option value="More than 3" label="More than 3">More than 3</option>
</select>                            </div><b style="display:block;float:left;font:normal 12px arial;color:#FE6C00;margin-left:25px;padding-top:5px">(Optional)</b><br clear="all">
                            <div id="errmsg_no_of_kids" class="error" style="display:none;margin-top:5px"></div>
                        </span><div style="padding:5px"></div>
                    </div>
                    <!-- HAVE CHILDREN END -->



                    <!-- HEIGHT -->
                    <label class="label-180" for="height">Height <span class="mandatory">*</span></label>
                    <div id="height_div" class="select_error">
                    
<select name="height" id="height" class="select1" style="margin-left:0px;" onfocus="$(&quot;#errmsg_height&quot;).hide();toggleHint_new(&quot;show&quot;, this.name, document.getElementById(&quot;height&quot;), document.getElementById(&quot;height&quot;).offsetWidth)" onblur="validate_height();">
    <option value="" label="Select" selected="selected">Select</option>
    <option value="53" label="4ft 5in - 134cm">4ft 5in - 134cm</option>
    <option value="54" label="4ft 6in - 137cm">4ft 6in - 137cm</option>
    <option value="55" label="4ft 7in - 139cm">4ft 7in - 139cm</option>
    <option value="56" label="4ft 8in - 142cm">4ft 8in - 142cm</option>
    <option value="57" label="4ft 9in - 144cm">4ft 9in - 144cm</option>
    <option value="58" label="4ft 10in - 147cm">4ft 10in - 147cm</option>
    <option value="59" label="4ft 11in - 149cm">4ft 11in - 149cm</option>
    <option value="60" label="5ft - 152cm">5ft - 152cm</option>
    <option value="61" label="5ft 1in - 154cm">5ft 1in - 154cm</option>
    <option value="62" label="5ft 2in - 157cm">5ft 2in - 157cm</option>
    <option value="63" label="5ft 3in - 160cm">5ft 3in - 160cm</option>
    <option value="64" label="5ft 4in - 162cm">5ft 4in - 162cm</option>
    <option value="65" label="5ft 5in - 165cm">5ft 5in - 165cm</option>
    <option value="66" label="5ft 6in - 167cm">5ft 6in - 167cm</option>
    <option value="67" label="5ft 7in - 170cm">5ft 7in - 170cm</option>
    <option value="68" label="5ft 8in - 172cm">5ft 8in - 172cm</option>
    <option value="69" label="5ft 9in - 175cm">5ft 9in - 175cm</option>
    <option value="70" label="5ft 10in - 177cm">5ft 10in - 177cm</option>
    <option value="71" label="5ft 11in - 180cm">5ft 11in - 180cm</option>
    <option value="72" label="6ft - 182cm">6ft - 182cm</option>
    <option value="73" label="6ft 1in - 185cm">6ft 1in - 185cm</option>
    <option value="74" label="6ft 2in - 187cm">6ft 2in - 187cm</option>
    <option value="75" label="6ft 3in - 190cm">6ft 3in - 190cm</option>
    <option value="76" label="6ft 4in - 193cm">6ft 4in - 193cm</option>
    <option value="77" label="6ft 5in - 195cm">6ft 5in - 195cm</option>
    <option value="78" label="6ft 6in - 198cm">6ft 6in - 198cm</option>
    <option value="79" label="6ft 7in - 200cm">6ft 7in - 200cm</option>
    <option value="80" label="6ft 8in - 203cm">6ft 8in - 203cm</option>
    <option value="81" label="6ft 9in - 205cm">6ft 9in - 205cm</option>
    <option value="82" label="6ft 10in - 208cm">6ft 10in - 208cm</option>
    <option value="83" label="6ft 11in - 210cm">6ft 11in - 210cm</option>
    <option value="84" label="7ft - 213cm">7ft - 213cm</option>
</select>                    </div>
                    <div id="errmsg_height" class="error" style="display:none;"></div><br>
                    
            <!-- HINT STARTS HERE -->
            <span class="hint" id="hint_height" style="display:none;">
            <div>
                <div class="top_hint"></div><div class="middle_hint">Please select height of the person looking<br> to get married.<br><br></div><div class="bottom_hint"></div>
            </div>
            <div class="arrow_hint"></div>
            </span>
            <!-- HINT ENDS HERE -->
                            <!-- HEIGHT END -->

					 <div style="padding:5px"></div>

                    <!-- CASTE -->
                    <div id="show_caste">
                        <label class="label-180" for="caste">Caste / Sect <span class="mandatory">*</span></label>
                        <div id="caste_div">
                            
<select name="caste" id="caste" class="select1" style="margin-left:0px;" onfocus="toggleHint_new(&quot;show&quot;, this.name, document.getElementById(&quot;caste&quot;),document.getElementById(&quot;caste&quot;).offsetWidth)" onchange="show_hide_subcaste(this.value);" onblur="toggleHint(&#39;hide&#39;, &#39;caste&#39;);">
    <option value="" label="Don&#39;t Know" selected="selected">Don't Know</option>
    <option value="6000 Niyogi" label="6000 Niyogi">6000 Niyogi</option>
    
    <option value="Other" label="Other">Other</option>
</select>                        </div>
                    </div>
                    
            <!-- HINT STARTS HERE -->
            <span class="hint" id="hint_caste" style="display:none;">
            <div>
                <div class="top_hint"></div><div class="middle_hint">Please select caste of the person looking to get married.<br><br></div><div class="bottom_hint"></div>
            </div>
            <div class="arrow_hint"></div>
            </span>
            <!-- HINT ENDS HERE -->
                            <!-- CASTE END -->

					<div style="padding:5px"></div>

                    <div id="subcaste_optional" style="display:block">
                    <label class="label-180" for="subcaste">Sub-Caste</label>
                    
<input type="text" name="subcaste" id="subcaste" value="" class="input1 ac_input" style="margin-left:0px;" autocomplete="off"><br>
                    </div>

                    

                </div>
            </div>
            <script language="JavaScript">
            <!--
                var marital_status_obj = document.forms["frm_profile"].maritalstatus;
                var marital_status_chosen = "";
                for(var j=0; j < marital_status_obj.length; j++)
                {
                    if(marital_status_obj[j].checked == true)
                    {
                        marital_status_chosen = marital_status_obj[j].value;
                    }
                }
                show_hidechildren_div(marital_status_chosen);
                reset_havechildren();
            //-->
            </script>
            <!-- Marital Status / HEIGHT END -->

            <div style="padding:1px"></div>

            <!-- Lifestyle -->
            <div class="form">
                <div class="wrapper">

                    <label class="label-180" for="diet">Diet</label>
                    <span class="fl">
<label for="diet-Veg"><input type="radio" name="diet" id="diet-Veg" value="Veg" class="radio" onclick="$(&quot;#errmsg_diet&quot;).html(&quot;&quot;).hide();">Veg</label> <label for="diet-NonVeg"><input type="radio" name="diet" id="diet-NonVeg" value="Non-Veg" class="radio" onclick="$(&quot;#errmsg_diet&quot;).html(&quot;&quot;).hide();">Non-Veg</label> <label for="diet-OccasionallyNonVeg"><input type="radio" name="diet" id="diet-OccasionallyNonVeg" value="Occasionally Non-Veg" class="radio" onclick="$(&quot;#errmsg_diet&quot;).html(&quot;&quot;).hide();">Occasionally Non-Veg</label> <label for="diet-Eggetarian"><input type="radio" name="diet" id="diet-Eggetarian" value="Eggetarian" class="radio" onclick="$(&quot;#errmsg_diet&quot;).html(&quot;&quot;).hide();">Eggetarian</label> <label for="diet-Jain"><input type="radio" name="diet" id="diet-Jain" value="Jain" class="radio" onclick="$(&quot;#errmsg_diet&quot;).html(&quot;&quot;).hide();">Jain</label> <label for="diet-Vegan"><input type="radio" name="diet" id="diet-Vegan" value="Vegan" class="radio" onclick="$(&quot;#errmsg_diet&quot;).html(&quot;&quot;).hide();">Vegan</label></span>
                    <a href="javascript:;" class="tooltip_icon tt rightEnd" style="margin:7px 0px;_margin:0px 0px;">
                        <span class="tooltip">
                            <span class="top"></span>
                            <span class="middle" style="color:#000">
                                Vegan diet excludes all meat, fish and dairy products as well as any food derived from a living animal such as eggs.
                                <div style="padding:5px"></div>
                                A Vegan person is a strict vegetarian, who eats only foods of plant origin.
                            </span>
                            <span class="bottom"></span>
                        </span>
                    </a>
                    <div id="errmsg_diet" class="error" style="display:none"></div><br>

                    <div style="padding:5px"></div>

                    <label class="label-180" for="smoke">Smoke <span class="mandatory">*</span></label>
                        <span class="fl">
<label for="smoke-No"><input type="radio" name="smoke" id="smoke-No" value="No" class="radio" onclick="$(&quot;#errmsg_smoke&quot;).html(&quot;&quot;).hide();">No</label> <label for="smoke-Occasionally"><input type="radio" name="smoke" id="smoke-Occasionally" value="Occasionally" class="radio" onclick="$(&quot;#errmsg_smoke&quot;).html(&quot;&quot;).hide();">Occasionally</label> <label for="smoke-Yes"><input type="radio" name="smoke" id="smoke-Yes" value="Yes" class="radio" onclick="$(&quot;#errmsg_smoke&quot;).html(&quot;&quot;).hide();">Yes</label></span>
                    <div id="errmsg_smoke" class="error" style="display:none"></div><br>

                    <div style="padding:5px"></div>

                    <label class="label-180" for="drink">Drink <span class="mandatory">*</span></label>
                        <span class="fl">
<label for="drink-No"><input type="radio" name="drink" id="drink-No" value="No" class="radio" onclick="$(&quot;#errmsg_drink&quot;).html(&quot;&quot;).hide();">No</label> <label for="drink-Occasionally"><input type="radio" name="drink" id="drink-Occasionally" value="Occasionally" class="radio" onclick="$(&quot;#errmsg_drink&quot;).html(&quot;&quot;).hide();">Occasionally</label> <label for="drink-Yes"><input type="radio" name="drink" id="drink-Yes" value="Yes" class="radio" onclick="$(&quot;#errmsg_drink&quot;).html(&quot;&quot;).hide();">Yes</label></span>
                    <div id="errmsg_drink" class="error" style="display:none"></div><br>

                    <div style="padding:5px"></div>

                    <label class="label-180" for="personal_values">Personal Values</label>
                    <span class="fl">
<label for="personal_values-Traditional"><input type="radio" name="personal_values" id="personal_values-Traditional" value="Traditional" class="radio" onclick="$(&quot;#errmsg_personal_values&quot;).html(&quot;&quot;).hide();">Traditional</label> <label for="personal_values-Moderate"><input type="radio" name="personal_values" id="personal_values-Moderate" value="Moderate" class="radio" onclick="$(&quot;#errmsg_personal_values&quot;).html(&quot;&quot;).hide();">Moderate</label> <label for="personal_values-Liberal"><input type="radio" name="personal_values" id="personal_values-Liberal" value="Liberal" class="radio" onclick="$(&quot;#errmsg_personal_values&quot;).html(&quot;&quot;).hide();">Liberal</label></span>
                    <div id="errmsg_personal_values" class="error" style="display:none;width:170px;"></div> <br>

                </div>
            </div>
            <!-- Lifestyle END -->

            <div style="padding:1px"></div>

            <!-- Complexion -->
            <div class="form">
                <div class="wrapper">

                    <label class="label-180" for="complexion">Complexion</label>
                    <span class="fl">
<label for="complexion-VeryFair"><input type="radio" name="complexion" id="complexion-VeryFair" value="Very Fair" class="radio" onclick="$(&quot;#errmsg_complexion&quot;).html(&quot;&quot;).hide();">Very Fair</label> <label for="complexion-Fair"><input type="radio" name="complexion" id="complexion-Fair" value="Fair" class="radio" onclick="$(&quot;#errmsg_complexion&quot;).html(&quot;&quot;).hide();">Fair</label> <label for="complexion-Wheatish"><input type="radio" name="complexion" id="complexion-Wheatish" value="Wheatish" class="radio" onclick="$(&quot;#errmsg_complexion&quot;).html(&quot;&quot;).hide();">Wheatish</label> <label for="complexion-Dark"><input type="radio" name="complexion" id="complexion-Dark" value="Dark" class="radio" onclick="$(&quot;#errmsg_complexion&quot;).html(&quot;&quot;).hide();">Dark</label></span>
                    <div id="errmsg_complexion" class="error" style="display:none;width:170px;"></div><br>

                    <div style="padding:5px"></div>

                    <label class="label-180" for="bodytype">Body Type</label>
                    <span class="fl">
<label for="bodytype-Slim"><input type="radio" name="bodytype" id="bodytype-Slim" value="Slim" class="radio" onclick="$(&quot;#errmsg_bodytype&quot;).html(&quot;&quot;).hide();">Slim</label> <label for="bodytype-Athletic"><input type="radio" name="bodytype" id="bodytype-Athletic" value="Athletic" class="radio" onclick="$(&quot;#errmsg_bodytype&quot;).html(&quot;&quot;).hide();">Athletic</label> <label for="bodytype-Average"><input type="radio" name="bodytype" id="bodytype-Average" value="Average" class="radio" onclick="$(&quot;#errmsg_bodytype&quot;).html(&quot;&quot;).hide();">Average</label> <label for="bodytype-Heavy"><input type="radio" name="bodytype" id="bodytype-Heavy" value="Heavy" class="radio" onclick="$(&quot;#errmsg_bodytype&quot;).html(&quot;&quot;).hide();">Heavy</label></span>
                    <div id="errmsg_bodytype" class="error" style="display:none;width:170px;"></div><br>

                    <div style="padding:5px"></div>

                    <label class="label-180" for="specialcases"><span style="float:left">Special Cases <span class="mandatory">*</span></span><a href="javascript:;" class="tooltip_icon tt rightEnd"><span class="tooltip" style="right:-41px"><span class="top"></span><span class="middle" style="color:#000">'Special Cases' is a category created to refer to members who are physically or mentally challenged by birth or due to accident, or have physical abnormalities affecting looks and bodily functions.<div style="padding:5px"></div>The 'Special Cases' option also includes HIV positive members.</span><span class="bottom"></span></span></a></label>
                    <div id="specialcases_div" class="select_error">
                        
<select name="specialcases" id="specialcases" class="select1" style="margin-left:0px;width:286px" onfocus="$(&quot;#errmsg_specialcases&quot;).hide();toggleHint_new(&quot;show&quot;, this.name, document.getElementById(&quot;specialcases&quot;),document.getElementById(&quot;specialcases&quot;).offsetWidth)" onblur="validate_specialcases();">
    <option value="None" label="None">None</option>
    <option value="Physically challenged from birth" label="Physically challenged from birth">Physically challenged from birth</option>
    <option value="Physically challenged due to accident" label="Physically challenged due to accident">Physically challenged due to accident</option>
    <option value="Mentally challenged from birth" label="Mentally challenged from birth">Mentally challenged from birth</option>
    <option value="Mentally challenged due to accident" label="Mentally challenged due to accident">Mentally challenged due to accident</option>
    <option value="Physical abnormality affecting only looks" label="Physical abnormality affecting only looks">Physical abnormality affecting only looks</option>
    <option value="Physical abnormality affecting bodily functions" label="Physical abnormality affecting bodily functions">Physical abnormality affecting bodily functions</option>
    <option value="Physically and mentally challenged" label="Physically and mentally challenged">Physically and mentally challenged</option>
    <option value="HIV positive" label="HIV positive">HIV positive</option>
</select>                    </div>
                    <div id="errmsg_specialcases" class="error" style="display:none"></div><br>
					
            <!-- HINT STARTS HERE -->
            <span class="hint" id="hint_specialcases" style="display:none;">
            <div>
                <div class="top_hint"></div><div class="middle_hint">'Special cases' applies to members who are<br> physically or mentally challenged.<br><br></div><div class="bottom_hint"></div>
            </div>
            <div class="arrow_hint"></div>
            </span>
            <!-- HINT ENDS HERE -->
        
                    <div style="padding:5px"></div>

                </div>
            </div>
            <!-- Complexion END -->
						<div style="padding:1px"></div>

			<!-- RESIDENCE -->

              <div class="form">
                <div class="wrapper">

                    <label class="label-180" for="stateofresidence">State of Residence <span class="mandatory">*</span></label>
                    <div id="state_div" class="select_error">
                        
<select name="stateofresidence" id="stateofresidence" class="select1" style="margin-left:0px;" onchange="display_dropdown(&quot;India&quot;, this.value,&quot;&quot;,document.getElementById(&quot;nearest_city&quot;),&quot;http://img.shaadi.com&quot;,&quot;modify_profile&quot;,&quot;Y&quot;,&quot;state_selected&quot;);" onfocus="$(&quot;#errmsg_state&quot;).hide();toggleHint_new(&quot;show&quot;, this.name, document.getElementById(&quot;stateofresidence&quot;), document.getElementById(&quot;stateofresidence&quot;).offsetWidth)" onblur="validate_stateofresidence();">
    
<option value="">Select state</option><option value="Andaman &amp; Nicobar">Andaman &amp; Nicobar</option><option value="West Bengal">West Bengal</option><option value="-">Other</option></select>                    </div>
                     <div style="margin-top:2px; padding-left:50px;display:none" id="loading_city"></div>
                    <div style="padding-left:50px;display:none" id="loading_state"></div>
                    <div id="errmsg_state" class="error" style="display:none"></div><br>
                    
            <!-- HINT STARTS HERE -->
            <span class="hint" id="hint_stateofresidence" style="display:none;">
            <div>
                <div class="top_hint"></div><div class="middle_hint">Please select state of current residence of the person looking to get married.<br><br></div><div class="bottom_hint"></div>
            </div>
            <div class="arrow_hint"></div>
            </span>
            <!-- HINT ENDS HERE -->
        
                    <div style="padding:5px"></div>

                    <label class="label-180" for="nearest_city">City of Residence <span class="mandatory">*</span></label>
                    <div id="city_div" class="select_error">
                        
<select name="nearest_city" id="nearest_city" class="select1" style="margin-left:0px;" onfocus="$(&quot;#errmsg_city&quot;).hide();toggleHint_new(&quot;show&quot;, this.name, document.getElementById(&quot;nearest_city&quot;), document.getElementById(&quot;nearest_city&quot;).offsetWidth)" onblur="validate_nearest_city();">
    
<option value="">Select city</option>
<option >bhopal</option></select>                      </div>
                    <div id="errmsg_city" class="error" style="display:none"></div><br>
					
            <!-- HINT STARTS HERE -->
            <span class="hint" id="hint_nearest_city" style="display:none;">
            <div>
                <div class="top_hint"></div><div class="middle_hint">Please select city of residence of the person looking to get married.<br><br></div><div class="bottom_hint"></div>
            </div>
            <div class="arrow_hint"></div>
            </span>
            <!-- HINT ENDS HERE -->
        
                    <a id="externalhit" onclick="onloadstatedisplay();" style="display:none;"></a>
                    <script language="JavaScript">
                        function onloadstatedisplay(){
                                                        display_dropdown("India", "","",document.getElementById("nearest_city"),"http://img.shaadi.com","modify_profile","Y","state_selected");
                        }
                        display_state_dropdown("India", "", "", document.frm_profile.stateofresidence, "http://img.shaadi.com", "modify_profile", "");
                    </script>

               
<input type="hidden" name="residencystatus" value="-" id="residencystatus">                </div>
            </div>
            <!-- RESIDENCE END -->

            <div style="padding:1px"></div>

            <!-- PRIVACY OPTIONS -->
            <div class="form">
                 <div class="wrapper" style="padding:5px 15px 10px 15px;position:relative">



					  <div style="padding:5px"></div>

                    <label class="label-180" for="type">Mobile Number <span class="mandatory">*</span></label>


                    <div style="float:left;">

                           



                            <div style="float:left;width:90px;display:none" id="contact_mobile_std_span">
                                
<input type="text" name="contact_mobile_std" id="contact_mobile_std" value="" class="input1 area" onfocus="contact_number_onfocus(&quot;mobile&quot;,&quot;std&quot;);" onblur="contact_number_default_text();validate_contact_number(&quot;mobile&quot;,&quot;std&quot;,&quot;no&quot;);" onkeyup="return checkvalidnumber(event,&quot;mobile&quot;,this.name);" style="width:70px;margin-left:4px;margin-right:2px" size="2" title="Only enter the Area Code here." maxlength="4">
                            </div>

                            <div style="width:115px;float:left">
                                
<input type="text" name="contact_mobile_number" id="contact_mobile_number" value="" class="input1 area" maxlength="10" onfocus="contact_number_onfocus(&quot;mobile&quot;,&quot;number&quot;);" onblur="contact_number_default_text();validate_contact_number(&quot;mobile&quot;,&quot;number&quot;,&quot;no&quot;);" onkeyup="return checkvalidnumber(event,&quot;mobile&quot;,this.name);" style="width:115px" size="13" title="Enter phone number without area/country code.">

                            </div>


                        
<input type="hidden" name="contact_isd" value="+91" id="contact_isd">

                        <span style="line-height:1px"><br></span>


                        <span style="line-height:1px"><br clear="all"></span><!--[if (IE 6)|(IE 7)]><span style="line-height:1px"><br /></span><![endif]-->



                    </div>

                    <br clear="all">
                    <label class="or">or</label>
                    <div style="float:left">
                    	<div id="errmsg_contact_mobile_std" class="error" style="display:none;width:300px;margin-bottom:10px;"></div><span style="line-height:1px"><br clear="all"></span>
                   </div>
                    <br clear="all">



                    <label class="label-180" for="type">Landline Number</label>


                    <div style="float:left;margin-bottom:5px;">

                           



                          

                            <div style="width:115px;float:left">
                                
<input type="text" name="contact_tel_number" id="contact_tel_number" value="" class="input1 area" maxlength="8" onfocus="contact_number_onfocus(&quot;tel&quot;,&quot;number&quot;);" onblur="contact_number_default_text();validate_contact_number(&quot;tel&quot;,&quot;number&quot;,&quot;no&quot;);" onkeyup="return checkvalidnumber(event,&quot;tel&quot;,this.name);" style="width:115px" size="13" title="Enter phone number without area/country code.">                                <br>

                            </div><br clear="all">


                                                <script language="JavaScript">
                        <!--
                            changecountrycode2('+91|India',document.frm_profile.contact_isd);
                        //-->
                        </script>
                        
                        <span style="line-height:1px"><br></span>

                        <div id="errmsg_contact_tel_std" class="error" style="display:none;width:300px;margin-bottom:10px;"></div><span style="line-height:1px"><br clear="all"></span>


                        <span style="line-height:1px"><br clear="all"></span><!--[if (IE 6)|(IE 7)]><span style="line-height:1px"><br /></span><![endif]-->



                    </div>
					<br clear="all">
					

                </div>
            </div>


            <div style="padding:1px"></div>

            <!-- Annual Income / Education & Profession -->
            <div class="form">
                  <div class="wrapper" style="padding:5px 15px 10px 15px;">

					<div style="padding:5px"></div>

                    <!-- Education Level -->
                    <label class="label-180" for="educationlevel">Education Level <span class="mandatory">*</span></label>
                    <div id="educationlevel_div" class="select_error">
                        
<select name="educationlevel" id="educationlevel" class="select1" style="margin-left:0px;" onchange="validate_educationlevel(this.name);" onfocus="$(&quot;#errmsg_educationlevel&quot;).hide();toggleHint_new(&quot;show&quot;, this.name, document.getElementById(&quot;educationlevel&quot;),document.getElementById(&quot;educationlevel&quot;).offsetWidth)" onblur="validate_educationlevel();hide_annual_income();">
    <option value="" label="Select" selected="selected">Select</option>
    <option value="Bachelors" label="Bachelors">Bachelors</option>
    <option value="Masters" label="Masters">Masters</option>
    <option value="Doctorate" label="Doctorate">Doctorate</option>
    <option value="Diploma" label="Diploma">Diploma</option>
    <option value="Undergraduate" label="Undergraduate">Undergraduate</option>
    <option value="Associates degree" label="Associates degree">Associates degree</option>
    <option value="Honours degree" label="Honours degree">Honours degree</option>
    <option value="Trade school" label="Trade school">Trade school</option>
    <option value="High school" label="High school">High school</option>
    <option value="Less than high school" label="Less than high school">Less than high school</option>
</select>                    </div>
                    <div id="errmsg_educationlevel" class="error" style="display:none"></div><br>
                    
            <!-- HINT STARTS HERE -->
            <span class="hint" id="hint_educationlevel" style="display:none;">
            <div>
                <div class="top_hint"></div><div class="middle_hint">Please select educational level of the person looking to get married.<br><br></div><div class="bottom_hint"></div>
            </div>
            <div class="arrow_hint"></div>
            </span>
            <!-- HINT ENDS HERE -->
                            <!-- Education Level END -->

                    <div style="padding:5px"></div>

                    <!-- Education Area -->
                    <label class="label-180" for="educationfield">Education Field <span class="mandatory">*</span></label>
                    <div id="educationfield_div" class="select_error">
                    
<select name="educationfield" id="educationfield" class="select1" onchange="validate_educationlevel(this.name);" onfocus="$(&quot;#errmsg_educationfield&quot;).hide();toggleHint_new(&quot;show&quot;, this.name,  document.getElementById(&quot;educationfield&quot;),document.getElementById(&quot;educationfield&quot;).offsetWidth)" onblur="validate_educationfield();hide_annual_income();" style="margin-left:0px;">
    <option value="" label="Select" selected="selected">Select</option>
    <option value="Advertising/ Marketing" label="Advertising/ Marketing">Advertising/ Marketing</option>
    <option value="Administrative services" label="Administrative services">Administrative services</option>
    <option value="Architecture" label="Architecture">Architecture</option>
    <option value="Armed Forces" label="Armed Forces">Armed Forces</option>
    <option value="Arts" label="Arts">Arts</option>
    <option value="Commerce" label="Commerce">Commerce</option>
    <option value="Computers/ IT" label="Computers/ IT">Computers/ IT</option>
    <option value="Education" label="Education">Education</option>
    <option value="Engineering/ Technology" label="Engineering/ Technology">Engineering/ Technology</option>
    <option value="Fashion" label="Fashion">Fashion</option>
    <option value="Finance" label="Finance">Finance</option>
    <option value="Fine Arts" label="Fine Arts">Fine Arts</option>
    <option value="Home Science" label="Home Science">Home Science</option>
    <option value="Law" label="Law">Law</option>
    <option value="Management" label="Management">Management</option>
    <option value="Medicine" label="Medicine">Medicine</option>
    <option value="Nursing/ Health Sciences" label="Nursing/ Health Sciences">Nursing/ Health Sciences</option>
    <option value="Office administration" label="Office administration">Office administration</option>
    <option value="Science" label="Science">Science</option>
    <option value="Shipping" label="Shipping">Shipping</option>
    <option value="Travel &amp; Tourism" label="Travel &amp; Tourism">Travel &amp; Tourism</option>
</select>                    </div>
                    <div id="errmsg_educationfield" class="error" style="display:none"></div><br>
					
            <!-- HINT STARTS HERE -->
            <span class="hint" id="hint_educationfield" style="display:none;">
            <div>
                <div class="top_hint"></div><div class="middle_hint">Please select educational field of the person looking to get married.<br><br></div><div class="bottom_hint"></div>
            </div>
            <div class="arrow_hint"></div>
            </span>
            <!-- HINT ENDS HERE -->
                            <!-- Education Area END -->

                    <div style="padding:5px"></div>

					<!-- Working with ST -->
                    <label class="label-180" for="occupation">Working With </label>
                    <div id="working_with_div" class="select_error">
                        
<select name="working_with" id="working_with" class="select1" onfocus="$(&quot;#errmsg_working_with&quot;).hide();toggleHint_new(&quot;show&quot;, this.name, document.getElementById(&quot;working_with&quot;),document.getElementById(&quot;working_with&quot;).offsetWidth)" onchange="validate_working_with();" onblur="validate_working_with();" style="margin-left:0px;">
    <option value="" label="Select" selected="selected">Select</option>
    <option value="Private Company" label="Private Company">Private Company</option>
    <option value="Government / Public Sector" label="Government / Public Sector">Government / Public Sector</option>
    <option value="Defense / Civil Services" label="Defense / Civil Services">Defense / Civil Services</option>
    <option value="Business / Self Employed" label="Business / Self Employed">Business / Self Employed</option>
    <option value="Non Working" label="Non Working">Non Working</option>
</select>                    </div>
                    <div id="errmsg_working_with" class="error" style="display:none"></div><br>
                    
            <!-- HINT STARTS HERE -->
            <span class="hint" id="hint_working_with" style="display:none;">
            <div>
                <div class="top_hint"></div><div class="middle_hint">Please specify type of organization the person looking to get married is working with.<br><br></div><div class="bottom_hint"></div>
            </div>
            <div class="arrow_hint"></div>
            </span>
            <!-- HINT ENDS HERE -->
                            <!-- Working with END -->

                    <div style="padding:5px"></div>

                    <!-- Profession / Industry -->
                    <label class="label-180" for="occupation">Working As </label>
                    <div id="occupation_div" class="select_error">
                        
<select name="occupation" id="occupation" class="select1" onfocus="$(&quot;#errmsg_occupation&quot;).hide();toggleHint_new(&quot;show&quot;, this.name, document.getElementById(&quot;occupation&quot;),document.getElementById(&quot;occupation&quot;).offsetWidth)" onchange="validate_occupation();" onblur="validate_occupation();" style="margin-left:0px;width:286px">
    <option value="" label="Select" selected="selected">Select</option>
    <optgroup label="Accounting, Banking &amp; Finance">
    </optgroup>
    <option value="Banking Professional" label="Banking Professional">Banking Professional</option>
    <option value="Chartered Accountant" label="Chartered Accountant">Chartered Accountant</option>
    <option value="Company Secretary" label="Company Secretary">Company Secretary</option>
    <option value="Finance Professional" label="Finance Professional">Finance Professional</option>
    <option value="Investment Professional" label="Investment Professional">Investment Professional</option>
    <option value="Accounting Professional (Others)" label="Accounting Professional (Others)">Accounting Professional (Others)</option>
    <optgroup label="Administration &amp; HR">
    </optgroup>
    <option value="Admin Professional" label="Admin Professional">Admin Professional</option>
    <option value="Human Resources Professional" label="Human Resources Professional">Human Resources Professional</option>
    <optgroup label="Advertising, Media &amp; Entertainment">
    </optgroup>
    <option value="Actor" label="Actor">Actor</option>
    <option value="Advertising Professional" label="Advertising Professional">Advertising Professional</option>
    <option value="Entertainment Professional" label="Entertainment Professional">Entertainment Professional</option>
    <option value="Event Manager" label="Event Manager">Event Manager</option>
    <option value="Journalist" label="Journalist">Journalist</option>
    <option value="Media Professional" label="Media Professional">Media Professional</option>
    <option value="Public Relations Professional" label="Public Relations Professional">Public Relations Professional</option>
    <optgroup label="Agriculture">
    </optgroup>
    <option value="Farming" label="Farming">Farming</option>
    <option value="Horticulturist" label="Horticulturist">Horticulturist</option>
    <option value="Agricultural Professional (Others)" label="Agricultural Professional (Others)">Agricultural Professional (Others)</option>
    <optgroup label="Airline &amp; Aviation">
    </optgroup>
    <option value="Air Hostess / Flight Attendant" label="Air Hostess / Flight Attendant">Air Hostess / Flight Attendant</option>
    <option value="Pilot / Co-Pilot" label="Pilot / Co-Pilot">Pilot / Co-Pilot</option>
    <option value="Other Airline Professional" label="Other Airline Professional">Other Airline Professional</option>
    <optgroup label="Architecture &amp; Design">
    </optgroup>
    <option value="Architect" label="Architect">Architect</option>
    <option value="Interior Designer" label="Interior Designer">Interior Designer</option>
    <option value="Landscape Architect" label="Landscape Architect">Landscape Architect</option>
    <optgroup label="Artists, Animators &amp; Web Designers">
    </optgroup>
    <option value="Animator" label="Animator">Animator</option>
    <option value="Commercial Artist" label="Commercial Artist">Commercial Artist</option>
    <option value="Web / UX Designers" label="Web / UX Designers">Web / UX Designers</option>
    <option value="Artist (Others)" label="Artist (Others)">Artist (Others)</option>
    <optgroup label="Beauty, Fashion &amp; Jewellery Designers">
    </optgroup>
    <option value="Beautician" label="Beautician">Beautician</option>
    <option value="Fashion Designer" label="Fashion Designer">Fashion Designer</option>
    <option value="Hairstylist" label="Hairstylist">Hairstylist</option>
    <option value="Jewellery Designer" label="Jewellery Designer">Jewellery Designer</option>
    <option value="Designer (Others)" label="Designer (Others)">Designer (Others)</option>
    <optgroup label="BPO, KPO, &amp; Customer Support">
    </optgroup>
    <option value="Customer Support / BPO / KPO Professional" label="Customer Support / BPO / KPO Professional">Customer Support / BPO / KPO Professional</option>
    <optgroup label="Civil Services / Law Enforcement">
    </optgroup>
    <option value="IAS / IRS / IES / IFS" label="IAS / IRS / IES / IFS">IAS / IRS / IES / IFS</option>
    <option value="Indian Police Services (IPS)" label="Indian Police Services (IPS)">Indian Police Services (IPS)</option>
    <option value="Law Enforcement Employee (Others)" label="Law Enforcement Employee (Others)">Law Enforcement Employee (Others)</option>
    <optgroup label="Defense">
    </optgroup>
    <option value="Airforce" label="Airforce">Airforce</option>
    <option value="Army" label="Army">Army</option>
    <option value="Navy" label="Navy">Navy</option>
    <optgroup label="Education &amp; Training">
    </optgroup>
    <option value="Lecturer" label="Lecturer">Lecturer</option>
    <option value="Professor" label="Professor">Professor</option>
    <option value="Research Assistant" label="Research Assistant">Research Assistant</option>
    <option value="Research Scholar" label="Research Scholar">Research Scholar</option>
    <option value="Teacher" label="Teacher">Teacher</option>
    <option value="Training Professional (Others)" label="Training Professional (Others)">Training Professional (Others)</option>
    <optgroup label="Engineering">
    </optgroup>
    <option value="Civil Engineer" label="Civil Engineer">Civil Engineer</option>
    <option value="Electronics / Telecom Engineer" label="Electronics / Telecom Engineer">Electronics / Telecom Engineer</option>
    <option value="Mechanical / Production Engineer" label="Mechanical / Production Engineer">Mechanical / Production Engineer</option>
    <option value="Non IT Engineer (Others)" label="Non IT Engineer (Others)">Non IT Engineer (Others)</option>
    <optgroup label="Hotel &amp; Hospitality">
    </optgroup>
    <option value="Chef / Sommelier / Food Critic" label="Chef / Sommelier / Food Critic">Chef / Sommelier / Food Critic</option>
    <option value="Catering Professional" label="Catering Professional">Catering Professional</option>
    <option value="Hotel &amp; Hospitality Professional (Others)" label="Hotel &amp; Hospitality Professional (Others)">Hotel &amp; Hospitality Professional (Others)</option>
    <optgroup label="IT &amp; Software Engineering">
    </optgroup>
    <option value="Software Developer / Programmer" label="Software Developer / Programmer">Software Developer / Programmer</option>
    <option value="Software Consultant" label="Software Consultant">Software Consultant</option>
    <option value="Hardware &amp; Networking professional" label="Hardware &amp; Networking professional">Hardware &amp; Networking professional</option>
    <option value="Software Professional (Others)" label="Software Professional (Others)">Software Professional (Others)</option>
    <optgroup label="Legal">
    </optgroup>
    <option value="Lawyer" label="Lawyer">Lawyer</option>
    <option value="Legal Assistant" label="Legal Assistant">Legal Assistant</option>
    <option value="Legal Professional (Others)" label="Legal Professional (Others)">Legal Professional (Others)</option>
    <optgroup label="Medical &amp; Healthcare">
    </optgroup>
    <option value="Dentist" label="Dentist">Dentist</option>
    <option value="Doctor" label="Doctor">Doctor</option>
    <option value="Medical Transcriptionist" label="Medical Transcriptionist">Medical Transcriptionist</option>
    <option value="Nurse" label="Nurse">Nurse</option>
    <option value="Pharmacist" label="Pharmacist">Pharmacist</option>
    <option value="Physician Assistant" label="Physician Assistant">Physician Assistant</option>
    <option value="Physiotherapist / Occupational Therapist" label="Physiotherapist / Occupational Therapist">Physiotherapist / Occupational Therapist</option>
    <option value="Psychologist" label="Psychologist">Psychologist</option>
    <option value="Surgeon" label="Surgeon">Surgeon</option>
    <option value="Veterinary Doctor" label="Veterinary Doctor">Veterinary Doctor</option>
    <option value="Therapist (Others)" label="Therapist (Others)">Therapist (Others)</option>
    <option value="Medical / Healthcare Professional (Others)" label="Medical / Healthcare Professional (Others)">Medical / Healthcare Professional (Others)</option>
    <optgroup label="Merchant Navy">
    </optgroup>
    <option value="Merchant Naval Officer" label="Merchant Naval Officer">Merchant Naval Officer</option>
    <option value="Mariner" label="Mariner">Mariner</option>
    <optgroup label="Sales &amp; Marketing">
    </optgroup>
    <option value="Marketing Professional" label="Marketing Professional">Marketing Professional</option>
    <option value="Sales Professional" label="Sales Professional">Sales Professional</option>
    <optgroup label="Science">
    </optgroup>
    <option value="Biologist / Botanist" label="Biologist / Botanist">Biologist / Botanist</option>
    <option value="Physicist" label="Physicist">Physicist</option>
    <option value="Science Professional (Others)" label="Science Professional (Others)">Science Professional (Others)</option>
    <optgroup label="Corporate Professionals">
    </optgroup>
    <option value="CxO / Chairman / Director / President" label="CxO / Chairman / Director / President">CxO / Chairman / Director / President</option>
    <option value="VP / AVP / GM / DGM" label="VP / AVP / GM / DGM">VP / AVP / GM / DGM</option>
    <option value="Sr. Manager / Manager" label="Sr. Manager / Manager">Sr. Manager / Manager</option>
    <option value="Consultant / Supervisor / Team Leads" label="Consultant / Supervisor / Team Leads">Consultant / Supervisor / Team Leads</option>
    <option value="Team Member / Staff" label="Team Member / Staff">Team Member / Staff</option>
    <optgroup label="Others">
    </optgroup>
    <option value="Agent / Broker / Trader / Contractor" label="Agent / Broker / Trader / Contractor">Agent / Broker / Trader / Contractor</option>
    <option value="Business Owner / Entrepreneur" label="Business Owner / Entrepreneur">Business Owner / Entrepreneur</option>
    <option value="Politician" label="Politician">Politician</option>
    <option value="Social Worker / Volunteer / NGO" label="Social Worker / Volunteer / NGO">Social Worker / Volunteer / NGO</option>
    <option value="Sportsman" label="Sportsman">Sportsman</option>
    <option value="Travel &amp; Transport Professional" label="Travel &amp; Transport Professional">Travel &amp; Transport Professional</option>
    <option value="Writer" label="Writer">Writer</option>
    <optgroup label="Non Working">
    </optgroup>
    <option value="Student" label="Student">Student</option>
    <option value="Retired" label="Retired">Retired</option>
    <option value="Not working" label="Not working">Not working</option>
</select>                    </div>
                    <div id="errmsg_occupation" class="error" style="display:none;width:200px"></div><br>
                    
            <!-- HINT STARTS HERE -->
            <span class="hint" id="hint_occupation" style="display:none;">
            <div>
                <div class="top_hint"></div><div class="middle_hint">Please specify profession area of the person looking to get married.<br><br></div><div class="bottom_hint"></div>
            </div>
            <div class="arrow_hint"></div>
            </span>
            <!-- HINT ENDS HERE -->
                            <!-- Profession / Industry END -->

                    <div style="padding:5px"></div>

                    <!-- working after marriage -->
                                        <label class="label-180" for="work_after_marriage">Would you prefer working after marriage?</label>
                    <div id="work_after_marriage_div" class="select_error">
                    
<label for="work_after_marriage-Yes"><input type="radio" name="work_after_marriage" id="work_after_marriage-Yes" value="Yes" class="radio" onfocus="$(&quot;#errmsg_work_after_marriage&quot;).hide();" onclick="validate_work_after_marriage();">Yes</label> <label for="work_after_marriage-No"><input type="radio" name="work_after_marriage" id="work_after_marriage-No" value="No" class="radio" onfocus="$(&quot;#errmsg_work_after_marriage&quot;).hide();" onclick="validate_work_after_marriage();">No</label> <label for="work_after_marriage-NotSure"><input type="radio" name="work_after_marriage" id="work_after_marriage-NotSure" value="Not Sure" class="radio" onfocus="$(&quot;#errmsg_work_after_marriage&quot;).hide();" onclick="validate_work_after_marriage();">Not Sure</label>                         <br>

                    </div>
                         <div id="errmsg_work_after_marriage" class="error" style="display:none"></div>
                    <br>

                    <div style="padding:10px"></div>
                                        <!-- working after marriage END -->


                    <!-- Annual Income -->
                    <label class="label-180" for="annualincome">Annual Income <span class="mandatory">*</span></label>
                    <div id="ai_div" class="select_error">
                        
<select name="annualincome" id="annualincome" class="select1" onblur="validate_annualincome();" onfocus="$(&quot;#errmsg_annualincome&quot;).hide();toggleHint_new(&quot;show&quot;, this.name, document.getElementById(&quot;annualincome&quot;),document.getElementById(&quot;annualincome&quot;).offsetWidth)" style="margin-left:0px;">
    <option value="" label="Select" selected="selected">Select</option>
    <option value="Not applicable" label="Not applicable">Not applicable</option>
    <option value="Less than Rs 50 thousand" label="Less than Rs 50 thousand">Less than Rs 50 thousand</option>
    <option value="Rs 50 thousand to 1 lakh" label="Rs 50 thousand to 1 lakh">Rs 50 thousand to 1 lakh</option>
    <option value="Rs 1 lakh to 2 lakhs" label="Rs 1 lakh to 2 lakhs">Rs 1 lakh to 2 lakhs</option>
    <option value="Rs 2 lakhs to 3 lakhs" label="Rs 2 lakhs to 3 lakhs">Rs 2 lakhs to 3 lakhs</option>
    <option value="Rs 3 lakhs to 4 lakhs" label="Rs 3 lakhs to 4 lakhs">Rs 3 lakhs to 4 lakhs</option>
    <option value="Rs 4 lakhs to 5 lakhs" label="Rs 4 lakhs to 5 lakhs">Rs 4 lakhs to 5 lakhs</option>
    <option value="Rs 5 lakhs to 7 lakhs" label="Rs 5 lakhs to 7 lakhs">Rs 5 lakhs to 7 lakhs</option>
    <option value="Rs 7 lakhs to 10 lakhs" label="Rs 7 lakhs to 10 lakhs">Rs 7 lakhs to 10 lakhs</option>
    <option value="Rs 10 lakhs to 15 lakhs" label="Rs 10 lakhs to 15 lakhs">Rs 10 lakhs to 15 lakhs</option>
    <option value="Rs 15 lakhs to 20 lakhs" label="Rs 15 lakhs to 20 lakhs">Rs 15 lakhs to 20 lakhs</option>
    <option value="Rs 20 lakhs to 30 lakhs" label="Rs 20 lakhs to 30 lakhs">Rs 20 lakhs to 30 lakhs</option>
    <option value="Rs 30 lakhs and above" label="Rs 30 lakhs and above">Rs 30 lakhs and above</option>
    <option value="Dont want to specify" label="Dont want to specify">Dont want to specify</option>
</select>                    </div>
                    <div id="errmsg_annualincome" class="error" style="display:none"></div><br>
					
            <!-- HINT STARTS HERE -->
			<label class="label-180" for="annualincome">Membership <span class="mandatory">*</span></label>
			<div>
			<input type="radio" name="membership" value="0" checked="checked" />
			Regular
			&nbsp;&nbsp;
			<input type="radio" name="membership" value="1" />
			Silver
			&nbsp;&nbsp;
			<input type="radio" name="membership" value="2" />
			Gold
			
			</div>
            <span class="hint" id="hint_annualincome" style="display:none;">
            <div>
                <div class="top_hint"></div><div class="middle_hint">Please select annual income of the person looking to get married.<br><br></div><div class="bottom_hint"></div>
            </div>
            <div class="arrow_hint"></div>
            </span>
            <!-- HINT ENDS HERE -->
                            <!-- Annual Income END -->

                </div>
            </div>
            <!-- Annual Income / Education & Profession END -->

            <div style="padding:1px"></div>

            <!-- Describe yourself -->
            <div class="form">
                <div class="wrapper" style="padding:5px 15px 10px 15px">
                    <a name="moreabt"></a>
                    <span style="color:#FE6C00;float:left;font:normal 12px arial;margin-top:2px;*margin-top:0px">Describe yourself, likes &amp; dislikes, kind of person you are looking for, etc.</span><br clear="all">

                    

                    <label class="label-180" for="aboutyourself">Describe yourself <span class="mandatory">*</span></label>
                    
<textarea name="aboutyourself" id="aboutyourself" class="" wrap="virtual" onkeyup="calcCharLen(&quot;frm_profile&quot;, &quot;aboutyourself&quot;, &quot;counter1&quot;, 4000);" onblur="calcCharLen(&quot;frm_profile&quot;, &quot;aboutyourself&quot;, &quot;counter1&quot;, 4000);validate_aboutyourself();" onfocus="$(&quot;errmsg_aboutyourself&quot;).hide();" maxlength="4000" style="width:479px;height:75px;font:normal 13px arial;color:#434343;border:1px solid #BDD6A8;y-overflow:scroll;x-overflow:none;background:#F7F7F7 " rows="24" cols="80"></textarea>                    <br>

                    <div style="padding:2px"></div>

                    <div style="margin-left:190px;_margin-left:193px;width:467px;*width:469px;font:normal 13px arial;color:#434343;padding:0px 6px;border:solid 1px #D4D4D4">
                        <div style="float:left;padding-top:4px">(min. 50 characters; max. 4000)</div>
                        <span style="display:block;text-align:right"><input type="text" name="counter1" id="counter1" value="0" readonly="" style="width:45px;font:normal 18px arial;color:#C0C0C0;text-align:right;background:none;border:none"></span>
                        <span style="line-height:2px"><br clear="all"></span>
                    </div>

                    <div id="errmsg_aboutyourself" class="error" style="display:none;margin:0 0 0 190px;_margin:0 0 0 97px;width:350px"></div><br>
                </div>
            </div>
            			<a href="javascript:void(0);"><img src="<?php echo base_url(); ?>images/crea-profile.gif" style="cursor:pointer;border:none;margin:15px 0px 0px 207px" width="234" border="0" height="43" alt="Create My Profile" title="Create My Profile" onclick="javascript:validate_error(&#39;SH91017969&#39;); return false;"></a>
			<!-- Describe yourself END -->
            
		
<input type="hidden" name="btn" value="2" id="btn">		
<input type="hidden" name="profession_grp_flag" value="2" id="profession_grp_flag">        </form>
</div>
</div>

        <br clear="all">
        
				<div id="footer">

					<div id="short_intro">
					  <div class="spacer_2"></div>

						<div id="copyrights_commu"><span>Copyright � 1996-2011 Swayamvaraonline.com Matrimonials - The No.1 Matrimonial Services Provider&#8482; All Rights Reserved.</span></div>
					</div><div id="secure_img">
				
				</div>
            

</body>

</html>

