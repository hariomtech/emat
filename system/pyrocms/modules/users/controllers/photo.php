<?php 
ob_start();
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Photo extends CI_Controller {

	
	
	public function index()
	{
	
	//$this->load->helper('url');
	    $this->load->helper('users/user');
	$this->load->view('header');
     	
		$this->load->view('photo');
			 $this->load->view('footer');
	}
	
	public function uploadphoto()
	{
	
	     if (!file_exists("files/".$_SESSION['newuid'])) 
	     {

       mkdir("files/".$_SESSION['newuid'], 0777);
    
	      }
		  $path="files/".$_SESSION['newuid']."/";
	$config['upload_path'] = "files/".$_SESSION['newuid'];
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size']    = '5048'; //2 meg


        $this->load->library('upload');
		 
        $i=0;
        foreach($_FILES as $key => $value)
        {
            if( ! empty($key['name']))
            {
                $this->upload->initialize($config);
        
                if ( ! $photoname=$this->upload->do_upload($key))
                {
                    $errors[] = $this->upload->display_errors();
                    
                }    
                else
                {
                   $arr=$this->upload->data();
				   if($i!=0)
				   {
				   $str.=",".$path.$arr['file_name'];
				   }
				   else
				   {
                    $str=$path.$arr['file_name'];
					$i++;
					}

                }
             }
        
        }
	 $this->load->model('photo_model', '', TRUE);
	 $status= $this->photo_model->save($str);
	 $this->load->view('header');
	 $data['msg']='Photo uploded sucessfully !!';
	  $this->load->helper('users/user');  
		$this->load->view('photo',$data);
		 $this->load->view('footer');
	}
	
	public function photoup()
	{
	 if(!$this->session->userdata('id'))
	{
	
	redirect('users/login');
	}
	if (!file_exists("files/".$this->session->userdata('id'))) 
	     {

       mkdir("files/".$this->session->userdata('id'), 0777);
    
	      }
		  $path="files/".$this->session->userdata('id')."/";
	$config['upload_path'] = "files/".$this->session->userdata('id');
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size']    = '2048'; //2 meg

        $this->load->library('upload',$config);
		$configThumb = array();
      $configThumb['image_library'] = 'gd2';
      $configThumb['source_image'] = '';
      $configThumb['create_thumb'] = TRUE;
      $configThumb['maintain_ratio'] = TRUE;
      /* Set the height and width or thumbs */
      /* Do not worry - CI is pretty smart in resizing */
      /* It will create the largest thumb that can fit in those dimensions */
      /* Thumbs will be saved in same upload dir but with a _thumb suffix */
      /* e.g. 'image.jpg' thumb would be called 'image_thumb.jpg' */
      $configThumb['width'] = 140;
      $configThumb['height'] = 160;
      /* Load the image library */
      $this->load->library('image_lib'); 
        $i=0;
        foreach($_FILES as $key => $value)
        {
            if( ! empty($key['name']))
            {
                $this->upload->initialize($config);
        
                if ( ! $photoname=$this->upload->do_upload($key))
                {
                    $errors[] = $this->upload->display_errors();
                    
                }    
                else
                {
                   $arr=$this->upload->data();
				   if($i!=0)
				   {
				   $str.=",".$path.$arr['file_name'];
				   }
				   else
				   {
                    $str=$path.$arr['file_name'];
					$i++;
					}

                }
             }
			  $uploadedFiles[$i] = $arr;
        /* If the file is an image - create a thumbnail */
        if($arr['is_image'] == 1) {
           $configThumb['source_image'] = $arr['full_path'];
          $this->image_lib->initialize($configThumb);
          $this->image_lib->resize();
}
        
        }
	
	 $this->load->view('header');
	 $data['msg']='Photo uploded sucessfully !!';
	  $this->load->model('profile_m','',TRUE);
	$data['query']=$this->profile_m->photoupload($str); 
	  /*$this->load->helper('users/user');
	 
	$this->load->model('profile_m','',TRUE);
	$this->load->model('profile_model','',TRUE);
$data['query']=$this->profile_model->showprofile($this->session->userdata('id'));
	$data['photo']=$this->profile_m->getphoto();   
		$this->load->view('uploadphoto',$data);
		 $this->load->view('footer');*/
		redirect('users/edittag/uploadphoto', 'refresh');
	}
	
	public function delete($id)
	{
	if(!$this->session->userdata('id'))
	{
	  redirect('users/login');
	}
	 $this->load->view('header');
	 $data['msg']='Photo deleted successfully !!';
	 
	  $this->load->helper('users/user');
	  $this->load->model('profile_m','',TRUE);
	$this->profile_m->deletephoto($id); 
	$this->load->model('profile_m','',TRUE);
	$this->load->model('profile_model','',TRUE);
$data['query']=$this->profile_model->showprofile($this->session->userdata('id'));
	$data['photo']=$this->profile_m->getphoto();   
		$this->load->view('uploadphoto',$data);
		 $this->load->view('footer');
	}
	
	public function first($id)
	{
	if(!$this->session->userdata('id'))
	{
	  redirect('users/login');
	}
	 $this->load->view('header');
	 $data['msg']='Photo moved to first !!';
	 
	  $this->load->helper('users/user');
	  $this->load->model('profile_m','',TRUE);
	$this->profile_m->movefirst($id); 
	$this->load->model('profile_m','',TRUE);
	$this->load->model('profile_model','',TRUE);
$data['query']=$this->profile_model->showprofile($this->session->userdata('id'));
	$data['photo']=$this->profile_m->getphoto();   
		$this->load->view('uploadphoto',$data);
		 $this->load->view('footer');
	}
}?>