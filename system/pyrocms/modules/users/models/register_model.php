<?php
class Register_Model extends CI_Model 
{
public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->config('users/ion_auth', TRUE);
		$this->load->helper('cookie');
		$this->load->helper('date');
		$this->load->library('session');
		$this->tables  = $this->config->item('tables', 'ion_auth');
		$this->columns = $this->config->item('columns', 'ion_auth');

		$this->identity_column = $this->config->item('identity', 'ion_auth');
	    $this->store_salt      = $this->config->item('store_salt', 'ion_auth');
	    $this->salt_length     = $this->config->item('salt_length', 'ion_auth');
	    $this->meta_join       = $this->config->item('join', 'ion_auth');
	}
  public function save()
   { 
       $this->load->library('session');
			$uid=rand(100001,999999);
  $newdata = array(
                   'countryselected'  => $_POST['countryofresidence'],
                   'casteselected'     => $_POST['castearray'] 
               );

$this->session->set_userdata($newdata);
 
		$salt		= $this->store_salt ? $this->salt() : FALSE;
		$password	= $this->hash_password($_POST['password1'], $salt);
      $date1=$_POST['year']."-".$_POST['month']."-".$_POST['day'] ;
		
		 $this->db->query("insert into users(email,password,salt,group_id,ip_address,active,activation_code,created_on) value('".mysql_real_escape_string($_POST['email'])."','".$password."','".$salt."','2','".$_SERVER['REMOTE_ADDR']."','0','".mysql_real_escape_string(md5($uid))."','".strtotime(date('Y/m/d'))."')")or die(mysql_error());
		 
		 $pid=mysql_insert_id();
	if($_POST['mareacode'])
	{
	$m=$_POST['mareacode']."-".mysql_real_escape_string($_POST['contact_mobile_number']);
	}
	else
	{
	$m=mysql_real_escape_string($_POST['contact_mobile_number']);
	}
	if($_POST['lareacode'])
	{
	$l=$_POST['lareacode']."-".mysql_real_escape_string($_POST['contact_tel_number']);
	}	
	else
	{
	$l=mysql_real_escape_string($_POST['contact_tel_number']);
	} 
		$this->db->query("insert into profiles(user_id,uid,first_name,last_name,dob,gender,religion,mother_tongue,country,caste,mobile,phone) value('".$pid."','".$uid."','".mysql_real_escape_string($_POST['first_name'])."','".mysql_real_escape_string($_POST['last_name'])."','".$date1."','".mysql_real_escape_string($_POST['gender'])."','".mysql_real_escape_string($_POST['community'])."','".mysql_real_escape_string($_POST['mother_tongue'])."','".mysql_real_escape_string($_POST['countryofresidence'])."','".mysql_real_escape_string($_POST['castearray'])."' ,'".$m."','".$l."') ")or die(mysql_error());
		
		$to=$_POST['email'];
		
		$rquery=mysql_query("select * from email_templates where slug='registeremail'")or die(mysql_error());
		$rrow=mysql_fetch_array($rquery);
	 $subject = $rrow['subject'];
     
$headers = "MIME-Version: 1.0\r\n";			
			
$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
$headers .= "From: admin@swayamvar.com";

 $str=str_replace("[name]",ucfirst($_POST['first_name']),$rrow['body']);
			
			//$message="You are registered successfully at Bubblegum.<br/>Kind regards,<br/>The Team at Bubblegum";
            mail($to,$subject,$str,$headers); 
$rquery1=mysql_query("select * from email_templates where slug='verifyac'")or die(mysql_error());
		$rrow1=mysql_fetch_array($rquery1);
	$subject = $rrow1['subject'];
     
			
	
$link=base_url()."index.php/users/register/verify/".$uid;
//verifyac


$str1=str_replace("[name]",ucfirst($_POST['first_name']),$rrow1['body']);
$str2=str_replace("[email]",ucfirst($_POST['email']),$str1);
$str3=str_replace("[password]",ucfirst($_POST['password1']),$str2);
$str4=str_replace("[uid]",ucfirst($uid),$str3);
$str5=str_replace("[link]",ucfirst($link),$str4);

if($_POST['membership']==0)
{
 mail($to,$subject,$str5,$headers);
}


		 return($pid);
   }
   public function save_step2()
   { 
  // $str= "update registration set maritalstatus='".$_POST['maritalstatus']."', height='".$_POST['height']."', caste='".$_POST['caste']."', smoke='".$_POST['smoke']."', drink='".$_POST['drink']."', specialclasses='".$_POST['specialcases']."', stateresidence='".$_POST['stateofresidence']."', cityresidence='".$_POST['nearest_city']."', mobile='".$_POST['contact_mobile_number']."', landline='".$_POST['contact_tel_number']."', education='".$_POST['educationlevel']."', preferwork='".$_POST['work_after_marriage']."', income='".$_POST['annualincome']."', urself='".$_POST['aboutyourself']."',havingchild='".$_POST['children']."', noofchild='".$_POST['no_of_kids']."'";
   $str= "update profiles set maritalstatus='".mysql_real_escape_string($_POST['maritalstatus'])."', createdby='".mysql_real_escape_string($_POST['postedby'])."',  height='".mysql_real_escape_string($_POST['height'])."', smoke='".mysql_real_escape_string($_POST['smoke'])."', drink='".mysql_real_escape_string($_POST['drink'])."', specialclasses='".mysql_real_escape_string($_POST['specialcases'])."', stateresidence='".mysql_real_escape_string($_POST['stateofresidence'])."', cityresidence='".mysql_real_escape_string($_POST['nearest_city'])."',education='".mysql_real_escape_string($_POST['educationlevel'])."', bio='".mysql_real_escape_string($_POST['aboutyourself'])."',havingchild='".mysql_real_escape_string($_POST['children'])."', noofchild='".mysql_real_escape_string($_POST['no_of_kids'])."', weight='".mysql_real_escape_string($_POST['weight'])."', familytype='".mysql_real_escape_string($_POST['family_type'])."',familystatus='".mysql_real_escape_string($_POST[' personal_values'])."'";
   if(isset($_POST['dosham']))
   {
   $str.=", dosham='".mysql_real_escape_string($_POST['dosham'])."'";
   }
   if(isset($_POST['subcaste']))
   {
   $str.=", subcaste='".mysql_real_escape_string($_POST['subcaste'])."'";
   }
   if(isset($_POST['family_value']))
   {
   $str.=", personalvalue='".mysql_real_escape_string($_POST['family_value'])."'";
   }
   if(isset($_POST['diet']))
   {
   $str.=", diet='".mysql_real_escape_string($_POST['diet'])."'";
   }
   if(isset($_POST['complexion']))
   {
   $str.=", complexion='".mysql_real_escape_string($_POST['complexion'])."'";
   }
    if(isset($_POST['bodytype']))
   {
   $str.=", bodytype='".mysql_real_escape_string($_POST['bodytype'])."'";
   }
   if(isset($_POST['working_with']))
   {
   $str.=",workingwith='".mysql_real_escape_string($_POST['working_with'])."'";
   }
   if(isset($_POST['occupation']))
   {
   $str.=",workingas='".mysql_real_escape_string($_POST['occupation'])."'";
   }
    if(isset($_POST['income']))
   {
   
   $str.=",income='".mysql_real_escape_string($_POST['annualincome'])."'";
   }
   if(isset($_POST['manglik']))
   {
   
   $str.=",manglik='".mysql_real_escape_string($_POST['manglik'])."'";
   }
   if(isset($_POST['STAR']))
   {
   
   $str.=",star='".mysql_real_escape_string($_POST['STAR'])."'";
   }
    if(isset($_POST['RAASI']))
   {
   
   $str.=",raasi='".mysql_real_escape_string($_POST['RAASI'])."'";
   }
   
   
   $str.=" where user_id=".$_POST['id'];
   
   	 $this->db->query($str)or die(mysql_error());
		 return(mysql_insert_id());
   }
   
   public function checkmail()
   {
   
   $query=mysql_query("select * from users where email='".$_POST['email']."'")or die(mysql_error());
   if(mysql_num_rows($query)>0)
   {
   return('exist');
   }
   }
   
   public function verify($uid)
   {
   mysql_query('update users as u, profiles as p set active=1,activation_code="" where p.uid="'.$uid.'" and p.user_id=u.id')or die(mysql_error());
   }
   
   public function checklink($uid)
   {
     $query=mysql_query("select * from profiles where uid='".$uid."'")or die(mysql_error());
	 if(mysql_num_rows($query)>0)
	 {
	 return('exist');
	 }
   }
   
   public function getmambership()
   {
    $query = $this->db->query('SELECT * FROM membership');
    return $query->result_array();
    }
	public function getcountry()
   {
    $query = $this->db->query('SELECT * FROM countries');
    return $query;
    }
	
	/**
	 * Generates a random salt value.
	 *
	 * @return void
	 * @author Mathew
	 **/
	public function salt()
	{
		return substr(md5(uniqid(rand(), true)), 0, $this->salt_length);
	}
	
	/**
	 * Hashes the password to be stored in the database.
	 *
	 * @return void
	 * @author Mathew
	 **/
	public function hash_password($password, $salt=false)
	{
	    if (empty($password))
	    {
	    	return FALSE;
	    }

	    if ($this->store_salt && $salt)
		{
			return  sha1($password . $salt);
		}
		else
		{
	    	$salt = $this->salt();
	    	return  $salt . substr(sha1($salt . $password), 0, -$this->salt_length);
		}
	}
	
	function hobbysave($id)
	{
	   $hobby= implode(",",$_POST['HOBBIES']);
	   if($hobby!="" && $_POST['HOBBIESDESC'])
	   {
	    $hobby.=",other:".$_POST['HOBBIESDESC'];
	   }
	   else
	   {
	   $hobby="other:".$_POST['HOBBIESDESC'];
	   }
	   
	   $interest=implode(",",$_POST['INTEREST']);
	   if($interest!="" && $_POST['INTERESTDESC'])
	   {
	    $interest.=",other:".$_POST['INTERESTDESC'];
	   }
	   else
	   {
	   $interest="other:".$_POST['INTERESTDESC'];
	   }
	   $music=implode(",",$_POST['MUSIC']);
	   if($music!="" && $_POST['MUSICDESC'])
	   {
	    $music.=",other:".$_POST['MUSICDESC'];
	   }
	   else
	   {
	   $music="other:".$_POST['MUSICDESC'];
	   }
	   $read=implode(",",$_POST['READ']);
	   if($read!="" && $_POST['READDESC'])
	   {
	    $read.=",other:".$_POST['READDESC'];
	   }
	   else
	   {
	   $read="other:".$_POST['READDESC'];
	   }
	   $movie=implode(",",$_POST['MOVIE']);
	   if($movie!="" && $_POST['MOVIEDESC'])
	   {
	    $movie.=",other:".$_POST['MOVIEDESC'];
	   }
	   else
	   {
	   $movie="other:".$_POST['MOVIEDESC'];
	   }
	    $sports=implode(",",$_POST['SPORTS']);
	   if($sports!="" && $_POST['SPORTSDESC'])
	   {
	    $sports.=",other:".$_POST['SPORTSDESC'];
	   }
	   else
	   {
	    $sports='other:'.$_POST['SPORTSDESC'];
	   }
	   $food=implode(",",$_POST['FOOD']);
	   if($food!="" && $_POST['FOODDESC'])
	   {
	    $food.=",other:".$_POST['FOODDESC'];
	   }
	   else
	   {
	   $food='other:'.$_POST['FOODDESC'];
	   }
	    $dress=implode(",",$_POST['DRESS']);
	   if($dress!="" && $_POST['DRESSDESC'])
	   {
	    $dress.=",other:".$_POST['DRESSDESC'];
	   }
	   else
	   {
	   $dress='other:'.$_POST['DRESSESC'];
	   }
	   
	    $spoke=implode(",",$_POST['SPOKENLANG']);
	   if($spoke!="" && $_POST['SPOKENLANGDESC'])
	   {
	    $spoke.=",other:".$_POST['SPOKENLANGDESC'];
	   }
	   else
	   {
	   $spoke='other:'.$_POST['SPOKENLANGDESC'];
	   }
	  //echo "insert into hobbies(user_id,hobby,interest,music,read1,movie,sports,food,dress,spoke) value('".$id."','".mysql_real_escape_string($hobby)."','".mysql_real_escape_string($interest)."','".mysql_real_escape_string($music)."','".mysql_real_escape_string($read)."','".mysql_real_escape_string($movie)."','".mysql_real_escape_string($sports)."','".mysql_real_escape_string($food)."','".mysql_real_escape_string($dress)."','".mysql_real_escape_string($spoke)."')<br/>";
	   mysql_query("insert into hobbies(user_id,hobby,interest,music,read1,movie,sports,food,dress,spoke) value('".$id."','".mysql_real_escape_string($hobby)."','".mysql_real_escape_string($interest)."','".mysql_real_escape_string($music)."','".mysql_real_escape_string($read)."','".mysql_real_escape_string($movie)."','".mysql_real_escape_string($sports)."','".mysql_real_escape_string($food)."','".mysql_real_escape_string($dress)."','".mysql_real_escape_string($spoke)."')")or die(mysql_error());
	   
	}
	
	/*public function getstate()
	{
	$query=$this->db->query('select * from countries1 order by state')or die(mysql_error());
	return $query;
	}*/
}
?>